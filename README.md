# Tricoteuses-Assemblee

## _Retrieve, clean up & handle French Assemblée nationale's open data_

## Retrieval of open data (in JSON format) from Assemblée nationale's website

```bash
mkdir ../assemblee-data/
npx babel-node --extensions ".ts" -- src/scripts/retrieve_open_data.ts --fetch ../assemblee-data/
```

## Reorganizating open data files and directories into cleaner (and split) directories

```bash
npx babel-node --extensions ".ts" -- src/scripts/reorganize_data.ts ../assemblee-data/
```

_Note_: These reorganized files are also available in [Tricoteuses / Data / Données brutes de l'Assemblée](https://git.en-root.org/tricoteuses/data/assemblee-brut). They are updated on a regular basis.

## Validation & cleaning of JSON data

```bash
npx babel-node --extensions ".ts" -- src/scripts/clean_reorganized_data.ts ../assemblee-data/
```

_Note_: These split & cleaned files are also available in [Tricoteuses / Data / Données nettoyées de l'Assemblée](https://git.en-root.org/tricoteuses/data/assemblee-nettoye) with the `_nettoye` suffix. They are updated on a regular basis.

## Retrieval of députés' pictures from Assemblée nationale's website

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_deputes_photos.ts --fetch ../assemblee-data/
```

## Retrieval of sénateurs' pictures from Assemblée nationale's website

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_senateurs_photos.ts --fetch ../assemblee-data/
```

## Retrieval of documents from Assemblée nationale's website

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_textes_lois.ts ../assemblee-data/
```

## Test loading everything in memory

### Test loading small split files

```bash
npx babel-node --extensions ".ts" --max-old-space-size=2048 -- src/scripts/test_load.ts ../assemblee-data/
```

### Test loading big non-split files

```bash
npx babel-node --extensions ".ts" --max-old-space-size=2048 -- src/scripts/test_load_big_files.ts ../assemblee-data/
```

_Note_: The big non-split open data files should not be used. Use small split files instead.

## Initial generation of TypeScript & JSON schema files from JSON data.

```bash
npx quicktype --acronym-style=camel -o src/raw_types/acteurs_et_organes.ts ../assemblee-data/AMO{10,20,30,40,50}_*.json
```

```bash
npx quicktype --acronym-style=camel -o src/raw_types/agendas.ts ../assemblee-data/Agenda_{XIV,XV}.json
```

```bash
npx babel-node --extensions ".ts" --max-old-space-size=4096 -- src/scripts/merge_amendements.ts -v ../assemblee-data/
NODE_OPTIONS=--max-old-space-size=4096 npx quicktype --acronym-style=camel -o src/raw_types/amendements.ts ../assemblee-data/Amendements_XV_fusionne.json
```

Edit `src/raw_types/amendements.ts` to:

- Replace `r("ActeurRefElement")` with `""`.
- Remove 2 definitions of `ActeurRefElement` and replace it with `string` elsewhere.
- Replace `r("GroupePolitiqueRefEnum")` with `""`.
- Remove 2 definitions of `GroupePolitiqueRefEnum` and replace it with `string` elsewhere.
- Replace `r("Organe")` with `""`.
- Remove 2 definitions of `Organe` and replace it with `string` elsewhere.
- Replace `r("Libelle")` with `""`.
- Remove 2 definitions of `Libelle` and replace it with `string` elsewhere.
- Add:
  ```typescript
  export interface AmendementWrapper {
    amendement: Amendement
  }
  ```
- Add:
  ```typescript
    "AmendementWrapper": o([
        { json: "amendement", js: "amendement", typ: r("Amendement") },
    ], false),
  ```
- Add the following static methods to class `Convert`:

  ```typescript
    public static toAmendementWrapper(json: string): AmendementWrapper {
        return cast(JSON.parse(json), r("AmendementWrapper"));
    }

    public static amendementWrapperToJson(value: AmendementWrapper): string {
        return JSON.stringify(uncast(value, r("AmendementWrapper")), null, 2);
    }
  ```

```bash
npx quicktype --acronym-style=camel -o src/raw_types/dossiers_legislatifs.ts ../assemblee-data/Dossiers_Legislatifs_{XIV,XV}.json
```

Edit `src/raw_types/dossiers_legislatifs.ts` to:

- Replace regular expression `r\(".*OrganeRef"\)` with `""`.
- Remove definitions of regular expression `[^ ]*OrganeRef` and replace it with `string`.
- Replace regular expression `r\(".*DossierRef"\)` with `""`.
- Remove definitions of regular expression `[^ ]*DossierRef` and replace it with `string`.
- Replace regular expression `r\(".*AuteurMotion"\)` with `""`.
- Remove definitions of regular expression `[^ ]*AuteurMotion` and replace it with `string`.
- Replace regular expression `r\(".*DenominationStructurelle"\)` except for `DocumentDenominationStructurelle` with `""`.
- Remove 2 definitions of regular expression `[^ ]*DenominationStructurelle` except for `DocumentDenominationStructurelle`and replace it with `string`.

```bash
npx babel-node --extensions ".ts" -- src/scripts/merge_scrutins.ts -v ../assemblee-data/
npx quicktype --acronym-style=camel -o src/raw_types/scrutins.ts ../assemblee-data/Scrutins_{XIV,XV_fusionne}.json
```

Edit `src/raw_types/scrutins.ts` to:

- Replace `r("ActeurRef")` with `""`.
- Remove 2 definitions of `ActeurRef` and replace it with `string` elsewhere.
- Replace `r("GroupeOrganeRef")` with `""`.
- Remove 2 definitions of `GroupeOrganeRef` and replace it with `string` elsewhere.
- Replace `r("MandatRef")` with `""`.
- Remove 2 definitions of `MandatRef` and replace it with `string` elsewhere.
- Replace `r("ScrutinOrganeRef")` with `""`.
- Remove 2 definitions of `ScrutinOrganeRef` and replace it with `string` elsewhere.
- Replace `r("SessionRef")` with `""`.
- Remove 2 definitions of `SessionRef` and replace it with `string` elsewhere.
- Add:
  ```typescript
  export interface ScrutinWrapper {
    scrutin: Scrutin
  }
  ```
- Add:
  ```typescript
    "ScrutinWrapper": o([
        { json: "scrutin", js: "scrutin", typ: r("Scrutin") },
    ], false),
  ```
- Add the following static methods to class `Convert`:

  ```typescript
    public static toScrutinWrapper(json: string): ScrutinWrapper {
        return cast(JSON.parse(json), r("ScrutinWrapper"));
    }

    public static scrutinWrapperToJson(value: ScrutinWrapper): string {
        return JSON.stringify(uncast(value, r("ScrutinWrapper")), null, 2);
    }
  ```

---

## Updating JSON schema files and validating JSON files

* Convert src/types/*.ts into JSON schemas for comparison purposes

```bash
for f in src/types/*.ts ; do b=$(basename $f .ts) ; npx typescript-json-schema src/types/$b.ts '*' > src/schemas/converted_from_type/$b.json ; done
```

* Manually update src/schemas/*/*.json to account for these differences
* Verify the JSON files validate with the updated schema

```bash
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s acteur/Acteur.json -r organe/CodeTypeOrgane.json -r "acteur/*.json" --data "../data/assemblee-nettoye/AMO*nettoye/acteurs/*.json"
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s organe/Organe.json -r "organe/*.json" --data "../data/assemblee-nettoye/AMO*nettoye/organes/*.json"
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s document/Document.json -r "document/*.json" --data "../data/assemblee-nettoye/Dossiers_Legislatifs_{XIV,XV}_nettoye/documents/**/*.json"
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s dossier/DossierLegislatif.json -r "dossier/*.json" -r "document/*.json" --data "../data/assemblee-nettoye/Dossiers_Legislatifs_{XIV,XV}_nettoye/dossiers/**/*.json"
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s amendement/Amendement.json -r 'amendement/*.json' --data '../data/assemblee-nettoye/Amendements_XV_nettoye/**/*.json'
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s agenda/Agenda.json -r 'agenda/*.json' --data '../data/assemblee-nettoye/Agenda_{XIV,XV}_nettoye/**/*.json'
npx babel-node --extensions .ts -- src/scripts/validate_json.ts --repository=$(git rev-parse --show-toplevel) -s scrutin/Scrutin.json -r 'scrutin/*.json' --data '../data/assemblee-nettoye/Scrutins_{XIV,XV}_nettoye/**/*.json'
```

If an error occurs and the schema must be fixed:

* Verify the schema works by using **--dev** to use the schema from the current working directory instead of fetching them from the tag maching the version mentionned in the JSON file. For instance, if the file `acteurs/PA766283.json` has `schemaVersion = "acteur-1.0"` it will use the schema found at [schema-acteur-1.0](https://git.en-root.org/tricoteuses/tricoteuses-assemblee/tree/schema-acteur-1.0) and **not** the current working directory, except if **--dev** is used.
* Once the schema is verified to work, add a tag matching the directory of the schema. For instance for `amendement/Amendement.json` or any of its references (i.e. `amendement/*.json`), set the tag `schema-amendement-X.Y`.
  * If the schema change is backward compatible (i.e. software using the corresponding JSON won't break), increment Y (X.1, X.2, ...)
  * If the schema change is not backward compatible, increment X and set Y to zero (1.0, 2.0, ...)

The tag with the highest version will be used by `src/scripts/clean_reorganized_data.ts` to add a `schemaVersion` field for all JSON files created in a `*_nettoye` repository from that point on. The goal is for a JSON file to validate against an immutable schema identified by a version tag and to all each JSON file to have a different version of the schema.

See the [discussion in the forum](https://forum.en-root.org/t/release-management/85/) for more information and further discussion.

## Helpers to create documentation

```bash
$ npx babel-node --extensions .ts -- src/scripts/document_dossiers_legislatifs.ts --data ../data/assemblee-nettoye/Dossiers_Legislatifs_{XIV,XV}_nettoye/dossiers/**/*.json
```

See the [data-site README](https://git.en-root.org/tricoteuses/data-site/blob/master/README.md) for more information about how it is used.

## Obsolete or Now Useless Scripts

### Validation & cleaning of big non-split files

```bash
npx babel-node --extensions ".ts" --max-old-space-size=8192 -- src/scripts/clean_data.ts ../assemblee-data/
```

_Note_: The big non-split open data files should not be used. Use small split files instead.

### Retrieval of députés' non open data informations from Assemblée nationale's website

```bash
npx babel-node --extensions ".ts" -- src/scripts/retrieve_deputes_infos.ts --fetch --parse ../assemblee-data/
```
