module.exports = {
  env: {
    node: true,
  },
  extends: "eslint:recommended",
  globals: {
    Promise: true,
    Set: true,
  },
  parser: "babel-eslint",
  rules: {
    "comma-dangle": ["error", "always-multiline"],
    "max-len": [
      "error",
      { code: 90, ignoreRegExpLiterals: true, ignoreUrls: true, tabWidth: 2 },
    ],
    "no-undef": "error",
    "no-unused-expressions": "error",
    "no-unused-vars": "error",
    "no-use-before-define": ["error", "nofunc"],
    quotes: ["error", "double", "avoid-escape"],
    semi: ["error", "never"],
  },
}
