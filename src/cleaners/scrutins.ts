import assert from "assert"

import { cleanBooleanAttribute, cleanXmlArtefacts } from "./xml"

function cleanDecompteNominatif(decompteNominatif: any) {
  cleanXmlArtefacts(decompteNominatif)

  let nonVotants = decompteNominatif.nonVotants
  if (nonVotants !== undefined) {
    if (typeof nonVotants === "string") {
      assert.strictEqual(nonVotants, "0")
      nonVotants = []
    } else if (!Array.isArray(nonVotants)) {
      assert(nonVotants)
      nonVotants = [nonVotants]
    }
    const nonVotantsArray = nonVotants
    nonVotants = []
    for (let nonVotant of nonVotantsArray) {
      if (nonVotant === null) {
        continue
      }
      nonVotant = nonVotant.votant
      if (Array.isArray(nonVotant)) {
        nonVotants.push(...nonVotant)
      } else {
        assert(nonVotant)
        nonVotants.push(nonVotant)
      }
    }
    if (nonVotants.length === 0) {
      delete decompteNominatif.nonVotants
    } else {
      decompteNominatif.nonVotants = nonVotants
      for (const votant of nonVotants) {
        cleanVotant(votant)
      }
    }
  }

  let pour = decompteNominatif.pour
  if (pour === undefined) {
    pour = decompteNominatif.pours
  } else {
    assert.strictEqual(decompteNominatif.pours, undefined)
  }
  if (pour !== undefined) {
    pour = pour.votant
    if (!Array.isArray(pour)) {
      assert(pour)
      pour = [pour]
    }
    if (pour.length === 0) {
      delete decompteNominatif.pour
    } else {
      decompteNominatif.pour = pour
      for (const votant of pour) {
        cleanVotant(votant)
      }
    }
    delete decompteNominatif.pours
  }

  let contre = decompteNominatif.contre
  if (contre === undefined) {
    contre = decompteNominatif.contres
  } else {
    assert.strictEqual(decompteNominatif.contres, undefined)
  }
  if (contre !== undefined) {
    contre = contre.votant
    if (!Array.isArray(contre)) {
      assert(contre)
      contre = [contre]
    }
    if (contre.length === 0) {
      delete decompteNominatif.contre
    } else {
      decompteNominatif.contre = contre
      for (const votant of contre) {
        cleanVotant(votant)
      }
    }
    delete decompteNominatif.contres
  }

  let abstentions = decompteNominatif.abstentions
  if (abstentions !== undefined) {
    if (!Array.isArray(abstentions)) {
      abstentions = [abstentions]
    }
    const abstentionsArray = abstentions
    abstentions = []
    for (let absentionsItem of abstentionsArray) {
      if (absentionsItem === null) {
        continue
      }
      absentionsItem = absentionsItem.votant
      if (Array.isArray(absentionsItem)) {
        abstentions.push(...absentionsItem)
      } else {
        assert(absentionsItem)
        abstentions.push(absentionsItem)
      }
    }
    if (abstentions.length === 0) {
      delete decompteNominatif.abstentions
    } else {
      decompteNominatif.abstentions = abstentions
      for (const votant of abstentions) {
        cleanVotant(votant)
      }
    }
  }

  let nonVotantsVolontaires = decompteNominatif.nonVotantsVolontaires
  if (nonVotantsVolontaires !== undefined) {
    if (!Array.isArray(nonVotantsVolontaires)) {
      nonVotantsVolontaires = [nonVotantsVolontaires]
    }
    const nonVotantsVolontairesArray = nonVotantsVolontaires
    nonVotantsVolontaires = []
    for (let absentionsItem of nonVotantsVolontairesArray) {
      if (absentionsItem === null) {
        continue
      }
      absentionsItem = absentionsItem.votant
      if (Array.isArray(absentionsItem)) {
        nonVotantsVolontaires.push(...absentionsItem)
      } else {
        assert(absentionsItem)
        nonVotantsVolontaires.push(absentionsItem)
      }
    }
    if (nonVotantsVolontaires.length === 0) {
      delete decompteNominatif.nonVotantsVolontaires
    } else {
      decompteNominatif.nonVotantsVolontaires = nonVotantsVolontaires
      for (const votant of nonVotantsVolontaires) {
        cleanVotant(votant)
      }
    }
  }
}

export function cleanScrutin(scrutin: any): void {
  cleanXmlArtefacts(scrutin)

  const demandeur = scrutin.demandeur
  assert(demandeur)
  {
    cleanXmlArtefacts(demandeur)

    assert.strictEqual(demandeur.referenceLegislative, undefined)
  }

  const objet = scrutin.objet
  assert(objet)
  {
    cleanXmlArtefacts(objet)

    assert.strictEqual(objet.referenceLegislative, undefined)
  }

  let ventilationVotes = scrutin.ventilationVotes
  assert(ventilationVotes)
  {
    ventilationVotes = ventilationVotes.organe
    assert(ventilationVotes)

    let groupes = ventilationVotes.groupes
    assert(groupes)
    {
      groupes = groupes.groupe
      assert(Array.isArray(groupes))
      for (const groupe of groupes) {
        const vote = groupe.vote
        assert(vote)
        {
          const decompteVoix = vote.decompteVoix
          assert(decompteVoix)
          {
            if (decompteVoix.abstention !== undefined) {
              assert.strictEqual(decompteVoix.abstentions, undefined)
              decompteVoix.abstentions = decompteVoix.abstention
              delete decompteVoix.abstention
            }

            if (decompteVoix.nonVotant !== undefined) {
              assert.strictEqual(decompteVoix.nonVotants, undefined)
              decompteVoix.nonVotants = decompteVoix.nonVotant
              delete decompteVoix.nonVotant
            }
          }

          const decompteNominatif = vote.decompteNominatif
          assert(decompteNominatif)
          cleanDecompteNominatif(decompteNominatif)
        }
      }
      ventilationVotes.groupes = groupes
    }

    scrutin.ventilationVotes = ventilationVotes
  }

  const miseAuPoint = scrutin.miseAuPoint
  if (miseAuPoint !== undefined) {
    cleanDecompteNominatif(miseAuPoint)

    let dysfonctionnement = miseAuPoint.dysfonctionnement
    if (dysfonctionnement !== undefined) {
      cleanDecompteNominatif(dysfonctionnement)
      if (
        Object.values(dysfonctionnement).filter(value => value !== undefined).length === 0
      ) {
        delete miseAuPoint.dysfonctionnement
      }
    }

    if (Object.values(miseAuPoint).filter(value => value !== undefined).length === 0) {
      delete scrutin.miseAuPoint
    }
  }
}

function cleanVotant(votant: any) {
  cleanBooleanAttribute(votant, "parDelegation")
}
