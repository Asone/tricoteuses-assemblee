import assert from "assert"

import { cleanXmlArtefacts } from "./xml"

export function cleanActeur(acteur: any): void {
  cleanXmlArtefacts(acteur)

  assert.strictEqual(acteur.uid["@xsi:type"], "IdActeur_type")
  acteur.uid = acteur.uid["#text"]
  assert(acteur.uid)

  const etatCivil = acteur.etatCivil
  {
    cleanXmlArtefacts(etatCivil)

    const ident = etatCivil.ident
    {
      cleanXmlArtefacts(ident)

      const infoNaissance = etatCivil.infoNaissance
      {
        cleanXmlArtefacts(infoNaissance)
      }
    }
  }

  const profession = acteur.profession
  if (profession !== undefined) {
    cleanXmlArtefacts(profession)

    const socProcInsee = profession.socProcINSEE
    assert(socProcInsee)
    cleanXmlArtefacts(socProcInsee)
    delete profession.socProcINSEE
    profession.socProcInsee = socProcInsee
  }

  const uriHatvp = acteur.uri_hatvp
  if (uriHatvp !== undefined) {
    acteur.uriHatvp = uriHatvp
    delete acteur.uri_hatvp
  }

  let adresses = acteur.adresses
  if (adresses !== undefined) {
    adresses = adresses.adresse
    if (adresses === undefined) {
      delete acteur.adresses
      adresses = undefined
    } else if (Array.isArray(adresses)) {
      acteur.adresses = adresses
    } else {
      assert.notStrictEqual(adresses, null)
      assert.strictEqual(typeof adresses, "object")
      acteur.adresses = adresses = [adresses]
    }
    if (adresses !== undefined) {
      for (const adresse of adresses) {
        cleanXmlArtefacts(adresse)

        const xsiType = adresse["@xsi:type"]
        assert(
          [
            "AdresseMail_Type",
            "AdressePostale_Type",
            "AdresseSiteWeb_Type",
            "AdresseTelephonique_Type",
          ].includes(xsiType),
          `Unexpected "@xsi:type": ${xsiType}`,
        )
        adresse.xsiType = xsiType
        delete adresse["@xsi:type"]

        assert.notStrictEqual(adresse.uid, undefined)
        assert.notStrictEqual(adresse.type, undefined)
        assert.notStrictEqual(adresse.typeLibelle, undefined)

        if (xsiType === "AdressePostale_Type") {
          assert.strictEqual(adresse.adresseDeRattachement, undefined)
        }
      }
    }
  }

  let mandats = acteur.mandats
  if (mandats !== undefined) {
    let mandat = mandats.mandat
    if (Array.isArray(mandat)) {
      mandats = acteur.mandats = mandat
    } else if (typeof mandat === "object") {
      mandats = acteur.mandats = [mandat]
    }

    for (const mandat of mandats) {
      cleanXmlArtefacts(mandat)

      const xsiType = mandat["@xsi:type"]
      if (xsiType !== undefined) {
        mandat.xsiType = xsiType
        delete mandat["@xsi:type"]
      }

      const infosQualite = mandat.infosQualite
      {
        cleanXmlArtefacts(infosQualite)
      }

      const organes = mandat.organes
      delete mandat.organes
      let organesRefs = organes.organeRef
      assert(organesRefs)
      if (!Array.isArray(organesRefs)) {
        organesRefs = [organesRefs]
      }
      mandat.organesRefs = organesRefs

      let suppleant = mandat.suppleants
      delete mandat.suppleants
      if (suppleant) {
        suppleant = suppleant.suppleant
        assert(suppleant)
        mandat.suppleant = suppleant

        cleanXmlArtefacts(suppleant)
      }

      const election = mandat.election
      if (election !== undefined) {
        cleanXmlArtefacts(election)

        const lieu = election.lieu
        cleanXmlArtefacts(lieu)
      }

      const mandature = mandat.mandature
      if (mandature !== undefined) {
        cleanXmlArtefacts(mandature)
      }

      assert.strictEqual(mandat.chambre, undefined)

      // Replace collaborateurs with an array of collaborateurs.
      let collaborateurs = mandat.collaborateurs
      if (collaborateurs !== undefined) {
        const collaborateursArray = Array.isArray(collaborateurs)
          ? collaborateurs.filter(collaborateur => collaborateur !== null)
          : [collaborateurs]
        collaborateurs = []
        collaborateursArray.map(({ collaborateur }) => {
          if (Array.isArray(collaborateur)) {
            for (const collaborateurItem of collaborateur) {
              if (collaborateurItem !== null) {
                collaborateurs.push(collaborateurItem)
              }
            }
          } else if (collaborateur) {
            collaborateurs.push(collaborateur)
          }
        })
        if (collaborateurs.length === 0) {
          delete mandat.collaborateurs
          collaborateurs = undefined
        } else {
          mandat.collaborateurs = collaborateurs
        }

        for (const collaborateur of collaborateurs || []) {
          cleanXmlArtefacts(collaborateur)

          assert.strictEqual(collaborateur.dateDebut, undefined)
          assert.strictEqual(collaborateur.dateFin, undefined)
        }
      }

      const infosHorsSian = mandat.InfosHorsSIAN
      if (infosHorsSian !== undefined) {
        cleanXmlArtefacts(infosHorsSian)

        assert.strictEqual(infosHorsSian.HATVP_URI, undefined)
        assert.strictEqual(Object.keys(infosHorsSian).length, 0)
        delete mandat.InfosHorsSIAN
      }
    }

    // Sort mandats to ease comparison of different open data files.
    mandats.sort((a: { uid: string }, b: { uid: string }) =>
      a.uid.length === b.uid.length
        ? a.uid.localeCompare(b.uid)
        : a.uid.length - b.uid.length,
    )
  }
}

export function cleanRapporteur(rapporteur: any) {
  const etudePlfRef = rapporteur.etudePLFRef
  if (rapporteur.etudePLFRef !== undefined) {
    delete rapporteur.etudePLFRef
    rapporteur.etudePlfRef = etudePlfRef
  }
}
