import assert from "assert"

import { cleanRapporteur } from "./acteurs"
import { cleanXmlArtefacts } from "./xml"

export function cleanActeLegislatif(acteLegislatif: any): void {
  cleanXmlArtefacts(acteLegislatif)

  const xsiType = acteLegislatif["@xsi:type"]
  if (xsiType !== undefined) {
    acteLegislatif.xsiType = xsiType
    delete acteLegislatif["@xsi:type"]
  }

  //

  let auteursRefs = acteLegislatif.auteurs
  if (auteursRefs !== undefined) {
    auteursRefs = auteursRefs.acteurRef
    if (!Array.isArray(auteursRefs)) {
      assert(auteursRefs)
      auteursRefs = [auteursRefs]
    }
    acteLegislatif.auteursRefs = auteursRefs
    delete acteLegislatif.auteurs
  }

  const casSaisine = acteLegislatif.casSaisine
  if (casSaisine !== undefined) {
    cleanTypeDeclaration(casSaisine)
  }

  const contributionInternaute = acteLegislatif.contributionInternaute
  if (contributionInternaute !== undefined) {
    cleanXmlArtefacts(contributionInternaute)
  }

  assert.strictEqual(acteLegislatif.dateRetrait, undefined)

  const decision = acteLegislatif.decision
  if (decision !== undefined) {
    cleanTypeDeclaration(decision)
  }

  const infoJo = acteLegislatif.infoJO
  if (infoJo !== undefined) {
    cleanInfoJo(infoJo)
    acteLegislatif.infoJo = infoJo
    delete acteLegislatif.infoJO
  }

  const infoJoce = acteLegislatif.infoJOCE
  if (infoJoce !== undefined) {
    acteLegislatif.infoJoce = infoJoce
    delete acteLegislatif.infoJOCE

    const refJoce = infoJoce.refJOCE
    if (refJoce !== undefined) {
      infoJoce.refJoce = refJoce
      delete infoJoce.refJOCE
    }

    const dateJoce = infoJoce.dateJOCE
    if (dateJoce !== undefined) {
      infoJoce.dateJoce = dateJoce
      delete infoJoce.dateJOCE
    }
  }

  let infoJoRect = acteLegislatif.infoJORect
  if (infoJoRect !== undefined) {
    if (!Array.isArray(infoJoRect)) {
      infoJoRect = [infoJoRect]
    }
    acteLegislatif.infoJoRect = infoJoRect
    delete acteLegislatif.infoJORect

    for (const infoJoRectItem of infoJoRect) {
      cleanInfoJo(infoJoRectItem)
    }
  }

  // Convert acteLegislatif.initiateur and acteLegislatif.initiateurs to a DossierParlementaireInitiateur.
  let initiateur = acteLegislatif.initiateur
  if (initiateur !== undefined) {
    initiateur = initiateur.acteurs.acteur
    if (!Array.isArray(initiateur)) {
      assert(initiateur)
      initiateur = [initiateur]
    }
    initiateur = {
      acteurs: initiateur,
    }
    acteLegislatif.initiateur = initiateur
  }
  initiateur = acteLegislatif.initiateurs
  if (initiateur !== undefined) {
    assert.strictEqual(acteLegislatif.initiateur, undefined)
    if (initiateur.acteurRef === undefined) {
      assert.notStrictEqual(initiateur.organeRef, undefined)
    } else {
      if (!Array.isArray(initiateur)) {
        assert(initiateur)
        initiateur = [initiateur]
      }
      initiateur = {
        acteurs: initiateur,
      }
    }
    acteLegislatif.initiateur = initiateur
    delete acteLegislatif.initiateurs
  }

  assert.strictEqual(acteLegislatif.odSeancejRef, undefined)

  const provenanceRef = acteLegislatif.provenance
  if (provenanceRef !== undefined) {
    delete acteLegislatif.provenance
    acteLegislatif.provenanceRef = provenanceRef
  }

  let rapporteurs = acteLegislatif.rapporteurs
  if (rapporteurs !== undefined) {
    rapporteurs = rapporteurs.rapporteur
    if (!Array.isArray(rapporteurs)) {
      assert(rapporteurs)
      rapporteurs = [rapporteurs]
    }
    acteLegislatif.rapporteurs = rapporteurs
    for (const rapporteur of rapporteurs) {
      cleanRapporteur(rapporteur)
    }
  }

  const referenceNor = acteLegislatif.referenceNOR
  if (referenceNor !== undefined) {
    acteLegislatif.referenceNor = referenceNor
    delete acteLegislatif.referenceNOR
  }

  assert.strictEqual(acteLegislatif.reunion, undefined)

  const statutAdoption = acteLegislatif.statutAdoption
  if (statutAdoption !== undefined) {
    cleanTypeDeclaration(statutAdoption)
  }

  const statutConclusion = acteLegislatif.statutConclusion
  if (statutConclusion !== undefined) {
    cleanTypeDeclaration(statutConclusion)
    if (statutConclusion.famCode === "TSORTFnull") {
      // Occured once in https://git.en-root.org/tricoteuses/data/assemblee-brut/Dossiers_Legislatifs_XV/commit/8383800fe7d993da7b596c5b20fadc31c4df3835
      delete acteLegislatif.statutConclusion
    }
  }

  const texteAdopteRef = acteLegislatif.texteAdopte
  if (texteAdopteRef !== undefined) {
    delete acteLegislatif.texteAdopte
    acteLegislatif.texteAdopteRef = texteAdopteRef
  }

  const texteAssocieRef = acteLegislatif.texteAssocie
  if (texteAssocieRef !== undefined) {
    delete acteLegislatif.texteAssocie
    acteLegislatif.texteAssocieRef = texteAssocieRef
  }

  let textesAssocies = acteLegislatif.textesAssocies
  if (textesAssocies !== undefined) {
    textesAssocies = textesAssocies.texteAssocie
    if (!Array.isArray(textesAssocies)) {
      assert(textesAssocies)
      textesAssocies = [textesAssocies]
    }
    acteLegislatif.textesAssocies = textesAssocies

    for (const texteAssocie of textesAssocies) {
      const texteAssocieRef = texteAssocie.refTexteAssocie
      if (texteAssocieRef !== undefined) {
        delete texteAssocie.refTexteAssocie
        texteAssocie.texteAssocieRef = texteAssocieRef
      }
    }
  }

  const typeDeclaration = acteLegislatif.typeDeclaration
  if (typeDeclaration !== undefined) {
    cleanTypeDeclaration(typeDeclaration)
  }

  const typeMotion = acteLegislatif.typeMotion
  if (typeMotion !== undefined) {
    cleanTypeDeclaration(typeMotion)
  }

  const typeMotionCensure = acteLegislatif.typeMotionCensure
  if (typeMotionCensure !== undefined) {
    cleanTypeDeclaration(typeMotionCensure)
  }

  let voteRefs = acteLegislatif.voteRefs
  if (voteRefs !== undefined) {
    voteRefs = voteRefs.voteRef
    if (!Array.isArray(voteRefs)) {
      assert(voteRefs)
      voteRefs = [voteRefs]
    }
    acteLegislatif.voteRefs = voteRefs
  }

  //

  let actesLegislatifs = acteLegislatif.actesLegislatifs
  if (actesLegislatifs !== undefined) {
    actesLegislatifs = actesLegislatifs.acteLegislatif
    if (!Array.isArray(actesLegislatifs)) {
      assert(actesLegislatifs)
      actesLegislatifs = [actesLegislatifs]
    }
    acteLegislatif.actesLegislatifs = actesLegislatifs
    for (const acteLegislatif of actesLegislatifs) {
      cleanActeLegislatif(acteLegislatif)
    }
  }
}

function cleanInfoJo(infoJo: any): void {
  cleanXmlArtefacts(infoJo)

  const typeJo = infoJo.typeJO
  if (typeJo !== undefined) {
    infoJo.typeJo = typeJo
    delete infoJo.typeJO
  }

  const dateJo = infoJo.dateJO
  if (dateJo !== undefined) {
    infoJo.dateJo = dateJo
    delete infoJo.dateJO
  }

  assert.strictEqual(infoJo.pageJO, undefined)

  const numJo = infoJo.numJO
  if (numJo !== undefined) {
    infoJo.numJo = numJo
    delete infoJo.numJO
  }

  const referenceNor = infoJo.referenceNOR
  if (referenceNor !== undefined) {
    infoJo.referenceNor = referenceNor
    delete infoJo.referenceNOR
  }
}

function cleanTypeDeclaration(typeDeclaration: any) {
  typeDeclaration.famCode = typeDeclaration.fam_code
  delete typeDeclaration.fam_code
  assert(typeDeclaration.famCode)

  if (!typeDeclaration.libelle) {
    // Occured once in https://git.en-root.org/tricoteuses/data/assemblee-brut/Dossiers_Legislatifs_XV/commit/8383800fe7d993da7b596c5b20fadc31c4df3835:
    // "statutConclusion": {
    //   "fam_code": "TSORTFnull"
    // },
    // This famCode is an error.
    typeDeclaration.libelle = typeDeclaration.famCode
  }
}
