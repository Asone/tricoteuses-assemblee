import assert from "assert"

import { cleanBooleanAttribute, cleanXmlArtefacts } from "./xml"

function cleanCycleDeVie(cycleDeVie: any): void {
  cleanXmlArtefacts(cycleDeVie)

  const chrono = cycleDeVie.chrono
  assert(chrono)
  {
    cleanXmlArtefacts(chrono)

    let creation = chrono.creation
    assert.strictEqual(typeof creation, "string")
    creation = new Date(creation)
    chrono.creation = creation

    let cloture = chrono.cloture
    if (cloture !== undefined) {
      assert.strictEqual(typeof cloture, "string")
      cloture = new Date(cloture)
      chrono.cloture = cloture
    }
  }
}

export function cleanReunion(reunion: any): void {
  cleanXmlArtefacts(reunion)

  cleanBooleanAttribute(reunion, "captationVideo")
  cleanBooleanAttribute(reunion, "comiteSecret")
  cleanBooleanAttribute(reunion, "ouverturePresse")

  const xsiType = reunion["@xsi:type"]
  if (xsiType !== undefined) {
    reunion.xsiType = xsiType
    delete reunion["@xsi:type"]
  }

  const timestampDebut = reunion.timeStampDebut
  assert.notStrictEqual(timestampDebut, undefined)
  reunion.timestampDebut = timestampDebut
  delete reunion.timeStampDebut

  let timestampFin = reunion.timeStampFin
  if (timestampFin !== undefined) {
    assert.strictEqual(typeof timestampFin, "string")
    timestampFin = new Date(timestampFin)
    reunion.timestampFin = timestampFin
    delete reunion.timeStampFin
  }

  const lieu = reunion.lieu
  if (lieu !== undefined) {
    cleanXmlArtefacts(lieu)
  }

  const cycleDeVie = reunion.cycleDeVie
  assert.notStrictEqual(cycleDeVie, undefined)
  cleanCycleDeVie(cycleDeVie)

  const demandeurs = reunion.demandeurs
  if (demandeurs !== undefined) {
    let acteurs = demandeurs.acteur
    if (acteurs !== undefined) {
      if (!Array.isArray(acteurs)) {
        assert.notStrictEqual(acteurs, null)
        assert.strictEqual(typeof acteurs, "object")
        acteurs = [acteurs]
      }
      demandeurs.acteurs = acteurs
      delete demandeurs.acteur

      for (const acteur of acteurs) {
        cleanXmlArtefacts(acteur)
      }
    }
  }

  assert.strictEqual(reunion.demandeur, undefined)

  const participants = reunion.participants
  if (participants !== undefined) {
    cleanXmlArtefacts(participants)

    let participantsInternes = participants.participantsInternes
    if (participantsInternes !== undefined) {
      participantsInternes = participantsInternes.participantInterne
      if (!Array.isArray(participantsInternes)) {
        assert.notStrictEqual(participantsInternes, null)
        assert.strictEqual(typeof participantsInternes, "object")
        participantsInternes = [participantsInternes]
      }
      participants.participantsInternes = participantsInternes
    }

    let personnesAuditionnees = participants.personnesAuditionnees
    if (personnesAuditionnees !== undefined) {
      personnesAuditionnees = personnesAuditionnees.personneAuditionnee
      if (personnesAuditionnees === null) {
        delete participants.personnesAuditionnees
      } else {
        if (!Array.isArray(personnesAuditionnees)) {
          // assert.notStrictEqual(personnesAuditionnees, null)
          assert.strictEqual(typeof personnesAuditionnees, "object")
          personnesAuditionnees = [personnesAuditionnees]
        }
        personnesAuditionnees = personnesAuditionnees.filter(
          (personneAudionnee: any) => personneAudionnee !== null,
        )
        if (personnesAuditionnees.length > 0) {
          participants.personnesAuditionnees = personnesAuditionnees
        } else {
          delete participants.personnesAuditionnees
        }

        for (const personneAudionnee of personnesAuditionnees) {
          const xsiType = personneAudionnee.uid["@xsi:type"]
          assert(xsiType)
          personneAudionnee.xsiType = xsiType
          const uid = personneAudionnee.uid["#text"]
          assert(uid)
          personneAudionnee.uid = uid

          const ident = personneAudionnee.ident
          assert(ident)
          {
            cleanXmlArtefacts(ident)
          }
        }
      }
    }
  }

  if (reunion.ODJ !== undefined) {
    const odj = reunion.ODJ
    reunion.odj = odj
    delete reunion.ODJ
    {
      cleanXmlArtefacts(odj)

      let convocationOdj = odj.convocationODJ
      if (convocationOdj !== undefined) {
        if (Array.isArray(convocationOdj.item)) {
          convocationOdj = convocationOdj.item
        } else {
          assert(convocationOdj.item)
          convocationOdj = [convocationOdj.item]
        }
        odj.convocationOdj = convocationOdj
        delete odj.convocationODJ
      }

      let resumeOdj = odj.resumeODJ
      if (resumeOdj !== undefined) {
        if (Array.isArray(resumeOdj.item)) {
          resumeOdj = resumeOdj.item
        } else {
          assert(resumeOdj.item)
          resumeOdj = [resumeOdj.item]
        }
        odj.resumeOdj = resumeOdj
        delete odj.resumeODJ
      }

      let pointsOdj = odj.pointsODJ
      if (pointsOdj !== undefined) {
        let pointOdj = pointsOdj.pointODJ
        if (Array.isArray(pointOdj)) {
          pointsOdj = pointOdj
        } else {
          assert(pointOdj)
          pointsOdj = [pointOdj]
        }
        odj.pointsOdj = pointsOdj

        for (const pointOdj of pointsOdj) {
          cleanXmlArtefacts(pointOdj)

          cleanBooleanAttribute(pointOdj, "comiteSecret")

          const xsiType = pointOdj["@xsi:type"]
          if (xsiType !== undefined) {
            pointOdj.xsiType = xsiType
            delete pointOdj["@xsi:type"]
          }

          const cycleDeVie = pointOdj.cycleDeVie
          assert.notStrictEqual(cycleDeVie, undefined)
          cleanCycleDeVie(cycleDeVie)

          assert.strictEqual(pointOdj.demandeurPoint, undefined)

          let dossiersLegislatifsRefs = pointOdj.dossiersLegislatifsRefs
          if (dossiersLegislatifsRefs !== undefined) {
            if (Array.isArray(dossiersLegislatifsRefs.dossierRef)) {
              dossiersLegislatifsRefs = dossiersLegislatifsRefs.dossierRef
            } else {
              assert(dossiersLegislatifsRefs.dossierRef)
              dossiersLegislatifsRefs = [dossiersLegislatifsRefs.dossierRef]
            }
            pointOdj.dossiersLegislatifsRefs = dossiersLegislatifsRefs
          }

          pointOdj.typePointOdj = pointOdj.typePointODJ
          delete pointOdj.typePointODJ

          assert.strictEqual(pointOdj.textesAssocies, undefined)

          pointOdj.natureTravauxOdj = pointOdj.natureTravauxODJ
          delete pointOdj.natureTravauxODJ

          let dateConfPres = pointOdj.dateConfPres
          if (dateConfPres !== undefined) {
            assert.strictEqual(typeof dateConfPres, "string")
            assert(/^\d{4}-\d{2}-\d{2}\+\d{2}:00$/.test(dateConfPres))
            dateConfPres = new Date(dateConfPres.split("+")[0])
            pointOdj.dateConfPres = dateConfPres
          }

          let dateLettreMinistre = pointOdj.dateLettreMinistre
          if (dateLettreMinistre !== undefined) {
            assert.strictEqual(typeof dateLettreMinistre, "string")
            assert(/^\d{4}-\d{2}-\d{2}\+\d{2}:00$/.test(dateLettreMinistre))
            dateLettreMinistre = new Date(dateLettreMinistre.split("+")[0])
            pointOdj.dateLettreMinistre = dateLettreMinistre
          }
        }
        delete odj.pointsODJ
      }
    }
  }

  const identifiants = reunion.identifiants
  if (identifiants !== undefined) {
    cleanXmlArtefacts(identifiants)
    const numSeanceJo = identifiants.numSeanceJO
    if (numSeanceJo !== undefined) {
      identifiants.numSeanceJo = numSeanceJo
      delete identifiants.numSeanceJO
    }

    const idJo = identifiants.idJO
    if (idJo !== undefined) {
      identifiants.idJo = idJo
      delete identifiants.idJO
    }

    let dateSeance = identifiants.DateSeance
    assert.strictEqual(typeof dateSeance, "string")
    assert(/^\d{4}-\d{2}-\d{2}\+\d{2}:00$/.test(dateSeance))
    dateSeance = new Date(dateSeance.split("+")[0])
    identifiants.dateSeance = dateSeance
    delete identifiants.DateSeance
  }

  const infosReunionsInternationale = reunion.infosReunionsInternationale
  if (infosReunionsInternationale !== undefined) {
    cleanXmlArtefacts(infosReunionsInternationale)

    cleanBooleanAttribute(
      infosReunionsInternationale,
      "estReunionInternationale",
    )

    let listePays = infosReunionsInternationale.listePays
    if (listePays !== undefined) {
      listePays = listePays.paysRef
      if (!Array.isArray(listePays)) {
        assert.strictEqual(typeof listePays, "string")
        listePays = [listePays]
      }
      infosReunionsInternationale.listePays = listePays
    }
  }
}
