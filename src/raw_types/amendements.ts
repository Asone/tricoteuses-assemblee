// To parse this data:
//
//   import { Convert, Amendements, AmendementWrapper } from "./file";
//
//   const amendements = Convert.toAmendements(json);
//   const amendementWrapper = Convert.toAmendementWrapper(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Amendements {
    textesEtAmendements: TextesEtAmendements;
}

export interface TextesEtAmendements {
    texteleg: Texteleg[];
}

export interface Texteleg {
    "@xmlns:xsi":       string;
    refTexteLegislatif: string;
    amendements:        AmendementsClass;
}

export interface AmendementsClass {
    amendement: Amendement[] | Amendement;
    "#text"?:       string;
    offset?:        null;
    nom?:           Nom;
    avant_A_Apres?: AvantAApresEnum;
    dateDepot?:     Date;
    documentURI?:   string;}

export interface AmendementWrapper {
  amendement: Amendement
}

export interface Amendement {
    "@xmlns":                 string;
    uid:                      string;
    identifiant:              Identifiant;
    numeroLong:               string;
    etapeTexte:               EtapeTexte;
    triAmendement:            string;
    cardinaliteAmdtMultiples: string;
    amendementParent:         PuneHedgehog | string;
    etat:                     Etat;
    signataires:              Signataires;
    pointeurFragmentTexte:    PointeurFragmentTexte;
    corps:                    Corps;
    representations:          Representations;
    seanceDiscussion:         PuneHedgehog | string;
    sort:                     Sort;
    dateDepot:                PuneHedgehog | Date;
    dateDistribution:         PuneHedgehog | Date;
    article99:                string;
    loiReference:             LoiReference;
}

export interface PuneHedgehog {
    "@xmlns:xsi": string;
    "@xsi:nil":   string;
}

export interface Corps {
    dispositif?:                 string;
    exposeSommaire:              PuneHedgehog | string;
    annexeExposeSommaire:        PuneHedgehog;
    cartoucheDelaiDepotDepasse?: PuneHedgehog | string;
    avantAppel?:                 AvantAppel;
    dispositifAmdtCredit?:       DispositifAmdtCredit;
}

export interface AvantAppel {
    "@xmlns:xsi"?: string;
    "@xsi:nil"?:   string;
    dispositif?:   string;
}

export interface DispositifAmdtCredit {
    listeProgrammes: ListeProgrammes;
    totalAE:         Total;
    totalCP:         Total;
}

export interface ListeProgrammes {
    programme: ProgrammeElement[] | ProgrammeElement;
}

export interface ProgrammeElement {
    libelle:       string;
    id:            string;
    AE:            Ae;
    CP:            Ae;
    action:        Action;
    lignesCredits: LignesCredits | null;
}

export interface Ae {
    montantPositif: string;
    montantNegatif: string;
}

export enum Action {
    Creation = "creation",
    Modification = "modification",
    Suppression = "suppression",
}

export interface LignesCredits {
    ligneCredit: LigneCreditElement[] | LigneCreditElement;
}

export interface LigneCreditElement {
    id:      string;
    libelle: string;
    AE:      Ae;
    CP:      Ae;
}

export interface Total {
    montantPositif: string;
    montantNegatif: string;
    solde:          string;
}

export enum EtapeTexte {
    DeuxièmeLecture = "deuxième lecture",
    LectureDéfinitive = "Lecture définitive",
    LectureTexteCmp = "Lecture texte CMP",
    LectureUnique = "Lecture unique",
    NouvelleLecture = "Nouvelle Lecture",
    The1ÈreLecture1ÈreAssembléeSaisie = "1ère lecture (1ère assemblée saisie)",
    The1ÈreLecture2ÈmeAssembléeSaisie = "1ère lecture (2ème assemblée saisie)",
}

export enum Etat {
    ADiscuter = "A discuter",
    ADéposer = "A déposer",
    Discuté = "Discuté",
    EnRecevabilité = "En recevabilité",
    EnTraitement = "En traitement",
    Irrecevable = "Irrecevable",
    Retiré = "Retiré",
}

export interface Identifiant {
    legislature: string;
    numero:      string;
    numRect:     string;
    saisine:     Saisine;
}

export interface Saisine {
    refTexteLegislatif:         string;
    numeroPartiePLF:            string;
    organeExamen:               string;
    mentionSecondeDeliberation: string;
}

export interface LoiReference {
    codeLoi:         PuneHedgehog | null | string;
    divisionCodeLoi: PuneHedgehog | null | string;
}

export interface PointeurFragmentTexte {
    missionVisee: MissionVisee;
    division:     Division;
    alinea:       Alinea;
}

export interface Alinea {
    "@xmlns:xsi"?:      string;
    "@xsi:nil"?:        string;
    avant_A_Apres?:     PuneHedgehog | AvantAApresEnum | null;
    numero?:            string;
    alineaDesignation?: PuneHedgehog | string;
}

export enum AvantAApresEnum {
    A = "A",
    Apres = "Apres",
    Après = "Après",
    Avant = "Avant",
}

export interface Division {
    titre:                    string;
    articleDesignationCourte: string;
    type:                     DivisionType;
    avant_A_Apres:            AvantAApresEnum;
    divisionRattachee:        string;
    articleAdditionnel:       PuneHedgehog | string;
    chapitreAdditionnel:      PuneHedgehog | string;
    urlDivisionTexteVise:     PuneHedgehog | string;
}

export enum DivisionType {
    Annexe = "ANNEXE",
    Article = "ARTICLE",
    Chapitre = "CHAPITRE",
    Titre = "TITRE",
}

export interface MissionVisee {
    "@xmlns:xsi"?:      string;
    "@xsi:nil"?:        string;
    idMissionAN?:       string;
    libelleMission?:    string;
    codeMissionPLF?:    CodeMissionPlf;
    libelleMissionPLF?: string;
}

export enum CodeMissionPlf {
    B = "B",
    C = "C",
    D = "D",
}

export interface Representations {
    representation: Representation;
}

export interface Representation {
    nom:                     Nom;
    typeMime:                TypeMime;
    statutRepresentation:    StatutRepresentation;
    repSource:               PuneHedgehog;
    offset:                  PuneHedgehog;
    contenu:                 Contenu;
    dateDispoRepresentation: PuneHedgehog;
}

export interface Contenu {
    documentURI: string;
}

export enum Nom {
    Pdf = "PDF",
}

export interface StatutRepresentation {
    verbatim:       string;
    canonique:      string;
    officielle:     string;
    transcription:  string;
    enregistrement: string;
}

export interface TypeMime {
    type:    TypeMimeType;
    subType: Nom;
}

export enum TypeMimeType {
    Application = "application",
}

export interface Signataires {
    auteur:          Auteur;
    cosignataires:   Cosignataires;
    texteAffichable: string;
}

export interface Auteur {
    typeAuteur:         TypeAuteur;
    acteurRef:          PuneHedgehog | string;
    organeRef:          PuneHedgehog | string;
    groupePolitiqueRef: PuneHedgehog | string;
}

export enum TypeAuteur {
    Depute = "Depute",
    Gouvernement = "Gouvernement",
    Rapporteur = "Rapporteur",
}

export interface Cosignataires {
    acteurRef?:    string[] | string;
    "@xmlns:xsi"?: string;
    "@xsi:nil"?:   string;
}

export interface Sort {
    dateSaisie?:   PuneHedgehog | Date;
    sortEnSeance?: SortEnSeance;
    "@xmlns:xsi"?: string;
    "@xsi:nil"?:   string;
}

export enum SortEnSeance {
    Adopté = "Adopté",
    NonSoutenu = "Non soutenu",
    Rejeté = "Rejeté",
    Retiré = "Retiré",
    Tombé = "Tombé",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toAmendementWrapper(json: string): AmendementWrapper {
        return cast(JSON.parse(json), r("AmendementWrapper"));
    }

    public static amendementWrapperToJson(value: AmendementWrapper): string {
        return JSON.stringify(uncast(value, r("AmendementWrapper")), null, 2);
    }

    public static toAmendements(json: string): Amendements {
        return cast(JSON.parse(json), r("Amendements"));
    }

    public static amendementsToJson(value: Amendements): string {
        return JSON.stringify(uncast(value, r("Amendements")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(_typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

// function m(additional: any) {
//     return { props: [], additional };
// }

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Amendements": o([
        { json: "textesEtAmendements", js: "textesEtAmendements", typ: r("TextesEtAmendements") },
    ], false),
    "TextesEtAmendements": o([
        { json: "texteleg", js: "texteleg", typ: a(r("Texteleg")) },
    ], false),
    "Texteleg": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "refTexteLegislatif", js: "refTexteLegislatif", typ: "" },
        { json: "amendements", js: "amendements", typ: r("AmendementsClass") },
    ], false),
    "AmendementsClass": o([
        { json: "amendement", js: "amendement", typ: u(a(r("Amendement")), r("Amendement")) },
        { json: "#text", js: "#text", typ: u(undefined, "") },
        { json: "offset", js: "offset", typ: u(undefined, null) },
        { json: "nom", js: "nom", typ: u(undefined, r("Nom")) },
        { json: "avant_A_Apres", js: "avant_A_Apres", typ: u(undefined, r("DivisionAvantAApres")) },
        { json: "dateDepot", js: "dateDepot", typ: u(undefined, Date) },
        { json: "documentURI", js: "documentURI", typ: u(undefined, "") },
    ], false),
    "AmendementWrapper": o([
        { json: "amendement", js: "amendement", typ: r("Amendement") },
    ], false),
    "Amendement": o([
        { json: "@xmlns", js: "@xmlns", typ: "" },
        { json: "uid", js: "uid", typ: "" },
        { json: "identifiant", js: "identifiant", typ: r("Identifiant") },
        { json: "numeroLong", js: "numeroLong", typ: "" },
        { json: "etapeTexte", js: "etapeTexte", typ: r("EtapeTexte") },
        { json: "triAmendement", js: "triAmendement", typ: "" },
        { json: "cardinaliteAmdtMultiples", js: "cardinaliteAmdtMultiples", typ: "" },
        { json: "amendementParent", js: "amendementParent", typ: u(r("PuneHedgehog"), "") },
        { json: "etat", js: "etat", typ: r("Etat") },
        { json: "signataires", js: "signataires", typ: r("Signataires") },
        { json: "pointeurFragmentTexte", js: "pointeurFragmentTexte", typ: r("PointeurFragmentTexte") },
        { json: "corps", js: "corps", typ: r("Corps") },
        { json: "representations", js: "representations", typ: r("Representations") },
        { json: "seanceDiscussion", js: "seanceDiscussion", typ: u(r("PuneHedgehog"), "") },
        { json: "sort", js: "sort", typ: r("Sort") },
        { json: "dateDepot", js: "dateDepot", typ: u(r("PuneHedgehog"), Date) },
        { json: "dateDistribution", js: "dateDistribution", typ: u(r("PuneHedgehog"), Date) },
        { json: "article99", js: "article99", typ: "" },
        { json: "loiReference", js: "loiReference", typ: r("LoiReference") },
    ], false),
    "PuneHedgehog": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:nil", js: "@xsi:nil", typ: "" },
    ], false),
    "Corps": o([
        { json: "dispositif", js: "dispositif", typ: u(undefined, "") },
        { json: "exposeSommaire", js: "exposeSommaire", typ: u(r("PuneHedgehog"), "") },
        { json: "annexeExposeSommaire", js: "annexeExposeSommaire", typ: r("PuneHedgehog") },
        { json: "cartoucheDelaiDepotDepasse", js: "cartoucheDelaiDepotDepasse", typ: u(undefined, u(r("PuneHedgehog"), "")) },
        { json: "avantAppel", js: "avantAppel", typ: u(undefined, r("AvantAppel")) },
        { json: "dispositifAmdtCredit", js: "dispositifAmdtCredit", typ: u(undefined, r("DispositifAmdtCredit")) },
    ], false),
    "AvantAppel": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
        { json: "dispositif", js: "dispositif", typ: u(undefined, "") },
    ], false),
    "DispositifAmdtCredit": o([
        { json: "listeProgrammes", js: "listeProgrammes", typ: r("ListeProgrammes") },
        { json: "totalAE", js: "totalAE", typ: r("Total") },
        { json: "totalCP", js: "totalCP", typ: r("Total") },
    ], false),
    "ListeProgrammes": o([
        { json: "programme", js: "programme", typ: u(a(r("ProgrammeElement")), r("ProgrammeElement")) },
    ], false),
    "ProgrammeElement": o([
        { json: "libelle", js: "libelle", typ: "" },
        { json: "id", js: "id", typ: "" },
        { json: "AE", js: "AE", typ: r("Ae") },
        { json: "CP", js: "CP", typ: r("Ae") },
        { json: "action", js: "action", typ: r("Action") },
        { json: "lignesCredits", js: "lignesCredits", typ: u(r("LignesCredits"), null) },
    ], false),
    "Ae": o([
        { json: "montantPositif", js: "montantPositif", typ: "" },
        { json: "montantNegatif", js: "montantNegatif", typ: "" },
    ], false),
    "LignesCredits": o([
        { json: "ligneCredit", js: "ligneCredit", typ: u(a(r("LigneCreditElement")), r("LigneCreditElement")) },
    ], false),
    "LigneCreditElement": o([
        { json: "id", js: "id", typ: "" },
        { json: "libelle", js: "libelle", typ: "" },
        { json: "AE", js: "AE", typ: r("Ae") },
        { json: "CP", js: "CP", typ: r("Ae") },
    ], false),
    "Total": o([
        { json: "montantPositif", js: "montantPositif", typ: "" },
        { json: "montantNegatif", js: "montantNegatif", typ: "" },
        { json: "solde", js: "solde", typ: "" },
    ], false),
    "Identifiant": o([
        { json: "legislature", js: "legislature", typ: "" },
        { json: "numero", js: "numero", typ: "" },
        { json: "numRect", js: "numRect", typ: "" },
        { json: "saisine", js: "saisine", typ: r("Saisine") },
    ], false),
    "Saisine": o([
        { json: "refTexteLegislatif", js: "refTexteLegislatif", typ: "" },
        { json: "numeroPartiePLF", js: "numeroPartiePLF", typ: "" },
        { json: "organeExamen", js: "organeExamen", typ: "" },
        { json: "mentionSecondeDeliberation", js: "mentionSecondeDeliberation", typ: "" },
    ], false),
    "LoiReference": o([
        { json: "codeLoi", js: "codeLoi", typ: u(r("PuneHedgehog"), null, "") },
        { json: "divisionCodeLoi", js: "divisionCodeLoi", typ: u(r("PuneHedgehog"), null, "") },
    ], false),
    "PointeurFragmentTexte": o([
        { json: "missionVisee", js: "missionVisee", typ: r("MissionVisee") },
        { json: "division", js: "division", typ: r("Division") },
        { json: "alinea", js: "alinea", typ: r("Alinea") },
    ], false),
    "Alinea": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
        { json: "avant_A_Apres", js: "avant_A_Apres", typ: u(undefined, u(r("PuneHedgehog"), r("AvantAApresEnum"), null)) },
        { json: "numero", js: "numero", typ: u(undefined, "") },
        { json: "alineaDesignation", js: "alineaDesignation", typ: u(undefined, u(r("PuneHedgehog"), "")) },
    ], false),
    "Division": o([
        { json: "titre", js: "titre", typ: "" },
        { json: "articleDesignationCourte", js: "articleDesignationCourte", typ: "" },
        { json: "type", js: "type", typ: r("DivisionType") },
        { json: "avant_A_Apres", js: "avant_A_Apres", typ: r("AvantAApresEnum") },
        { json: "divisionRattachee", js: "divisionRattachee", typ: "" },
        { json: "articleAdditionnel", js: "articleAdditionnel", typ: u(r("PuneHedgehog"), "") },
        { json: "chapitreAdditionnel", js: "chapitreAdditionnel", typ: u(r("PuneHedgehog"), "") },
        { json: "urlDivisionTexteVise", js: "urlDivisionTexteVise", typ: u(r("PuneHedgehog"), "") },
    ], false),
    "MissionVisee": o([
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
        { json: "idMissionAN", js: "idMissionAN", typ: u(undefined, "") },
        { json: "libelleMission", js: "libelleMission", typ: u(undefined, "") },
        { json: "codeMissionPLF", js: "codeMissionPLF", typ: u(undefined, r("CodeMissionPlf")) },
        { json: "libelleMissionPLF", js: "libelleMissionPLF", typ: u(undefined, "") },
    ], false),
    "Representations": o([
        { json: "representation", js: "representation", typ: r("Representation") },
    ], false),
    "Representation": o([
        { json: "nom", js: "nom", typ: r("Nom") },
        { json: "typeMime", js: "typeMime", typ: r("TypeMime") },
        { json: "statutRepresentation", js: "statutRepresentation", typ: r("StatutRepresentation") },
        { json: "repSource", js: "repSource", typ: r("PuneHedgehog") },
        { json: "offset", js: "offset", typ: r("PuneHedgehog") },
        { json: "contenu", js: "contenu", typ: r("Contenu") },
        { json: "dateDispoRepresentation", js: "dateDispoRepresentation", typ: r("PuneHedgehog") },
    ], false),
    "Contenu": o([
        { json: "documentURI", js: "documentURI", typ: "" },
    ], false),
    "StatutRepresentation": o([
        { json: "verbatim", js: "verbatim", typ: "" },
        { json: "canonique", js: "canonique", typ: "" },
        { json: "officielle", js: "officielle", typ: "" },
        { json: "transcription", js: "transcription", typ: "" },
        { json: "enregistrement", js: "enregistrement", typ: "" },
    ], false),
    "TypeMime": o([
        { json: "type", js: "type", typ: r("TypeMimeType") },
        { json: "subType", js: "subType", typ: r("Nom") },
    ], false),
    "Signataires": o([
        { json: "auteur", js: "auteur", typ: r("Auteur") },
        { json: "cosignataires", js: "cosignataires", typ: r("Cosignataires") },
        { json: "texteAffichable", js: "texteAffichable", typ: "" },
    ], false),
    "Auteur": o([
        { json: "typeAuteur", js: "typeAuteur", typ: r("TypeAuteur") },
        { json: "acteurRef", js: "acteurRef", typ: u(r("PuneHedgehog"), "") },
        { json: "organeRef", js: "organeRef", typ: u(r("PuneHedgehog"), "") },
        { json: "groupePolitiqueRef", js: "groupePolitiqueRef", typ: u(r("PuneHedgehog"), "") },
    ], false),
    "Cosignataires": o([
        { json: "acteurRef", js: "acteurRef", typ: u(undefined, u(a(""), "")) },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
    ], false),
    "Sort": o([
        { json: "dateSaisie", js: "dateSaisie", typ: u(undefined, u(r("PuneHedgehog"), Date)) },
        { json: "sortEnSeance", js: "sortEnSeance", typ: u(undefined, r("SortEnSeance")) },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "@xsi:nil", js: "@xsi:nil", typ: u(undefined, "") },
    ], false),
    "Action": [
        "creation",
        "modification",
        "suppression",
    ],
    "EtapeTexte": [
        "deuxième lecture",
        "Lecture définitive",
        "Lecture texte CMP",
        "Lecture unique",
        "Nouvelle Lecture",
        "1ère lecture (1ère assemblée saisie)",
        "1ère lecture (2ème assemblée saisie)",
    ],
    "Etat": [
        "A discuter",
        "A déposer",
        "Discuté",
        "En recevabilité",
        "En traitement",
        "Irrecevable",
        "Retiré",
    ],
    "AvantAApresEnum": [
        "A",
        "Apres",
        "Après",
        "Avant",
    ],
    "DivisionType": [
        "ANNEXE",
        "ARTICLE",
        "CHAPITRE",
        "TITRE",
    ],
    "CodeMissionPlf": [
        "B",
        "C",
        "D",
    ],
    "Nom": [
        "PDF",
    ],
    "TypeMimeType": [
        "application",
    ],
    "TypeAuteur": [
        "Depute",
        "Gouvernement",
        "Rapporteur",
    ],
    "SortEnSeance": [
        "Adopté",
        "Non soutenu",
        "Rejeté",
        "Retiré",
        "Tombé",
    ],
};
