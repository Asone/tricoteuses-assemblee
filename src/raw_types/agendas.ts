// To parse this data:
//
//   import { Convert, Reunion, Agendas } from "./file";
//
//   const reunion = Convert.toReunion(json);
//   const agendas = Convert.toAgendas(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

export interface Reunion {
    reunion: ReunionReunion;
}

export interface ReunionReunion {
    "@xmlns":                     string;
    "@xmlns:xsi":                 string;
    "@xsi:type":                  ReunionXsiType;
    uid:                          string;
    timeStampDebut:               Date;
    timeStampFin?:                string;
    lieu?:                        Lieu;
    cycleDeVie:                   CycleDeVie;
    demandeurs?:                  Demandeurs | null;
    organeReuniRef:               null | string;
    typeReunion?:                 TypeReunion;
    demandeur?:                   null;
    participants?:                PurpleParticipants | null;
    sessionRef?:                  SessionRef | null;
    ouverturePresse?:             string;
    captationVideo?:              string;
    ODJ?:                         Odj;
    compteRenduRef?:              null | string;
    formatReunion?:               FormatReunion;
    infosReunionsInternationale?: InfosReunionsInternationale;
    identifiants?:                Identifiants;
}

export enum ReunionXsiType {
    ReunionCommissionType = "reunionCommission_type",
    ReunionInitParlementaireType = "reunionInitParlementaire_type",
    SeanceType = "seance_type",
}

export interface Odj {
    convocationODJ: ConvocationOdjClass | null;
    resumeODJ:      ConvocationOdjClass | null;
    pointsODJ?:     PointsOdj | null;
}

export interface ConvocationOdjClass {
    item: string[] | string;
}

export interface PointsOdj {
    pointODJ: PointOdjElement[] | PointOdjElement;
}

export interface PointOdjElement {
    "@xsi:type":             PointOdjXsiType;
    uid:                     string;
    cycleDeVie:              CycleDeVie;
    objet:                   string;
    demandeurPoint:          null;
    procedure:               Procedure | null;
    dossiersLegislatifsRefs: DossiersLegislatifsRefs | null;
    typePointODJ:            TypePointOdj;
    comiteSecret:            string;
    textesAssocies:          null;
    natureTravauxODJ?:       NatureTravauxOdj;
    dateConfPres?:           string;
    dateLettreMinistre?:     string;
}

export enum PointOdjXsiType {
    PodjReunionType = "podjReunion_type",
    PodjSeanceConfPresType = "podjSeanceConfPres_type",
}

export interface CycleDeVie {
    etat:   Etat;
    chrono: Chrono;
}

export interface Chrono {
    creation: string;
    cloture:  null | string;
}

export enum Etat {
    Annulé = "Annulé",
    Confirmé = "Confirmé",
    Eventuel = "Eventuel",
    Supprimé = "Supprimé",
}

export interface DossiersLegislatifsRefs {
    dossierRef: string[] | string;
}

export enum NatureTravauxOdj {
    Odjpr = "ODJPR",
    Odjsn = "ODJSN",
}

export enum Procedure {
    DiscussionGénéraleCommune = "discussion générale commune",
    ProcédureDExamenSimplifiéeArticle103 = "procédure d'examen simplifiée-Article 103",
    ProcédureDExamenSimplifiéeArticle106 = "procédure d'examen simplifiée-Article 106",
    ProcédureDeLégislationEnCommissionArticle1071 = "procédure de législation en commission-Article 107-1",
}

export enum TypePointOdj {
    AmendementsArt88 = "Amendements (Art. 88)",
    AmendementsArt91 = "Amendements (Art. 91)",
    Audition = "Audition",
    AuditionMinistre = "Audition ministre",
    AuditionMinistreOuverteÀLaPresse = "Audition ministre ouverte à la presse",
    AuditionOuverteÀLaPresse = "Audition ouverte à la presse",
    Communication = "Communication",
    ConstitutionDeMissionDInformation = "Constitution de mission d'information",
    Discussion = "Discussion",
    DébatDInitiativeParlementaire = "Débat d'initiative parlementaire",
    DéclarationDuGouvernementSuivieDUnDébat = "Déclaration du Gouvernement suivie d'un débat",
    EchangesDeVues = "Echanges de vues",
    Examen = "Examen",
    ExplicationsDeVoteDesGroupesEtVoteParScrutinPublic = "Explications de vote des groupes et vote par scrutin public",
    ExplicationsDeVoteEtVoteParScrutinPublic = "Explications de vote et vote par scrutin public",
    FixationDeLOrdreDuJour = "Fixation de l'ordre du jour",
    NominationBureau = "Nomination bureau",
    NominationCandidatsOrganismeExtraparlementaire = "Nomination candidats organisme extraparlementaire",
    NominationDUnMembreDUneMissionDInformation = "Nomination d'un membre d'une mission d'information",
    NominationRapporteur = "Nomination rapporteur",
    NominationRapporteurDApplication = "Nomination rapporteur d'application",
    NominationRapporteurDInformation = "Nomination rapporteur d'information",
    NominationRapporteurPourAvis = "Nomination rapporteur pour avis",
    OuvertureEtClôtureDeSession = "Ouverture et clôture de session",
    QuestionsAuGouvernement = "Questions au Gouvernement",
    QuestionsOralesSansDébat = "Questions orales sans débat",
    Rapport = "Rapport",
    RapportDInformation = "Rapport d'information",
    RapportPourAvis = "Rapport pour avis",
    SuiteDeLaDiscussion = "Suite de la discussion",
    TableRonde = "Table ronde",
    VoteParScrutinPublic = "Vote par scrutin public",
    VoteSolennel = "Vote solennel",
}

export interface Demandeurs {
    acteur?: ActeurElement[] | ActeurElement;
    organe?: Organe;
}

export interface ActeurElement {
    nom:       null | string;
    acteurRef: string;
}

export interface Organe {
    nom:       string;
    organeRef: string;
}

export enum FormatReunion {
    AuditionExterne = "AuditionExterne",
    AuditionParPresidentCommission = "AuditionParPresidentCommission",
    Ordinaire = "Ordinaire",
}

export interface Identifiants {
    numSeanceJO: null | string;
    idJO:        null | string;
    quantieme:   Quantieme;
    DateSeance:  string;
}

export enum Quantieme {
    Deuxième = "Deuxième",
    Première = "Première",
    Troisième = "Troisième",
    Unique = "Unique",
}

export interface InfosReunionsInternationale {
    estReunionInternationale:    string;
    listePays:                   ListePays | null;
    informationsComplementaires: null | string;
}

export interface ListePays {
    paysRef: string[] | string;
}

export interface Lieu {
    code?:         Code | null;
    libelleCourt?: LibelleCourt | null;
    libelleLong:   null | string;
}

export enum Code {
    An = "AN",
    Cg = "CG",
    S = "S",
    Salreu001 = "SALREU001",
    Salreu002 = "SALREU002",
    Salreu003 = "SALREU003",
    Salreu004 = "SALREU004",
    Salreu005 = "SALREU005",
    Salreu006 = "SALREU006",
    Salreu007 = "SALREU007",
    Salreu008 = "SALREU008",
    Salreu009 = "SALREU009",
    Salreu010 = "SALREU010",
    Salreu011 = "SALREU011",
    Salreu012 = "SALREU012",
    Salreu013 = "SALREU013",
    Salreu014 = "SALREU014",
    Salreu015 = "SALREU015",
    Salreu016 = "SALREU016",
    Salreu017 = "SALREU017",
    Salreu018 = "SALREU018",
    Salreu019 = "SALREU019",
    Salreu020 = "SALREU020",
    Salreu021 = "SALREU021",
    Salreu022 = "SALREU022",
    Salreu023 = "SALREU023",
    Salreu024 = "SALREU024",
    Salreu025 = "SALREU025",
    Salreu026 = "SALREU026",
    Salreu027 = "SALREU027",
    Salreu028 = "SALREU028",
    Salreu029 = "SALREU029",
    Salreu032 = "SALREU032",
    Salreu033 = "SALREU033",
    Salreu037 = "SALREU037",
    Salreu038 = "SALREU038",
    Salreu039 = "SALREU039",
    Salreu041 = "SALREU041",
    Salreu042 = "SALREU042",
    Salreu044 = "SALREU044",
    Salreu045 = "SALREU045",
    Salreu046 = "SALREU046",
    Salreu048 = "SALREU048",
    Salreu049 = "SALREU049",
    Salreu050 = "SALREU050",
}

export enum LibelleCourt {
    Salle6217 = "Salle 6217",
    Salle6237 = "Salle 6237",
    Salle6549 = "Salle 6549",
    Salle6550 = "Salle 6550",
    Salle6566 = "Salle 6566",
    Salle6695 = "Salle 6695",
    Salle6809 = "Salle 6809",
    Salle6822 = "Salle 6822",
    Salle6990 = "Salle 6990",
    Salle7042 = "Salle 7042",
    Salle7044 = "Salle 7044",
    Salle7070 = "Salle 7070",
    Salle7207 = "Salle 7207",
    Salle7326 = "Salle 7326",
    Salle7426 = "Salle 7426",
    SalleColbert = "Salle Colbert",
    SalleLamartine = "Salle Lamartine",
    SalleN1 = "Salle N° 1",
    SalleN2 = "Salle N° 2",
    SalleN23 = "Salle N° 23",
    SalleN3 = "Salle N° 3",
    SalleVictorHugo = "Salle Victor Hugo",
    SalonGabriel = "Salon Gabriel",
    SalonMansart = "Salon Mansart",
    SalonMarsI = "Salon Mars I",
    SalonMarsIi = "Salon Mars II",
    SalonMarsIii = "Salon Mars III",
    SalonVisconti = "Salon Visconti",
    Sully1 = "Sully 1",
    Sully2 = "Sully 2",
    Sully3 = "Sully 3",
    Sully4 = "Sully 4",
    Sully5 = "Sully 5",
    The1ErBureau = "1er Bureau",
    The2ÈmeBureau = "2ème Bureau",
    The3ÈmeBureau = "3ème Bureau",
    The4ÈmeBureau = "4ème Bureau",
    The5ÈmeBureau = "5ème Bureau",
    The6ÈmeBureau = "6ème Bureau",
    The7ÈmeBureau = "7ème Bureau",
    The8ÈmeBureau = "8ème Bureau",
    The9ÈmeBureau = "9ème Bureau",
}

export interface PurpleParticipants {
    participantsInternes:  PurpleParticipantsInternes | null;
    personnesAuditionnees: PurplePersonnesAuditionnees | null;
}

export interface PurpleParticipantsInternes {
    participantInterne: ParticipantInterneElement[] | ParticipantInterneElement;
}

export interface ParticipantInterneElement {
    acteurRef: string;
    presence:  Presence;
}

export enum Presence {
    Absent = "absent",
    Excusé = "excusé",
    Présent = "présent",
}

export interface PurplePersonnesAuditionnees {
    personneAuditionnee: null[] | null;
}

export enum SessionRef {
    Scr5A2017E1 = "SCR5A2017E1",
    Scr5A2017E2 = "SCR5A2017E2",
    Scr5A2017I1 = "SCR5A2017I1",
    Scr5A2017I2 = "SCR5A2017I2",
    Scr5A2017I3 = "SCR5A2017I3",
    Scr5A2017O1 = "SCR5A2017O1",
    Scr5A2018E1 = "SCR5A2018E1",
    Scr5A2018E2 = "SCR5A2018E2",
    Scr5A2018O1 = "SCR5A2018O1",
    Scr5A2019E1 = "SCR5A2019E1",
    Scr5A2019E2 = "SCR5A2019E2",
    Scr5A2019O1 = "SCR5A2019O1",
    Scr5A2020O1 = "SCR5A2020O1",
}

export enum TypeReunion {
    Dep = "DEP",
    Ga = "GA",
    Ge = "GE",
    Gevi = "GEVI",
    Gp = "GP",
    HéAurélien = "HÉ Aurélien",
}

export interface Agendas {
    reunions: Reunions;
}

export interface Reunions {
    reunion: ReunionElement[];
}

export interface ReunionElement {
    uid:              string;
    timeStampDebut:   Date;
    timeStampFin?:    string;
    lieu:             Lieu;
    cycleDeVie:       CycleDeVie;
    demandeurs?:      Demandeurs | null;
    organeReuniRef:   null | string;
    typeReunion?:     TypeReunion;
    "@xmlns:xsi"?:    string;
    demandeur?:       null;
    participants?:    FluffyParticipants | null;
    sessionRef?:      SessionRef | null;
    ouverturePresse?: string;
    ODJ?:             Odj;
    compteRenduRef?:  null | string;
    identifiants?:    Identifiants;
}

export interface FluffyParticipants {
    participantsInternes:  FluffyParticipantsInternes | null;
    personnesAuditionnees: FluffyPersonnesAuditionnees | null;
}

export interface FluffyParticipantsInternes {
    participantInterne: ParticipantInterneElement[];
}

export interface FluffyPersonnesAuditionnees {
    personneAuditionnee: PersonneAuditionneeElement[] | PersonneAuditionneeElement;
}

export interface PersonneAuditionneeElement {
    uid:      Uid;
    ident:    Ident;
    dateNais: Date | null;
}

export interface Ident {
    civ:        Civ;
    prenom:     string;
    nom:        string;
    alpha?:     string;
    trigramme?: null | string;
}

export enum Civ {
    M = "M.",
    Mme = "Mme",
}

export interface Uid {
    "@xsi:type": UidXsiType;
    "#text":     string;
}

export enum UidXsiType {
    IdActeurType = "IdActeur_type",
    IdPersonneExterneType = "IdPersonneExterne_type",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toReunion(json: string): Reunion {
        return cast(JSON.parse(json), r("Reunion"));
    }

    public static reunionToJson(value: Reunion): string {
        return JSON.stringify(uncast(value, r("Reunion")), null, 2);
    }

    public static toAgendas(json: string): Agendas {
        return cast(JSON.parse(json), r("Agendas"));
    }

    public static agendasToJson(value: Agendas): string {
        return JSON.stringify(uncast(value, r("Agendas")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(_typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

// function m(additional: any) {
//     return { props: [], additional };
// }

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Reunion": o([
        { json: "reunion", js: "reunion", typ: r("ReunionReunion") },
    ], false),
    "ReunionReunion": o([
        { json: "@xmlns", js: "@xmlns", typ: "" },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: "" },
        { json: "@xsi:type", js: "@xsi:type", typ: r("ReunionXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "timeStampDebut", js: "timeStampDebut", typ: Date },
        { json: "timeStampFin", js: "timeStampFin", typ: u(undefined, "") },
        { json: "lieu", js: "lieu", typ: u(undefined, r("Lieu")) },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "demandeurs", js: "demandeurs", typ: u(undefined, u(r("Demandeurs"), null)) },
        { json: "organeReuniRef", js: "organeReuniRef", typ: u(null, "") },
        { json: "typeReunion", js: "typeReunion", typ: u(undefined, r("TypeReunion")) },
        { json: "demandeur", js: "demandeur", typ: u(undefined, null) },
        { json: "participants", js: "participants", typ: u(undefined, u(r("PurpleParticipants"), null)) },
        { json: "sessionRef", js: "sessionRef", typ: u(undefined, u(r("SessionRef"), null)) },
        { json: "ouverturePresse", js: "ouverturePresse", typ: u(undefined, "") },
        { json: "captationVideo", js: "captationVideo", typ: u(undefined, "") },
        { json: "ODJ", js: "ODJ", typ: u(undefined, r("Odj")) },
        { json: "compteRenduRef", js: "compteRenduRef", typ: u(undefined, u(null, "")) },
        { json: "formatReunion", js: "formatReunion", typ: u(undefined, r("FormatReunion")) },
        { json: "infosReunionsInternationale", js: "infosReunionsInternationale", typ: u(undefined, r("InfosReunionsInternationale")) },
        { json: "identifiants", js: "identifiants", typ: u(undefined, r("Identifiants")) },
    ], false),
    "Odj": o([
        { json: "convocationODJ", js: "convocationODJ", typ: u(r("ConvocationOdjClass"), null) },
        { json: "resumeODJ", js: "resumeODJ", typ: u(r("ConvocationOdjClass"), null) },
        { json: "pointsODJ", js: "pointsODJ", typ: u(undefined, u(r("PointsOdj"), null)) },
    ], false),
    "ConvocationOdjClass": o([
        { json: "item", js: "item", typ: u(a(""), "") },
    ], false),
    "PointsOdj": o([
        { json: "pointODJ", js: "pointODJ", typ: u(a(r("PointOdjElement")), r("PointOdjElement")) },
    ], false),
    "PointOdjElement": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("PointOdjXsiType") },
        { json: "uid", js: "uid", typ: "" },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "objet", js: "objet", typ: "" },
        { json: "demandeurPoint", js: "demandeurPoint", typ: null },
        { json: "procedure", js: "procedure", typ: u(r("Procedure"), null) },
        { json: "dossiersLegislatifsRefs", js: "dossiersLegislatifsRefs", typ: u(r("DossiersLegislatifsRefs"), null) },
        { json: "typePointODJ", js: "typePointODJ", typ: r("TypePointOdj") },
        { json: "comiteSecret", js: "comiteSecret", typ: "" },
        { json: "textesAssocies", js: "textesAssocies", typ: null },
        { json: "natureTravauxODJ", js: "natureTravauxODJ", typ: u(undefined, r("NatureTravauxOdj")) },
        { json: "dateConfPres", js: "dateConfPres", typ: u(undefined, "") },
        { json: "dateLettreMinistre", js: "dateLettreMinistre", typ: u(undefined, "") },
    ], false),
    "CycleDeVie": o([
        { json: "etat", js: "etat", typ: r("Etat") },
        { json: "chrono", js: "chrono", typ: r("Chrono") },
    ], false),
    "Chrono": o([
        { json: "creation", js: "creation", typ: "" },
        { json: "cloture", js: "cloture", typ: u(null, "") },
    ], false),
    "DossiersLegislatifsRefs": o([
        { json: "dossierRef", js: "dossierRef", typ: u(a(""), "") },
    ], false),
    "Demandeurs": o([
        { json: "acteur", js: "acteur", typ: u(undefined, u(a(r("ActeurElement")), r("ActeurElement"))) },
        { json: "organe", js: "organe", typ: u(undefined, r("Organe")) },
    ], false),
    "ActeurElement": o([
        { json: "nom", js: "nom", typ: u(null, "") },
        { json: "acteurRef", js: "acteurRef", typ: "" },
    ], false),
    "Organe": o([
        { json: "nom", js: "nom", typ: "" },
        { json: "organeRef", js: "organeRef", typ: "" },
    ], false),
    "Identifiants": o([
        { json: "numSeanceJO", js: "numSeanceJO", typ: u(null, "") },
        { json: "idJO", js: "idJO", typ: u(null, "") },
        { json: "quantieme", js: "quantieme", typ: r("Quantieme") },
        { json: "DateSeance", js: "DateSeance", typ: "" },
    ], false),
    "InfosReunionsInternationale": o([
        { json: "estReunionInternationale", js: "estReunionInternationale", typ: "" },
        { json: "listePays", js: "listePays", typ: u(r("ListePays"), null) },
        { json: "informationsComplementaires", js: "informationsComplementaires", typ: u(null, "") },
    ], false),
    "ListePays": o([
        { json: "paysRef", js: "paysRef", typ: u(a(""), "") },
    ], false),
    "Lieu": o([
        { json: "code", js: "code", typ: u(undefined, u(r("Code"), null)) },
        { json: "libelleCourt", js: "libelleCourt", typ: u(undefined, u(r("LibelleCourt"), null)) },
        { json: "libelleLong", js: "libelleLong", typ: u(null, "") },
    ], false),
    "PurpleParticipants": o([
        { json: "participantsInternes", js: "participantsInternes", typ: u(r("PurpleParticipantsInternes"), null) },
        { json: "personnesAuditionnees", js: "personnesAuditionnees", typ: u(r("PurplePersonnesAuditionnees"), null) },
    ], false),
    "PurpleParticipantsInternes": o([
        { json: "participantInterne", js: "participantInterne", typ: u(a(r("ParticipantInterneElement")), r("ParticipantInterneElement")) },
    ], false),
    "ParticipantInterneElement": o([
        { json: "acteurRef", js: "acteurRef", typ: "" },
        { json: "presence", js: "presence", typ: r("Presence") },
    ], false),
    "PurplePersonnesAuditionnees": o([
        { json: "personneAuditionnee", js: "personneAuditionnee", typ: u(a(null), null) },
    ], false),
    "Agendas": o([
        { json: "reunions", js: "reunions", typ: r("Reunions") },
    ], false),
    "Reunions": o([
        { json: "reunion", js: "reunion", typ: a(r("ReunionElement")) },
    ], false),
    "ReunionElement": o([
        { json: "uid", js: "uid", typ: "" },
        { json: "timeStampDebut", js: "timeStampDebut", typ: Date },
        { json: "timeStampFin", js: "timeStampFin", typ: u(undefined, "") },
        { json: "lieu", js: "lieu", typ: r("Lieu") },
        { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
        { json: "demandeurs", js: "demandeurs", typ: u(undefined, u(r("Demandeurs"), null)) },
        { json: "organeReuniRef", js: "organeReuniRef", typ: u(null, "") },
        { json: "typeReunion", js: "typeReunion", typ: u(undefined, r("TypeReunion")) },
        { json: "@xmlns:xsi", js: "@xmlns:xsi", typ: u(undefined, "") },
        { json: "demandeur", js: "demandeur", typ: u(undefined, null) },
        { json: "participants", js: "participants", typ: u(undefined, u(r("FluffyParticipants"), null)) },
        { json: "sessionRef", js: "sessionRef", typ: u(undefined, u(r("SessionRef"), null)) },
        { json: "ouverturePresse", js: "ouverturePresse", typ: u(undefined, "") },
        { json: "ODJ", js: "ODJ", typ: u(undefined, r("Odj")) },
        { json: "compteRenduRef", js: "compteRenduRef", typ: u(undefined, u(null, "")) },
        { json: "identifiants", js: "identifiants", typ: u(undefined, r("Identifiants")) },
    ], false),
    "FluffyParticipants": o([
        { json: "participantsInternes", js: "participantsInternes", typ: u(r("FluffyParticipantsInternes"), null) },
        { json: "personnesAuditionnees", js: "personnesAuditionnees", typ: u(r("FluffyPersonnesAuditionnees"), null) },
    ], false),
    "FluffyParticipantsInternes": o([
        { json: "participantInterne", js: "participantInterne", typ: a(r("ParticipantInterneElement")) },
    ], false),
    "FluffyPersonnesAuditionnees": o([
        { json: "personneAuditionnee", js: "personneAuditionnee", typ: u(a(r("PersonneAuditionneeElement")), r("PersonneAuditionneeElement")) },
    ], false),
    "PersonneAuditionneeElement": o([
        { json: "uid", js: "uid", typ: r("Uid") },
        { json: "ident", js: "ident", typ: r("Ident") },
        { json: "dateNais", js: "dateNais", typ: u(Date, null) },
    ], false),
    "Ident": o([
        { json: "civ", js: "civ", typ: r("Civ") },
        { json: "prenom", js: "prenom", typ: "" },
        { json: "nom", js: "nom", typ: "" },
        { json: "alpha", js: "alpha", typ: u(undefined, "") },
        { json: "trigramme", js: "trigramme", typ: u(undefined, u(null, "")) },
    ], false),
    "Uid": o([
        { json: "@xsi:type", js: "@xsi:type", typ: r("UidXsiType") },
        { json: "#text", js: "#text", typ: "" },
    ], false),
    "ReunionXsiType": [
        "reunionCommission_type",
        "reunionInitParlementaire_type",
        "seance_type",
    ],
    "PointOdjXsiType": [
        "podjReunion_type",
        "podjSeanceConfPres_type",
    ],
    "Etat": [
        "Annulé",
        "Confirmé",
        "Eventuel",
        "Supprimé",
    ],
    "NatureTravauxOdj": [
        "ODJPR",
        "ODJSN",
    ],
    "Procedure": [
        "discussion générale commune",
        "procédure d'examen simplifiée-Article 103",
        "procédure d'examen simplifiée-Article 106",
        "procédure de législation en commission-Article 107-1",
    ],
    "TypePointOdj": [
        "Amendements (Art. 88)",
        "Amendements (Art. 91)",
        "Audition",
        "Audition ministre",
        "Audition ministre ouverte à la presse",
        "Audition ouverte à la presse",
        "Communication",
        "Constitution de mission d'information",
        "Discussion",
        "Débat d'initiative parlementaire",
        "Déclaration du Gouvernement suivie d'un débat",
        "Echanges de vues",
        "Examen",
        "Explications de vote des groupes et vote par scrutin public",
        "Explications de vote et vote par scrutin public",
        "Fixation de l'ordre du jour",
        "Nomination bureau",
        "Nomination candidats organisme extraparlementaire",
        "Nomination d'un membre d'une mission d'information",
        "Nomination rapporteur",
        "Nomination rapporteur d'application",
        "Nomination rapporteur d'information",
        "Nomination rapporteur pour avis",
        "Ouverture et clôture de session",
        "Questions au Gouvernement",
        "Questions orales sans débat",
        "Rapport",
        "Rapport d'information",
        "Rapport pour avis",
        "Suite de la discussion",
        "Table ronde",
        "Vote par scrutin public",
        "Vote solennel",
    ],
    "FormatReunion": [
        "AuditionExterne",
        "AuditionParPresidentCommission",
        "Ordinaire",
    ],
    "Quantieme": [
        "Deuxième",
        "Première",
        "Troisième",
        "Unique",
    ],
    "Code": [
        "AN",
        "CG",
        "S",
        "SALREU001",
        "SALREU002",
        "SALREU003",
        "SALREU004",
        "SALREU005",
        "SALREU006",
        "SALREU007",
        "SALREU008",
        "SALREU009",
        "SALREU010",
        "SALREU011",
        "SALREU012",
        "SALREU013",
        "SALREU014",
        "SALREU015",
        "SALREU016",
        "SALREU017",
        "SALREU018",
        "SALREU019",
        "SALREU020",
        "SALREU021",
        "SALREU022",
        "SALREU023",
        "SALREU024",
        "SALREU025",
        "SALREU026",
        "SALREU027",
        "SALREU028",
        "SALREU029",
        "SALREU032",
        "SALREU033",
        "SALREU037",
        "SALREU038",
        "SALREU039",
        "SALREU041",
        "SALREU042",
        "SALREU044",
        "SALREU045",
        "SALREU046",
        "SALREU048",
        "SALREU049",
        "SALREU050",
    ],
    "LibelleCourt": [
        "Salle 6217",
        "Salle 6237",
        "Salle 6549",
        "Salle 6550",
        "Salle 6566",
        "Salle 6695",
        "Salle 6809",
        "Salle 6822",
        "Salle 6990",
        "Salle 7042",
        "Salle 7044",
        "Salle 7070",
        "Salle 7207",
        "Salle 7326",
        "Salle 7426",
        "Salle Colbert",
        "Salle Lamartine",
        "Salle N° 1",
        "Salle N° 2",
        "Salle N° 23",
        "Salle N° 3",
        "Salle Victor Hugo",
        "Salon Gabriel",
        "Salon Mansart",
        "Salon Mars I",
        "Salon Mars II",
        "Salon Mars III",
        "Salon Visconti",
        "Sully 1",
        "Sully 2",
        "Sully 3",
        "Sully 4",
        "Sully 5",
        "1er Bureau",
        "2ème Bureau",
        "3ème Bureau",
        "4ème Bureau",
        "5ème Bureau",
        "6ème Bureau",
        "7ème Bureau",
        "8ème Bureau",
        "9ème Bureau",
    ],
    "Presence": [
        "absent",
        "excusé",
        "présent",
    ],
    "SessionRef": [
        "SCR5A2017E1",
        "SCR5A2017E2",
        "SCR5A2017I1",
        "SCR5A2017I2",
        "SCR5A2017I3",
        "SCR5A2017O1",
        "SCR5A2018E1",
        "SCR5A2018E2",
        "SCR5A2018O1",
        "SCR5A2019E1",
        "SCR5A2019E2",
        "SCR5A2019O1",
        "SCR5A2020O1",
    ],
    "TypeReunion": [
        "DEP",
        "GA",
        "GE",
        "GEVI",
        "GP",
        "HÉ Aurélien",
    ],
    "Civ": [
        "M.",
        "Mme",
    ],
    "UidXsiType": [
        "IdActeur_type",
        "IdPersonneExterne_type",
    ],
};
