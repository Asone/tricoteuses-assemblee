import assert from "assert"
import { JSDOM } from "jsdom"

export function parseTexteLoi(assembleeUrl: string, page: string) {
  // Repair HTML.
  const pageSegments = page.split("</style>")
  assert.strictEqual(pageSegments.length, 2, page)
  let html = pageSegments[1].trim()
  assert(html.startsWith("<div"))
  assert(html.endsWith("</div>"))
  html = `<html${html.substring(4, html.length - 6)}</html>`

  // Extract subdivisions from HTML.
  const { window } = new JSDOM(html)
  const { document } = window
  assert.strictEqual(document.children.length, 1)
  const htmlElement = document.children[0]
  assert.strictEqual(htmlElement.children.length, 2)
  const bodyElement = htmlElement.children[1]
  if (assembleeUrl === "http://www.assemblee-nationale.fr/15/textes/0675.asp") {
    // Repair document.
    assert.strictEqual(bodyElement.children.length, 1)
    const divElement = bodyElement.children[0]
    assert.strictEqual(divElement.tagName, "DIV")
    const newDivElement = bodyElement.insertBefore(
      document.createElement("div"),
      divElement,
    )
    bodyElement.insertBefore(document.createElement("br"), divElement)
    for (const divChild of [...divElement.children]) {
      if (divChild.getAttribute("class") === "a9ArticleNum") {
        break
      }
      newDivElement.appendChild(divChild)
    }
  }
  if (bodyElement.children.length < 3) {
    // Occurs in http://www.assemblee-nationale.fr/15/textes/0326.asp.
    window.close() // Free memory.
    return {
      error: { code: -1, message: "Texte de loi sans contenu" },
      html,
      page,
    }
  }
  assert.strictEqual(bodyElement.children[0].tagName, "DIV")
  // First child is a DIV describing the document (Assemblée's header). Skip it for now.
  let bodyChild = bodyElement.children[1]
  assert.strictEqual(bodyChild.tagName, "BR")
  let alineaElement: Element | null = null
  let isMultiLinesHeader: boolean = false
  let level: number | null = null
  let levels: number[] = []
  let lineElements: Element[] | null = null
  let nextParentState = null
  let paragraphChild: Element | null = null
  let paragraphElement: Element | null = null
  let state: string | null = "nextBodyChild"
  let subAlineaElement: Element | null = null
  const subdivisions = []
  let subdivisionAlineasHtml: string[] | null = null
  let subdivisionAlineasText: string[] | null = null
  let subdivisionHeadersHtml: string[] | null = null
  let subdivisionHeadersText: string[] | null = null
  while (state !== null) {
    switch (state) {
      case "alineaElement":
        switch (alineaElement!.tagName) {
          case "DIV":
            state = "firstSubAlineaElement"
            break
          case "H4":
            // TODO
            nextParentState = "nextAlineaElement"
            paragraphElement = alineaElement
            state = "firstParagraphChild"
            break
          case "P":
            nextParentState = "nextAlineaElement"
            paragraphElement = alineaElement
            state = "firstParagraphChild"
            break
          case "OL":
            // TODO
            state = "nextAlineaElement"
            break
          case "TABLE":
            // TODO
            state = "nextAlineaElement"
            break
          default:
            throw `Unexpected tag name for alinea element: ${alineaElement!.tagName}`
        }
        break
      case "firstAlineaElement":
        alineaElement = bodyChild.children[0]
        if (alineaElement === undefined) {
          // No alinea in current bodyChild: go to next bodyChild.
          state = "nextBodyChild"
        } else {
          state = "alineaElement"
        }
        break
      case "firstParagraphChild":
        paragraphChild = paragraphElement!.children[0]
        if (paragraphChild === undefined) {
          // No child in current paragraph: go to next parent element.
          state = nextParentState
        } else {
          const headerText = paragraphElement!.textContent
          const nameComputed = (headerText || "")
            .normalize("NFD")
            .replace(/[\u0300-\u036f]/g, "")
            .replace(/\(nouveau\)/, "")
            .replace(/\(Pour coordination\)/, "")
            .replace(/\(Supprimés?\)/, "")
            .replace(/ /g, " ")
            .replace(/[\-,.…]/g, "")
            .trim()
            .replace(/ {1,}/g, "_")
          const nameUpper = nameComputed.toUpperCase()
          if (nameUpper === "") {
            // Occurs in:
            // * http://www.assemblee-nationale.fr/15/textes/0445.asp
            // * http://www.assemblee-nationale.fr/15/textes/0626.asp
            // Skip paragraph.
            state = nextParentState
            break
          }
          const nextLevel =
            nameUpper.match(/^(RAPPORT_)?ANNEXE(_|$)/) !== null ||
            nameUpper.match(/^ETAT_/) !== null
              ? 0
              : nameUpper.match(/^TOME_/) !== null
              ? 1
              : nameUpper.match(/^PARTIE_/) !== null ||
                nameUpper.match(
                  /^(PREMIERE|SECONDE|DEUXIEME|TROISIEME|QUATRIEME)_PARTIE(_|$)/,
                ) !== null
              ? 2
              : nameUpper.match(/^LIVRE_/) !== null
              ? 3
              : nameUpper.match(/^TITRE_/) !== null
              ? 4
              : nameUpper.match(/^SOUSTITRE_/) !== null
              ? 5
              : nameUpper.match(/^CHAPITRE_/) !== null
              ? 6
              : nameUpper.match(/^SECTION_/) !== null
              ? 7
              : nameUpper.match(/^SOUSSECTION_/) !== null
              ? 8
              : nameUpper.match(/^ARTICLES?_/) !== null ||
                // "Exposé des motifs" must be at the same level as article.
                // "Exposé des motifs" without content:
                // * http://www.assemblee-nationale.fr/15/textes/0702.asp
                // "Exposé des motifs" with content:
                // * http://www.assemblee-nationale.fr/15/textes/1326.asp
                // * http://www.assemblee-nationale.fr/15/textes/1610.asp
                nameUpper.match(/^EXPOSE_DES_MOTIFS$/) !== null
              ? 9
              : null
          if (nextLevel === null && subdivisions.length === 0) {
            if (nameUpper.match(/^(PROJET|PROPOSITION)_DE_LOI(_|$)/) !== null) {
              // Occurs in:
              // * http://www.assemblee-nationale.fr/15/textes/0232.asp
              // * http://www.assemblee-nationale.fr/15/textes/0626.asp
              // * http://www.assemblee-nationale.fr/15/textes/0676.asp
            } else if (nameUpper === "JEUX_OLYMPIQUES_ET_PARALYMPIQUES_DE_2024") {
              // Occurs in http://www.assemblee-nationale.fr/15/textes/0676.asp
            } else if (nameUpper === "TEXTE_DE_LA_COMMISSION_MIXTE_PARITAIRE") {
              // Occurs in http://www.assemblee-nationale.fr/15/textes/1294.asp
            } else {
              console.log(
                `Unexpected nameUpper = "${nameUpper}" at beginning of "texte de loi" at ${assembleeUrl}`,
              )
            }
            // Skip paragraph.
            state = nextParentState
            break
          }
          if (nextLevel !== null) {
            level = nextLevel
            while (levels.length > 0 && level < levels[levels.length - 1]) {
              levels.pop()
            }
            if (levels.length === 0 || level > levels[levels.length - 1]) {
              levels.push(level)
            }
            // Articles & "Exposé des motifs" are the only divisions without second title.
            isMultiLinesHeader =
              nameUpper.match(/^ARTICLES?_/) === null &&
              nameUpper.match(/^EXPOSE_DES_MOTIFS$/) === null
            subdivisionAlineasHtml = []
            subdivisionAlineasText = []
            subdivisionHeadersHtml = []
            subdivisionHeadersText = []
            subdivisions.push({
              html: {
                alineas: subdivisionAlineasHtml,
                headers: subdivisionHeadersHtml,
              },
              id: "D_" + nameComputed,
              level: level + 1,
              relativeLevel: levels.length,
              text: {
                alineas: subdivisionAlineasText,
                headers: subdivisionHeadersText,
              },
            })
          }
          lineElements = []
          state = "paragraphChild"
        }
        break
      case "firstSubAlineaElement":
        subAlineaElement = alineaElement!.children[0]
        if (subAlineaElement === undefined) {
          // No sub-alinea in current alinea: go to next alinea.
          state = "nextAlineaElement"
        } else {
          state = "subAlineaElement"
        }
        break
      case "nextAlineaElement":
        alineaElement = alineaElement!.nextElementSibling
        if (alineaElement === null) {
          // The bodyChild has been fully parsed. Go to next bodyChild
          state = "nextBodyChild"
        } else {
          state = "alineaElement"
        }
        break
      case "nextBodyChild":
        if (bodyChild.nextElementSibling === null) {
          // The document has been fully parsed.
          state = null
        } else {
          bodyChild = bodyChild.nextElementSibling
          if (bodyChild.tagName === "DIV") {
            state = "firstAlineaElement"
          } else {
            // <P/> is for footnotes
            assert(
              ["BR", "HR", "P"].includes(bodyChild.tagName),
              `Ùnexpected tag name "${bodyChild.tagName}" for body child`,
            )
            // Stay in the same state to go to next bodyChild.
          }
        }
        break
      case "nextParagraphChild":
        paragraphChild = paragraphChild!.nextElementSibling
        if (paragraphChild === null) {
          // The paragraph element has been fully parsed. Go to next parent element.
          const lineText = lineElements!
            .map(element => element.textContent)
            .join("")
            .trim()
          if (lineText) {
            const lineHtml = lineElements!.map(element => element.outerHTML).join("")
            if (
              subdivisionHeadersText!.length === 0 ||
              (isMultiLinesHeader && subdivisionAlineasText!.length === 0)
            ) {
              subdivisionHeadersHtml!.push(lineHtml)
              subdivisionHeadersText!.push(lineText)
            } else {
              subdivisionAlineasHtml!.push(lineHtml)
              subdivisionAlineasText!.push(lineText)
            }
          }
          if (isMultiLinesHeader && subdivisionHeadersText!.length >= 2) {
            isMultiLinesHeader = false
          }
          lineElements = null
          state = nextParentState
        } else {
          state = "paragraphChild"
        }
        break
      case "nextSubAlineaElement":
        subAlineaElement = subAlineaElement!.nextElementSibling
        if (subAlineaElement === null) {
          // The alineaElement has been fully parsed. Go to next alineaElement.
          state = "nextAlineaElement"
        } else {
          state = "subAlineaElement"
        }
        break
      case "paragraphChild":
        switch (paragraphChild!.tagName) {
          case "A": {
            lineElements!.push(paragraphChild!)
            break
          }
          case "BR": {
            const lineText = lineElements!
              .map(element => element.textContent)
              .join("")
              .trim()
            if (lineText) {
              const lineHtml = lineElements!.map(element => element.outerHTML).join("")
              if (
                subdivisionHeadersText!.length === 0 ||
                (isMultiLinesHeader && subdivisionAlineasText!.length === 0)
              ) {
                subdivisionHeadersHtml!.push(lineHtml)
                subdivisionHeadersText!.push(lineText)
              } else {
                subdivisionAlineasHtml!.push(lineHtml)
                subdivisionAlineasText!.push(lineText)
              }
            }
            lineElements = []
            break
          }
          case "DEL": {
            lineElements!.push(paragraphChild!)
            break
          }
          case "IMG": {
            lineElements!.push(paragraphChild!)
            break
          }
          case "SPAN": {
            const className = paragraphChild!.getAttribute("class")
            switch (className) {
              case "aEloiPastille": {
                // Skip pastillé SPAN.
                break
              }
              case "aEloiPastille0": {
                // Skip pastillé SPAN.
                break
              }
              default:
                lineElements!.push(paragraphChild!)
            }
            break
          }
          default:
            throw `Unexpected tag name for paragraph child: ${paragraphChild!.tagName}`
        }
        state = "nextParagraphChild"
        break
      case "subAlineaElement":
        switch (subAlineaElement!.tagName) {
          case "P":
            nextParentState = "nextSubAlineaElement"
            paragraphElement = subAlineaElement
            state = "firstParagraphChild"
            break
          case "TABLE":
            // TODO
            state = "nextSubAlineaElement"
            break
          default:
            throw `Unexpected tag name for sub alinea element: ${
              subAlineaElement!.tagName
            }`
        }
        break
        state = "nextSubAlineaElement"
        break
      default:
        throw `Unexpected state: ${state}`
    }
  }

  window.close() // Free memory.

  return {
    error: null,
    html,
    page,
    subdivisions,
    url: assembleeUrl,
  }
}

export async function retrieveTexteLoiParsed(
  fetch: (url: string | Request, init?: RequestInit | undefined) => Promise<Response>,
  assembleeUrl: string,
) {
  const response = await fetch(assembleeUrl)
  const page = await response.text()
  if (!response.ok) {
    return {
      error: { code: response.status, message: response.statusText },
      page,
    }
  }
  return parseTexteLoi(assembleeUrl, page)
}
