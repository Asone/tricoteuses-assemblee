// To parse this data:
//
//   import { Convert, Document, DossierParlementaire, DossiersLegislatifs } from "./file";
//
//   const document = Convert.toDocument(json);
//   const dossierParlementaire = Convert.toDossierParlementaire(json);
//   const dossiersLegislatifs = Convert.toDossiersLegislatifs(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

import { Acteur, Mandat, Organe } from "./acteurs_et_organes"
import { PointOdj, Reunion } from "./agendas"
import { Scrutin } from "./scrutins"

export interface DossiersLegislatifs {
  textesLegislatifs: Document[]
  dossiersParlementaires: DossierParlementaire[]
}

export interface DossierParlementaire {
  schemaVersion?: string
  xsiType?: DossierParlementaireXsiType
  uid: string
  legislature: string
  titreDossier: TitreDossier
  procedureParlementaire: ProcedureParlementaire
  initiateur?: Initiateur
  actesLegislatifs: ActeLegislatif[]
  fusionDossier?: FusionDossier
  indexation?: Indexation
  plf?: EtudePlf[]
}

export enum DossierParlementaireXsiType {
  DossierCommissionEnqueteType = "DossierCommissionEnquete_Type",
  DossierIniativeExecutifType = "DossierIniativeExecutif_Type",
  DossierLegislatifType = "DossierLegislatif_Type",
  DossierMissionControleType = "DossierMissionControle_Type",
  DossierMissionInformationType = "DossierMissionInformation_Type",
  DossierResolutionAn = "DossierResolutionAN",
}

export interface EtudePlf {
  uid: string
  organeRef: string
  texteAssocie?: string
  rapporteurs?: Rapporteur[]
  missionMinefi?: MissionMinefi
  ordreDiqs: string
  ordreCommission: string
}

export interface MissionMinefi {
  typeMission: TypeMission
  libelleLong: string
  libelleCourt: string
  typeBudget: TypeBudget
  missions?: MissionMinefi[]
}

export enum TypeBudget {
  BudgetAnnexe = "Budget annexe",
  BudgetGénéral = "Budget général",
  CompteDeConcoursFinancier = "Compte de concours financier",
  CompteSpécial = "Compte spécial",
  PremièrePartie = "Première partie",
}

export enum TypeMission {
  MissionPrincipale = "mission principale",
  MissionSecondaire = "mission secondaire",
  PartieDeMission = "partie de mission",
}

export enum Qualite {
  Auteur = "auteur",
  Rapporteur = "rapporteur",
  RapporteurGénéral = "rapporteur général",
  RapporteurPourAvis = "rapporteur pour avis",
  RapporteurSpécial = "rapporteur spécial",
}

export enum TypeActeLegislatif {
  AdoptionEuropeType = "Adoption_Europe_Type",
  ConclusionEtapeCcType = "ConclusionEtapeCC_Type",
  CreationOrganeTemporaireType = "CreationOrganeTemporaire_Type",
  DecisionMotionCensureType = "DecisionMotionCensure_Type",
  DecisionRecevabiliteBureauType = "DecisionRecevabiliteBureau_Type",
  DecisionType = "Decision_Type",
  DeclarationGouvernementType = "DeclarationGouvernement_Type",
  DepotAccordInternationalType = "DepotAccordInternational_Type",
  DepotAvisConseilEtatType = "DepotAvisConseilEtat_Type",
  DepotInitiativeNavetteType = "DepotInitiativeNavette_Type",
  DepotInitiativeType = "DepotInitiative_Type",
  DepotLettreRectificativeType = "DepotLettreRectificative_Type",
  DepotMotionCensureType = "DepotMotionCensure_Type",
  DepotMotionReferendaireType = "DepotMotionReferendaire_Type",
  DepotRapportType = "DepotRapport_Type",
  DiscussionCommissionType = "DiscussionCommission_Type",
  DiscussionSeancePubliqueType = "DiscussionSeancePublique_Type",
  EtapeType = "Etape_Type",
  EtudeImpactType = "EtudeImpact_Type",
  MotionProcedureType = "MotionProcedure_Type",
  NominRapporteursType = "NominRapporteurs_Type",
  ProcedureAccelereType = "ProcedureAccelere_Type",
  PromulgationType = "Promulgation_Type",
  RenvoiCmpType = "RenvoiCMP_Type",
  RenvoiPrealableType = "RenvoiPrealable_Type",
  RetraitInitiativeType = "RetraitInitiative_Type",
  SaisieComAvisType = "SaisieComAvis_Type",
  SaisieComFondType = "SaisieComFond_Type",
  SaisineConseilConstitType = "SaisineConseilConstit_Type",
}

export interface ActeLegislatif {
  xsiType: TypeActeLegislatif
  uid: string
  codeActe: CodeActe
  libelleActe: LibelleActe
  organeRef?: string
  organe?: Organe // Added by Tricoteuses
  dateActe?: Date
  //
  anneeDecision?: string
  auteurMotion?: string
  auteursRefs?: string[]
  casSaisine?: TypeDeclaration
  codeLoi?: string
  contributionInternaute?: ContributionInternaute
  decision?: TypeDeclaration
  formuleDecision?: string
  infoJo?: InfoJo
  infoJoce?: InfoJoce
  infoJoRect?: InfoJo[]
  initiateur?: Initiateur
  motif?: Motif
  numDecision?: string
  odjRef?: string
  odj?: PointOdj // Added by Tricoteuses
  provenanceRef?: string
  provenance?: Organe // Added by Tricoteuses
  rapporteurs?: Rapporteur[]
  referenceNor?: string
  reunionRef?: string
  reunion?: Reunion // Added by Tricoteuses
  statutAdoption?: TypeDeclaration
  statutConclusion?: TypeDeclaration
  texteAdopteRef?: string
  texteAdopte?: Document // Added by Tricoteuses
  texteAssocieRef?: string
  texteAssocie?: Document // Added by Tricoteuses
  texteEuropeen?: TexteEuropeen
  texteLoiRef?: string
  textesAssocies?: TexteAssocie[]
  titreLoi?: string
  typeDeclaration?: TypeDeclaration
  typeMotion?: TypeDeclaration
  typeMotionCensure?: TypeDeclaration
  urlConclusion?: string
  urlEcheancierLoi?: string
  urlLegifrance?: string
  voteRefs?: string[]
  votes?: Scrutin[] // Added by Tricoteuses
  //
  actesLegislatifs?: ActeLegislatif[]
}

export type CodeActe =
  | CodeActeNiveau0
  | CodeActeNiveau1
  | CodeActeNiveau2
  | CodeActeNiveau3

export enum CodeActeNiveau0 {
  An1 = "AN1",
  An2 = "AN2",
  An20 = "AN20",
  An21 = "AN21",
  AnAppli = "AN-APPLI",
  Anldef = "ANLDEF",
  Anluni = "ANLUNI",
  Annlec = "ANNLEC",
  Cc = "CC",
  Cmp = "CMP",
  Eu = "EU",
  Prom = "PROM",
  Sn1 = "SN1",
  Sn2 = "SN2",
  Sn3 = "SN3",
  Snnlec = "SNNLEC",
}

export enum CodeActeNiveau1 {
  An1Acin = "AN1-ACIN",
  An1Avce = "AN1-AVCE",
  An1Com = "AN1-COM",
  An1Debats = "AN1-DEBATS",
  An1Depot = "AN1-DEPOT",
  An1Dgvt = "AN1-DGVT",
  An1Dptlettrect = "AN1-DPTLETTRECT",
  An1Eti = "AN1-ETI",
  An1Motion = "AN1-MOTION",
  An1Procacc = "AN1-PROCACC",
  An1Recbureau = "AN1-RECBUREAU",
  An1Rtrini = "AN1-RTRINI",
  An20Comenq = "AN20-COMENQ",
  An20Misinf = "AN20-MISINF",
  An20Rapport = "AN20-RAPPORT",
  An21Apan = "AN21-APAN",
  An21Debats = "AN21-DEBATS",
  An21Dgvt = "AN21-DGVT",
  An21Motion = "AN21-MOTION",
  An2Com = "AN2-COM",
  An2Debats = "AN2-DEBATS",
  An2Depot = "AN2-DEPOT",
  AnAppliRapport = "AN-APPLI-RAPPORT",
  AnldefCom = "ANLDEF-COM",
  AnldefDebats = "ANLDEF-DEBATS",
  AnldefDepot = "ANLDEF-DEPOT",
  AnldefDgvt = "ANLDEF-DGVT",
  AnluniCom = "ANLUNI-COM",
  AnluniDebats = "ANLUNI-DEBATS",
  AnluniDepot = "ANLUNI-DEPOT",
  AnluniRtrini = "ANLUNI-RTRINI",
  AnnlecCom = "ANNLEC-COM",
  AnnlecDebats = "ANNLEC-DEBATS",
  AnnlecDepot = "ANNLEC-DEPOT",
  AnnlecDgvt = "ANNLEC-DGVT",
  AnnlecMotion = "ANNLEC-MOTION",
  CcConclusion = "CC-CONCLUSION",
  CcSaisieAn = "CC-SAISIE-AN",
  CcSaisieDroit = "CC-SAISIE-DROIT",
  CcSaisiePan = "CC-SAISIE-PAN",
  CcSaisiePm = "CC-SAISIE-PM",
  CcSaisiePr = "CC-SAISIE-PR",
  CcSaisiePsn = "CC-SAISIE-PSN",
  CcSaisieSn = "CC-SAISIE-SN",
  CmpCom = "CMP-COM",
  CmpDebatsAn = "CMP-DEBATS-AN",
  CmpDebatsSn = "CMP-DEBATS-SN",
  CmpDec = "CMP-DEC",
  CmpDepot = "CMP-DEPOT",
  CmpSaisie = "CMP-SAISIE",
  EuDec = "EU-DEC",
  PromPub = "PROM-PUB",
  Sn1Avce = "SN1-AVCE",
  Sn1Com = "SN1-COM",
  Sn1Debats = "SN1-DEBATS",
  Sn1Depot = "SN1-DEPOT",
  Sn1Dptlettrect = "SN1-DPTLETTRECT",
  Sn1Eti = "SN1-ETI",
  Sn1Procacc = "SN1-PROCACC",
  Sn1Rtrini = "SN1-RTRINI",
  Sn2Com = "SN2-COM",
  Sn2Debats = "SN2-DEBATS",
  Sn2Depot = "SN2-DEPOT",
  Sn3Com = "SN3-COM",
  Sn3Debats = "SN3-DEBATS",
  Sn3Depot = "SN3-DEPOT",
  SnnlecCom = "SNNLEC-COM",
  SnnlecDebats = "SNNLEC-DEBATS",
  SnnlecDepot = "SNNLEC-DEPOT",
}

export enum CodeActeNiveau2 {
  An1ComAvis = "AN1-COM-AVIS",
  An1ComFond = "AN1-COM-FOND",
  An1DebatsDec = "AN1-DEBATS-DEC",
  An1DebatsMotion = "AN1-DEBATS-MOTION",
  An1DebatsMotionVote = "AN1-DEBATS-MOTION-VOTE",
  An1DebatsSeance = "AN1-DEBATS-SEANCE",
  An21DebatsMotionVote = "AN21-DEBATS-MOTION-VOTE",
  An21DebatsSeance = "AN21-DEBATS-SEANCE",
  An2ComAvis = "AN2-COM-AVIS",
  An2ComFond = "AN2-COM-FOND",
  An2DebatsDec = "AN2-DEBATS-DEC",
  An2DebatsSeance = "AN2-DEBATS-SEANCE",
  AnldefComFond = "ANLDEF-COM-FOND",
  AnldefDebatsDec = "ANLDEF-DEBATS-DEC",
  AnldefDebatsSeance = "ANLDEF-DEBATS-SEANCE",
  AnluniComCae = "ANLUNI-COM-CAE",
  AnluniComFond = "ANLUNI-COM-FOND",
  AnluniDebatsDec = "ANLUNI-DEBATS-DEC",
  AnluniDebatsSeance = "ANLUNI-DEBATS-SEANCE",
  AnnlecComAvis = "ANNLEC-COM-AVIS",
  AnnlecComFond = "ANNLEC-COM-FOND",
  AnnlecDebatsDec = "ANNLEC-DEBATS-DEC",
  AnnlecDebatsMotionVote = "ANNLEC-DEBATS-MOTION-VOTE",
  AnnlecDebatsSeance = "ANNLEC-DEBATS-SEANCE",
  CmpComNomin = "CMP-COM-NOMIN",
  CmpComRapportAn = "CMP-COM-RAPPORT-AN",
  CmpComRapportSn = "CMP-COM-RAPPORT-SN",
  CmpDebatsAnDec = "CMP-DEBATS-AN-DEC",
  CmpDebatsAnSeance = "CMP-DEBATS-AN-SEANCE",
  CmpDebatsSnDec = "CMP-DEBATS-SN-DEC",
  CmpDebatsSnSeance = "CMP-DEBATS-SN-SEANCE",
  Sn1ComAvis = "SN1-COM-AVIS",
  Sn1ComFond = "SN1-COM-FOND",
  Sn1DebatsDec = "SN1-DEBATS-DEC",
  Sn1DebatsMotion = "SN1-DEBATS-MOTION",
  Sn1DebatsMotionVote = "SN1-DEBATS-MOTION-VOTE",
  Sn1DebatsSeance = "SN1-DEBATS-SEANCE",
  Sn2ComAvis = "SN2-COM-AVIS",
  Sn2ComFond = "SN2-COM-FOND",
  Sn2DebatsDec = "SN2-DEBATS-DEC",
  Sn2DebatsSeance = "SN2-DEBATS-SEANCE",
  Sn3ComFond = "SN3-COM-FOND",
  Sn3DebatsDec = "SN3-DEBATS-DEC",
  Sn3DebatsSeance = "SN3-DEBATS-SEANCE",
  SnnlecComFond = "SNNLEC-COM-FOND",
  SnnlecDebatsDec = "SNNLEC-DEBATS-DEC",
  SnnlecDebatsSeance = "SNNLEC-DEBATS-SEANCE",
}

export enum CodeActeNiveau3 {
  An1ComAvisNomin = "AN1-COM-AVIS-NOMIN",
  An1ComAvisRapport = "AN1-COM-AVIS-RAPPORT",
  An1ComAvisReunion = "AN1-COM-AVIS-REUNION",
  An1ComAvisSaisie = "AN1-COM-AVIS-SAISIE",
  An1ComFondNomin = "AN1-COM-FOND-NOMIN",
  An1ComFondRapport = "AN1-COM-FOND-RAPPORT",
  An1ComFondReunion = "AN1-COM-FOND-REUNION",
  An1ComFondSaisie = "AN1-COM-FOND-SAISIE",
  An20ComenqCrea = "AN20-COMENQ-CREA",
  An20ComenqNomin = "AN20-COMENQ-NOMIN",
  An20ComenqRapport = "AN20-COMENQ-RAPPORT",
  An20MisinfCrea = "AN20-MISINF-CREA",
  An20MisinfNomin = "AN20-MISINF-NOMIN",
  An20MisinfRapport = "AN20-MISINF-RAPPORT",
  An2ComAvisRapport = "AN2-COM-AVIS-RAPPORT",
  An2ComAvisReunion = "AN2-COM-AVIS-REUNION",
  An2ComAvisSaisie = "AN2-COM-AVIS-SAISIE",
  An2ComFondNomin = "AN2-COM-FOND-NOMIN",
  An2ComFondRapport = "AN2-COM-FOND-RAPPORT",
  An2ComFondReunion = "AN2-COM-FOND-REUNION",
  An2ComFondSaisie = "AN2-COM-FOND-SAISIE",
  AnldefComFondRapport = "ANLDEF-COM-FOND-RAPPORT",
  AnldefComFondReunion = "ANLDEF-COM-FOND-REUNION",
  AnldefComFondSaisie = "ANLDEF-COM-FOND-SAISIE",
  AnluniComCaeDec = "ANLUNI-COM-CAE-DEC",
  AnluniComCaeNomin = "ANLUNI-COM-CAE-NOMIN",
  AnluniComCaeRapport = "ANLUNI-COM-CAE-RAPPORT",
  AnluniComCaeReunion = "ANLUNI-COM-CAE-REUNION",
  AnluniComCaeSaisie = "ANLUNI-COM-CAE-SAISIE",
  AnluniComFondNomin = "ANLUNI-COM-FOND-NOMIN",
  AnluniComFondRapport = "ANLUNI-COM-FOND-RAPPORT",
  AnluniComFondReunion = "ANLUNI-COM-FOND-REUNION",
  AnluniComFondSaisie = "ANLUNI-COM-FOND-SAISIE",
  AnnlecComAvisNomin = "ANNLEC-COM-AVIS-NOMIN",
  AnnlecComAvisRapport = "ANNLEC-COM-AVIS-RAPPORT",
  AnnlecComAvisReunion = "ANNLEC-COM-AVIS-REUNION",
  AnnlecComAvisSaisie = "ANNLEC-COM-AVIS-SAISIE",
  AnnlecComFondNomin = "ANNLEC-COM-FOND-NOMIN",
  AnnlecComFondRapport = "ANNLEC-COM-FOND-RAPPORT",
  AnnlecComFondReunion = "ANNLEC-COM-FOND-REUNION",
  AnnlecComFondSaisie = "ANNLEC-COM-FOND-SAISIE",
  Sn1ComAvisNomin = "SN1-COM-AVIS-NOMIN",
  Sn1ComAvisRapport = "SN1-COM-AVIS-RAPPORT",
  Sn1ComAvisSaisie = "SN1-COM-AVIS-SAISIE",
  Sn1ComFondNomin = "SN1-COM-FOND-NOMIN",
  Sn1ComFondRapport = "SN1-COM-FOND-RAPPORT",
  Sn1ComFondSaisie = "SN1-COM-FOND-SAISIE",
  Sn2ComAvisNomin = "SN2-COM-AVIS-NOMIN",
  Sn2ComAvisRapport = "SN2-COM-AVIS-RAPPORT",
  Sn2ComAvisSaisie = "SN2-COM-AVIS-SAISIE",
  Sn2ComFondNomin = "SN2-COM-FOND-NOMIN",
  Sn2ComFondRapport = "SN2-COM-FOND-RAPPORT",
  Sn2ComFondSaisie = "SN2-COM-FOND-SAISIE",
  Sn3ComFondRapport = "SN3-COM-FOND-RAPPORT",
  Sn3ComFondSaisie = "SN3-COM-FOND-SAISIE",
  SnnlecComFondNomin = "SNNLEC-COM-FOND-NOMIN",
  SnnlecComFondRapport = "SNNLEC-COM-FOND-RAPPORT",
  SnnlecComFondSaisie = "SNNLEC-COM-FOND-SAISIE",
}

export interface LibelleActe {
  nomCanonique: string
  libelleCourt?: string
}

export interface Rapporteur {
  acteurRef: string
  acteur?: Acteur // Added by Tricoteuses
  typeRapporteur: Qualite
  etudePlfRef?: string
}

export interface TypeDeclaration {
  famCode: FamCode
  libelle: string
}

export enum FamCode {
  Art493 = "Art.49.3",
  Ettd01 = "ETTD01",
  Tccmp01 = "TCCMP01",
  Tccmp02 = "TCCMP02",
  Tcd01 = "TCD01",
  Tcd02 = "TCD02",
  Tcd03 = "TCD03",
  Tcd04 = "TCD04",
  The02 = "02",
  Tmp02 = "TMP02",
  Tmp03 = "TMP03",
  Tmp05 = "TMP05",
  Tmp06 = "TMP06",
  Tmrc01 = "TMRC01",
  Tsccont01 = "TSCCONT01",
  Tsccont02 = "TSCCONT02",
  Tsccont03 = "TSCCONT03",
  Tsccont04 = "TSCCONT04",
  Tsccont05 = "TSCCONT05",
  Tsccont06 = "TSCCONT06",
  Tsccont07 = "TSCCONT07",
  Tsortf01 = "TSORTF01",
  Tsortf02 = "TSORTF02",
  Tsortf03 = "TSORTF03",
  Tsortf04 = "TSORTF04",
  Tsortf05 = "TSORTF05",
  Tsortf06 = "TSORTF06",
  Tsortf07 = "TSORTF07",
  Tsortf14 = "TSORTF14",
  Tsortf18 = "TSORTF18",
  Tsortf19 = "TSORTF19",
  Tsortf20 = "TSORTF20",
  Tsortf21 = "TSORTF21",
  Tsortf23 = "TSORTF23",
  Tsortmot01 = "TSORTMOT01",
  Tsortmot02 = "TSORTMOT02",
}

export interface TexteAssocie {
  typeTexte: TypeTexte
  texteAssocieRef: string
  texteAssocie?: Document // Added by Tricoteuses
}

export enum TypeTexte {
  Bta = "BTA",
  Tap = "TAP",
}

export interface ContributionInternaute {
  dateOuverture?: string
  dateFermeture?: string
}

export interface InfoJoce {
  refJoce: string
  dateJoce: string
}

export interface InitiateurActeur {
  acteurRef: string
  acteur?: Acteur // Added by Tricoteuses
  mandatRef?: string
  mandat?: Mandat // Added by Tricoteuses
}

export enum Motif {
  EnApplicationDeLArticle612DeLaConstitution = "En application de l'article 61§2 de la Constitution",
}

export interface TexteEuropeen {
  typeTexteEuropeen: string
  titreTexteEuropeen: string
}

export interface InfoJo {
  typeJo: TypeJo
  dateJo: string
  numJo: string
  urlLegifrance?: string
  referenceNor?: string
}

export enum TypeJo {
  JoLoiDecret = "JO_LOI_DECRET",
}

export interface InfoJoRectElement {
  typeJo: TypeJo
  dateJo: string
  numJo: string
}

export interface FusionDossier {
  cause: Cause
  dossierAbsorbantRef: string
}

export enum Cause {
  DossierAbsorbé = "Dossier absorbé",
  ExamenCommun = "Examen commun",
}

export interface Indexation {
  themes: Themes
}

export interface Themes {
  "@niveau": string
  theme: Theme
}

export interface Theme {
  libelleTheme: string
}

export interface Initiateur {
  acteurs?: InitiateurActeur[]
  organeRef?: string
  organe?: Organe // Added by Tricoteuses
}

export interface ProcedureParlementaire {
  code: string
  libelle: string
}

export interface TitreDossier {
  titre: string
  titreChemin?: string
  senatChemin?: string
}

export interface Document {
  schemaVersion?: string
  xsiType: TypeDocument
  uid: string
  legislature?: string
  cycleDeVie: CycleDeVie
  denominationStructurelle: DocumentDenominationStructurelle
  provenance?: Provenance
  titres: Titres
  divisions?: Division[]
  dossierRef: string
  dossier?: DossierParlementaire // Added by Tricoteuses
  classification: Classification
  auteurs: Auteur[]
  correction?: Correction
  notice: Notice
  indexation?: Indexation
  imprimerie?: Imprimerie
  coSignataires?: CoSignataireElement[]
  depotAmendements?: DepotAmendements
}

export enum TypeDocument {
  AccordInternationalType = "accordInternational_Type",
  AvisConseilEtatType = "avisConseilEtat_Type",
  DocumentEtudeImpactType = "documentEtudeImpact_Type",
  RapportParlementaireType = "rapportParlementaire_Type",
  TexteLoiType = "texteLoi_Type",
}

export interface Auteur {
  acteur?: AuteurActeur
  organeRef?: string
  organe?: Organe // Added by Tricoteuses
}

export interface AuteurActeur {
  acteurRef: string
  acteur?: Acteur // Added by Tricoteuses
  qualite: Qualite
}

export interface Classification {
  famille?: Famille
  type: ProcedureParlementaire
  sousType?: SousType
  statutAdoption?: StatutAdoption
}

export interface Famille {
  depot: ProcedureParlementaire
  classe: ProcedureParlementaire
  espece?: ProcedureParlementaire
}

export interface SousType {
  code: string
  libelle?: string
  libelleEdition?: string
}

export enum StatutAdoption {
  Adoptcom = "ADOPTCOM",
}

export interface CoSignataireElement {
  acteurRef?: string
  dateCosignature: string
  dateRetraitCosignature?: string
  edite: boolean
  organe?: CoSignataireOrgane
}

export interface CoSignataireOrgane {
  organeRef: string
  etApparentes: boolean
}

export interface Correction {
  typeCorrection: TypeCorrection
  niveauCorrection?: string
}

export enum TypeCorrection {
  Rectifié = "Rectifié",
}

export interface CycleDeVie {
  chrono: Chrono
}

export interface Chrono {
  dateCreation: Date
  dateDepot: Date
  datePublication?: Date
  datePublicationWeb?: Date
}

export enum DocumentDenominationStructurelle {
  Avis = "Avis",
  Déclaration = "Déclaration",
  Lettre = "Lettre",
  ProjetDeLoi = "Projet de loi",
  PropositionDeLoi = "Proposition de loi",
  PropositionDeRésolution = "Proposition de résolution",
  Rapport = "Rapport",
  RapportDInformation = "Rapport d'information",
}

export interface DepotAmendements {
  amendementsSeance: AmendementsSeance
  amendementsCommission?: AmendementsCommission[]
}

export interface AmendementsCommission {
  organeRef: string
  amendable: boolean
}

export interface AmendementsSeance {
  amendable: boolean
}

export interface Division {
  xsiType: TypeDocument
  uid: string
  legislature?: string
  cycleDeVie: CycleDeVie
  denominationStructurelle: string
  titres: Titres
  divisions?: Division[]
  dossierRef: string
  dossier?: DossierParlementaire // Added by Tricoteuses
  classification: Classification
  auteurs: Auteur[]
  correction?: Correction
  notice: Notice
  indexation?: Indexation
  imprimerie?: Imprimerie
}

export interface Imprimerie {
  dian?: string
  isbn?: string
  nbPage?: string
  prix?: string
}

export interface Notice {
  numNotice?: string
  formule?: string
  adoptionConforme: boolean
}

export interface Titres {
  titrePrincipal: string
  titrePrincipalCourt: string
}

export enum Provenance {
  Commission = "Commission",
  TexteDéposé = "Texte Déposé",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
  public static toDossiersLegislatifs(json: string): DossiersLegislatifs {
    return cast(JSON.parse(json), r("DossiersLegislatifs"))
  }

  public static dossiersLegislatifsToJson(value: DossiersLegislatifs): string {
    return JSON.stringify(uncast(value, r("DossiersLegislatifs")), null, 2)
  }

  public static toDocument(json: string): Document {
    return cast(JSON.parse(json), r("Document"))
  }

  public static documentToJson(value: Document): string {
    return JSON.stringify(uncast(value, r("Document")), null, 2)
  }

  public static toDossierParlementaire(json: string): DossierParlementaire {
    return cast(JSON.parse(json), r("DossierParlementaire"))
  }

  public static dossierParlementaireToJson(value: DossierParlementaire): string {
    return JSON.stringify(uncast(value, r("DossierParlementaire")), null, 2)
  }
}

function invalidValue(typ: any, val: any): never {
  throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`)
}

function jsonToJSProps(typ: any): any {
  if (typ.jsonToJS === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.json] = { key: p.js, typ: p.typ }))
    typ.jsonToJS = map
  }
  return typ.jsonToJS
}

function jsToJSONProps(typ: any): any {
  if (typ.jsToJSON === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.js] = { key: p.json, typ: p.typ }))
    typ.jsToJSON = map
  }
  return typ.jsToJSON
}

function transform(val: any, typ: any, getProps: any): any {
  function transformPrimitive(typ: string, val: any): any {
    if (typeof typ === typeof val) return val
    return invalidValue(typ, val)
  }

  function transformUnion(typs: any[], val: any): any {
    // val must validate against one typ in typs
    var l = typs.length
    for (var i = 0; i < l; i++) {
      var typ = typs[i]
      try {
        return transform(val, typ, getProps)
      } catch (_) {}
    }
    return invalidValue(typs, val)
  }

  function transformEnum(cases: string[], val: any): any {
    if (cases.indexOf(val) !== -1) return val
    return invalidValue(cases, val)
  }

  function transformArray(typ: any, val: any): any {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue("array", val)
    return val.map(el => transform(el, typ, getProps))
  }

  function transformDate(_typ: any, val: any): any {
    if (val === null) {
      return null
    }
    const d = new Date(val)
    if (isNaN(d.valueOf())) {
      return invalidValue("Date", val)
    }
    return d
  }

  function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
    if (val === null || typeof val !== "object" || Array.isArray(val)) {
      return invalidValue("object", val)
    }
    var result: any = {}
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key]
      const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined
      result[prop.key] = transform(v, prop.typ, getProps)
    })
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps)
      }
    })
    return result
  }

  if (typ === "any") return val
  if (typ === null) {
    if (val === null) return val
    return invalidValue(typ, val)
  }
  // if (typ === false) return invalidValue(typ, val)
  while (typeof typ === "object" && typ.ref !== undefined) {
    typ = typeMap[typ.ref]
  }
  if (Array.isArray(typ)) return transformEnum(typ, val)
  if (typeof typ === "object") {
    return typ.hasOwnProperty("unionMembers")
      ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty("arrayItems")
      ? transformArray(typ.arrayItems, val)
      : typ.hasOwnProperty("props")
      ? transformObject(getProps(typ), typ.additional, val)
      : invalidValue(typ, val)
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== "number") return transformDate(typ, val)
  return transformPrimitive(typ, val)
}

function cast<T>(val: any, typ: any): T {
  return transform(val, typ, jsonToJSProps)
}

function uncast<T>(val: T, typ: any): any {
  return transform(val, typ, jsToJSONProps)
}

function a(typ: any) {
  return { arrayItems: typ }
}

function u(...typs: any[]) {
  return { unionMembers: typs }
}

function o(props: any[], additional: any) {
  return { props, additional }
}

// function m(additional: any) {
//   return { props: [], additional }
// }

function r(name: string) {
  return { ref: name }
}

const typeMap: any = {
  DossiersLegislatifs: o(
    [
      {
        json: "textesLegislatifs",
        js: "textesLegislatifs",
        typ: a(r("Document")),
      },
      {
        json: "dossiersParlementaires",
        js: "dossiersParlementaires",
        typ: a(r("DossierParlementaire")),
      },
    ],
    false,
  ),
  DossierParlementaire: o(
    [
      { json: "schemaVersion", js: "schemaVersion", typ: u(undefined, "") },
      {
        json: "xsiType",
        js: "xsiType",
        typ: u(undefined, r("DossierParlementaireXsiType")),
      },
      { json: "uid", js: "uid", typ: "" },
      { json: "legislature", js: "legislature", typ: "" },
      { json: "titreDossier", js: "titreDossier", typ: r("TitreDossier") },
      {
        json: "procedureParlementaire",
        js: "procedureParlementaire",
        typ: r("ProcedureParlementaire"),
      },
      {
        json: "initiateur",
        js: "initiateur",
        typ: u(undefined, r("Initiateur")),
      },
      {
        json: "actesLegislatifs",
        js: "actesLegislatifs",
        typ: a(r("ActeLegislatif")),
      },
      {
        json: "fusionDossier",
        js: "fusionDossier",
        typ: u(undefined, r("FusionDossier")),
      },
      {
        json: "indexation",
        js: "indexation",
        typ: u(undefined, r("Indexation")),
      },
      { json: "plf", js: "plf", typ: u(undefined, a(r("EtudePlf"))) },
    ],
    false,
  ),
  EtudePlf: o(
    [
      { json: "uid", js: "uid", typ: "" },
      { json: "organeRef", js: "organeRef", typ: "" },
      { json: "texteAssocie", js: "texteAssocie", typ: u(undefined, "") },
      {
        json: "rapporteurs",
        js: "rapporteurs",
        typ: u(undefined, a(r("Rapporteur"))),
      },
      {
        json: "missionMinefi",
        js: "missionMinefi",
        typ: u(undefined, r("MissionMinefi")),
      },
      { json: "ordreDiqs", js: "ordreDiqs", typ: "" },
      { json: "ordreCommission", js: "ordreCommission", typ: "" },
    ],
    false,
  ),
  MissionMinefi: o(
    [
      { json: "typeMission", js: "typeMission", typ: r("TypeMission") },
      { json: "libelleLong", js: "libelleLong", typ: "" },
      { json: "libelleCourt", js: "libelleCourt", typ: "" },
      { json: "typeBudget", js: "typeBudget", typ: r("TypeBudget") },
      {
        json: "missions",
        js: "missions",
        typ: u(undefined, a(r("MissionMinefi"))),
      },
    ],
    false,
  ),
  ActeLegislatif: o(
    [
      { json: "xsiType", js: "xsiType", typ: r("TypeActeLegislatif") },
      { json: "uid", js: "uid", typ: "" },
      {
        json: "codeActe",
        js: "codeActe",
        typ: u(
          r("CodeActeNiveau0"),
          u(r("CodeActeNiveau1"), u(r("CodeActeNiveau2"), r("CodeActeNiveau3"))),
        ),
      },
      { json: "libelleActe", js: "libelleActe", typ: r("LibelleActe") },
      { json: "organeRef", js: "organeRef", typ: u(undefined, "") },
      { json: "dateActe", js: "dateActe", typ: u(undefined, Date) },
      {
        json: "actesLegislatifs",
        js: "actesLegislatifs",
        typ: u(undefined, a(r("ActeLegislatif"))),
      },
      { json: "anneeDecision", js: "anneeDecision", typ: u(undefined, "") },
      {
        json: "auteurMotion",
        js: "auteurMotion",
        typ: u(undefined, ""),
      },
      {
        json: "auteursRefs",
        js: "auteursRefs",
        typ: u(undefined, a("")),
      },
      {
        json: "casSaisine",
        js: "casSaisine",
        typ: u(undefined, r("TypeDeclaration")),
      },
      { json: "codeLoi", js: "codeLoi", typ: u(undefined, "") },
      {
        json: "contributionInternaute",
        js: "contributionInternaute",
        typ: u(undefined, r("ContributionInternaute")),
      },
      {
        json: "decision",
        js: "decision",
        typ: u(undefined, r("TypeDeclaration")),
      },
      { json: "formuleDecision", js: "formuleDecision", typ: u(undefined, "") },
      { json: "infoJo", js: "infoJo", typ: u(undefined, r("InfoJo")) },
      { json: "infoJoce", js: "infoJoce", typ: u(undefined, r("InfoJoce")) },
      {
        json: "infoJoRect",
        js: "infoJoRect",
        typ: u(undefined, a(r("InfoJo"))),
      },
      {
        json: "initiateur",
        js: "initiateur",
        typ: u(undefined, r("Initiateur")),
      },
      { json: "motif", js: "motif", typ: u(undefined, r("Motif")) },
      { json: "numDecision", js: "numDecision", typ: u(undefined, "") },
      { json: "odjRef", js: "odjRef", typ: u(undefined, "") },
      { json: "provenanceRef", js: "provenanceRef", typ: u(undefined, "") },
      {
        json: "rapporteurs",
        js: "rapporteurs",
        typ: u(undefined, a(r("Rapporteur"))),
      },
      { json: "referenceNor", js: "referenceNor", typ: u(undefined, "") },
      { json: "reunionRef", js: "reunionRef", typ: u(undefined, "") },
      {
        json: "statutAdoption",
        js: "statutAdoption",
        typ: u(undefined, r("TypeDeclaration")),
      },
      {
        json: "statutConclusion",
        js: "statutConclusion",
        typ: u(undefined, r("TypeDeclaration")),
      },
      {
        json: "texteAdopteRef",
        js: "texteAdopteRef",
        typ: u(undefined, ""),
      },
      { json: "texteAssocieRef", js: "texteAssocieRef", typ: u(undefined, "") },
      {
        json: "texteEuropeen",
        js: "texteEuropeen",
        typ: u(undefined, r("TexteEuropeen")),
      },
      { json: "texteLoiRef", js: "texteLoiRef", typ: u(undefined, "") },
      {
        json: "textesAssocies",
        js: "textesAssocies",
        typ: u(undefined, a(r("TexteAssocie"))),
      },
      { json: "titreLoi", js: "titreLoi", typ: u(undefined, "") },
      {
        json: "typeDeclaration",
        js: "typeDeclaration",
        typ: u(undefined, r("TypeDeclaration")),
      },
      {
        json: "typeMotion",
        js: "typeMotion",
        typ: u(undefined, r("TypeDeclaration")),
      },
      {
        json: "typeMotionCensure",
        js: "typeMotionCensure",
        typ: u(undefined, r("TypeDeclaration")),
      },
      { json: "urlConclusion", js: "urlConclusion", typ: u(undefined, "") },
      {
        json: "urlEcheancierLoi",
        js: "urlEcheancierLoi",
        typ: u(undefined, ""),
      },
      {
        json: "urlLegifrance",
        js: "urlLegifrance",
        typ: u(undefined, ""),
      },
      {
        json: "voteRefs",
        js: "voteRefs",
        typ: u(undefined, a("")),
      },
    ],
    false,
  ),
  LibelleActe: o(
    [
      { json: "nomCanonique", js: "nomCanonique", typ: "" },
      { json: "libelleCourt", js: "libelleCourt", typ: u(undefined, "") },
    ],
    false,
  ),
  Rapporteur: o(
    [
      { json: "acteurRef", js: "acteurRef", typ: "" },
      { json: "typeRapporteur", js: "typeRapporteur", typ: r("Qualite") },
      { json: "etudePlfRef", js: "etudePlfRef", typ: u(undefined, "") },
    ],
    false,
  ),
  TypeDeclaration: o(
    [
      { json: "famCode", js: "famCode", typ: r("FamCode") },
      { json: "libelle", js: "libelle", typ: "" },
    ],
    false,
  ),
  TexteAssocie: o(
    [
      { json: "typeTexte", js: "typeTexte", typ: r("TypeTexte") },
      { json: "texteAssocieRef", js: "texteAssocieRef", typ: "" },
    ],
    false,
  ),
  ContributionInternaute: o(
    [
      { json: "dateOuverture", js: "dateOuverture", typ: u(undefined, "") },
      { json: "dateFermeture", js: "dateFermeture", typ: u(undefined, "") },
    ],
    false,
  ),
  InfoJoce: o(
    [
      { json: "refJoce", js: "refJoce", typ: "" },
      { json: "dateJoce", js: "dateJoce", typ: "" },
    ],
    false,
  ),
  InitiateurActeur: o(
    [
      { json: "acteurRef", js: "acteurRef", typ: "" },
      { json: "mandatRef", js: "mandatRef", typ: u(undefined, "") },
    ],
    false,
  ),
  TexteEuropeen: o(
    [
      { json: "typeTexteEuropeen", js: "typeTexteEuropeen", typ: "" },
      { json: "titreTexteEuropeen", js: "titreTexteEuropeen", typ: "" },
    ],
    false,
  ),
  InfoJo: o(
    [
      { json: "typeJo", js: "typeJo", typ: r("TypeJo") },
      { json: "dateJo", js: "dateJo", typ: "" },
      { json: "numJo", js: "numJo", typ: "" },
      { json: "urlLegifrance", js: "urlLegifrance", typ: u(undefined, "") },
      { json: "referenceNor", js: "referenceNor", typ: u(undefined, "") },
    ],
    false,
  ),
  FusionDossier: o(
    [
      { json: "cause", js: "cause", typ: r("Cause") },
      { json: "dossierAbsorbantRef", js: "dossierAbsorbantRef", typ: "" },
    ],
    false,
  ),
  Indexation: o([{ json: "themes", js: "themes", typ: r("Themes") }], false),
  Themes: o(
    [
      { json: "@niveau", js: "@niveau", typ: "" },
      { json: "theme", js: "theme", typ: r("Theme") },
    ],
    false,
  ),
  Theme: o([{ json: "libelleTheme", js: "libelleTheme", typ: "" }], false),
  Initiateur: o(
    [
      {
        json: "acteurs",
        js: "acteurs",
        typ: u(undefined, a(r("InitiateurActeur"))),
      },
      { json: "organeRef", js: "organeRef", typ: u(undefined, "") },
    ],
    false,
  ),
  ProcedureParlementaire: o(
    [{ json: "code", js: "code", typ: "" }, { json: "libelle", js: "libelle", typ: "" }],
    false,
  ),
  TitreDossier: o(
    [
      { json: "titre", js: "titre", typ: "" },
      { json: "titreChemin", js: "titreChemin", typ: u(undefined, "") },
      { json: "senatChemin", js: "senatChemin", typ: u(undefined, "") },
    ],
    false,
  ),
  Document: o(
    [
      { json: "schemaVersion", js: "schemaVersion", typ: u(undefined, "") },
      { json: "xsiType", js: "xsiType", typ: r("TypeDocument") },
      { json: "uid", js: "uid", typ: "" },
      { json: "legislature", js: "legislature", typ: u(undefined, "") },
      { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
      {
        json: "denominationStructurelle",
        js: "denominationStructurelle",
        typ: r("DocumentDenominationStructurelle"),
      },
      {
        json: "provenance",
        js: "provenance",
        typ: u(undefined, r("Provenance")),
      },
      { json: "titres", js: "titres", typ: r("Titres") },
      {
        json: "divisions",
        js: "divisions",
        typ: u(undefined, a(r("Division"))),
      },
      { json: "dossierRef", js: "dossierRef", typ: "" },
      {
        json: "classification",
        js: "classification",
        typ: r("Classification"),
      },
      { json: "auteurs", js: "auteurs", typ: a(r("Auteur")) },
      {
        json: "correction",
        js: "correction",
        typ: u(undefined, r("Correction")),
      },
      { json: "notice", js: "notice", typ: r("Notice") },
      {
        json: "indexation",
        js: "indexation",
        typ: u(undefined, r("Indexation")),
      },
      {
        json: "imprimerie",
        js: "imprimerie",
        typ: u(undefined, r("Imprimerie")),
      },
      {
        json: "coSignataires",
        js: "coSignataires",
        typ: u(undefined, a(r("CoSignataireElement"))),
      },
      {
        json: "depotAmendements",
        js: "depotAmendements",
        typ: u(undefined, r("DepotAmendements")),
      },
    ],
    false,
  ),
  Auteur: o(
    [
      { json: "acteur", js: "acteur", typ: u(undefined, r("AuteurActeur")) },
      {
        json: "organeRef",
        js: "organeRef",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  AuteurActeur: o(
    [
      { json: "acteurRef", js: "acteurRef", typ: "" },
      { json: "qualite", js: "qualite", typ: r("Qualite") },
    ],
    false,
  ),
  Classification: o(
    [
      { json: "famille", js: "famille", typ: u(undefined, r("Famille")) },
      { json: "type", js: "type", typ: r("ProcedureParlementaire") },
      { json: "sousType", js: "sousType", typ: u(undefined, r("SousType")) },
      {
        json: "statutAdoption",
        js: "statutAdoption",
        typ: u(undefined, r("StatutAdoption")),
      },
    ],
    false,
  ),
  Famille: o(
    [
      { json: "depot", js: "depot", typ: r("ProcedureParlementaire") },
      { json: "classe", js: "classe", typ: r("ProcedureParlementaire") },
      {
        json: "espece",
        js: "espece",
        typ: u(undefined, r("ProcedureParlementaire")),
      },
    ],
    false,
  ),
  SousType: o(
    [
      { json: "code", js: "code", typ: "" },
      { json: "libelle", js: "libelle", typ: u(undefined, "") },
      { json: "libelleEdition", js: "libelleEdition", typ: u(undefined, "") },
    ],
    false,
  ),
  CoSignataireElement: o(
    [
      {
        json: "acteurRef",
        js: "acteurRef",
        typ: u(undefined, ""),
      },
      { json: "dateCosignature", js: "dateCosignature", typ: "" },
      {
        json: "dateRetraitCosignature",
        js: "dateRetraitCosignature",
        typ: u(undefined, ""),
      },
      { json: "edite", js: "edite", typ: false },
      {
        json: "organe",
        js: "organe",
        typ: u(undefined, r("CoSignataireOrgane")),
      },
    ],
    false,
  ),
  CoSignataireOrgane: o(
    [
      { json: "organeRef", js: "organeRef", typ: "" },
      { json: "etApparentes", js: "etApparentes", typ: false },
    ],
    false,
  ),
  Correction: o(
    [
      {
        json: "typeCorrection",
        js: "typeCorrection",
        typ: r("TypeCorrection"),
      },
      {
        json: "niveauCorrection",
        js: "niveauCorrection",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  CycleDeVie: o([{ json: "chrono", js: "chrono", typ: r("Chrono") }], false),
  Chrono: o(
    [
      { json: "dateCreation", js: "dateCreation", typ: Date },
      { json: "dateDepot", js: "dateDepot", typ: Date },
      {
        json: "datePublication",
        js: "datePublication",
        typ: u(undefined, Date),
      },
      {
        json: "datePublicationWeb",
        js: "datePublicationWeb",
        typ: u(undefined, Date),
      },
    ],
    false,
  ),
  DepotAmendements: o(
    [
      {
        json: "amendementsSeance",
        js: "amendementsSeance",
        typ: r("AmendementsSeance"),
      },
      {
        json: "amendementsCommission",
        js: "amendementsCommission",
        typ: u(undefined, a(r("AmendementsCommission"))),
      },
    ],
    false,
  ),
  AmendementsCommission: o(
    [
      { json: "organeRef", js: "organeRef", typ: "" },
      { json: "amendable", js: "amendable", typ: false },
    ],
    false,
  ),
  AmendementsSeance: o([{ json: "amendable", js: "amendable", typ: false }], false),
  Division: o(
    [
      { json: "xsiType", js: "xsiType", typ: r("TypeDocument") },
      { json: "uid", js: "uid", typ: "" },
      { json: "legislature", js: "legislature", typ: u(undefined, "") },
      { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
      {
        json: "denominationStructurelle",
        js: "denominationStructurelle",
        typ: "",
      },
      { json: "titres", js: "titres", typ: r("Titres") },
      {
        json: "divisions",
        js: "divisions",
        typ: u(undefined, a(r("Division"))),
      },
      { json: "dossierRef", js: "dossierRef", typ: "" },
      {
        json: "classification",
        js: "classification",
        typ: r("Classification"),
      },
      { json: "auteurs", js: "auteurs", typ: a(r("Auteur")) },
      {
        json: "correction",
        js: "correction",
        typ: u(undefined, r("Correction")),
      },
      { json: "notice", js: "notice", typ: r("Notice") },
      {
        json: "indexation",
        js: "indexation",
        typ: u(undefined, r("Indexation")),
      },
      {
        json: "imprimerie",
        js: "imprimerie",
        typ: u(undefined, r("Imprimerie")),
      },
    ],
    false,
  ),
  Imprimerie: o(
    [
      { json: "dian", js: "dian", typ: u(undefined, "") },
      { json: "isbn", js: "isbn", typ: u(undefined, "") },
      { json: "nbPage", js: "nbPage", typ: u(undefined, "") },
      { json: "prix", js: "prix", typ: u(undefined, "") },
    ],
    false,
  ),
  Notice: o(
    [
      { json: "numNotice", js: "numNotice", typ: u(undefined, "") },
      { json: "formule", js: "formule", typ: u(undefined, "") },
      { json: "adoptionConforme", js: "adoptionConforme", typ: false },
    ],
    false,
  ),
  Titres: o(
    [
      { json: "titrePrincipal", js: "titrePrincipal", typ: "" },
      { json: "titrePrincipalCourt", js: "titrePrincipalCourt", typ: "" },
    ],
    false,
  ),
  DossierParlementaireXsiType: [
    "DossierCommissionEnquete_Type",
    "DossierIniativeExecutif_Type",
    "DossierLegislatif_Type",
    "DossierMissionControle_Type",
    "DossierMissionInformation_Type",
    "DossierResolutionAN",
  ],
  TypeBudget: [
    "Budget annexe",
    "Budget général",
    "Compte de concours financier",
    "Compte spécial",
    "Première partie",
  ],
  TypeMission: ["mission principale", "mission secondaire", "partie de mission"],
  Qualite: [
    "auteur",
    "rapporteur",
    "rapporteur général",
    "rapporteur pour avis",
    "rapporteur spécial",
  ],
  TypeActeLegislatif: [
    "Adoption_Europe_Type",
    "ConclusionEtapeCC_Type",
    "CreationOrganeTemporaire_Type",
    "DecisionMotionCensure_Type",
    "DecisionRecevabiliteBureau_Type",
    "Decision_Type",
    "DeclarationGouvernement_Type",
    "DepotAccordInternational_Type",
    "DepotAvisConseilEtat_Type",
    "DepotInitiativeNavette_Type",
    "DepotInitiative_Type",
    "DepotLettreRectificative_Type",
    "DepotMotionCensure_Type",
    "DepotMotionReferendaire_Type",
    "DepotRapport_Type",
    "DiscussionCommission_Type",
    "DiscussionSeancePublique_Type",
    "Etape_Type",
    "EtudeImpact_Type",
    "MotionProcedure_Type",
    "NominRapporteurs_Type",
    "ProcedureAccelere_Type",
    "Promulgation_Type",
    "RenvoiCMP_Type",
    "RenvoiPrealable_Type",
    "RetraitInitiative_Type",
    "SaisieComAvis_Type",
    "SaisieComFond_Type",
    "SaisineConseilConstit_Type",
  ],
  CodeActeNiveau0: [
    "AN1",
    "AN2",
    "AN20",
    "AN21",
    "AN-APPLI",
    "ANLDEF",
    "ANLUNI",
    "ANNLEC",
    "CC",
    "CMP",
    "EU",
    "PROM",
    "SN1",
    "SN2",
    "SN3",
    "SNNLEC",
  ],
  CodeActeNiveau1: [
    "AN-APPLI-RAPPORT",
    "AN1-ACIN",
    "AN1-AVCE",
    "AN1-COM",
    "AN1-DEBATS",
    "AN1-DEPOT",
    "AN1-DGVT",
    "AN1-DPTLETTRECT",
    "AN1-ETI",
    "AN1-MOTION",
    "AN1-PROCACC",
    "AN1-RECBUREAU",
    "AN1-RTRINI",
    "AN2-COM",
    "AN2-DEBATS",
    "AN2-DEPOT",
    "AN20-COMENQ",
    "AN20-MISINF",
    "AN20-RAPPORT",
    "AN21-APAN",
    "AN21-DEBATS",
    "AN21-DGVT",
    "AN21-MOTION",
    "ANLDEF-COM",
    "ANLDEF-DEBATS",
    "ANLDEF-DEPOT",
    "ANLDEF-DGVT",
    "ANLUNI-COM",
    "ANLUNI-DEBATS",
    "ANLUNI-DEPOT",
    "ANLUNI-RTRINI",
    "ANNLEC-COM",
    "ANNLEC-DEBATS",
    "ANNLEC-DEPOT",
    "ANNLEC-DGVT",
    "ANNLEC-MOTION",
    "CC-CONCLUSION",
    "CC-SAISIE-AN",
    "CC-SAISIE-DROIT",
    "CC-SAISIE-PAN",
    "CC-SAISIE-PM",
    "CC-SAISIE-PR",
    "CC-SAISIE-PSN",
    "CC-SAISIE-SN",
    "CMP-COM",
    "CMP-DEBATS-AN",
    "CMP-DEBATS-SN",
    "CMP-DEC",
    "CMP-DEPOT",
    "CMP-SAISIE",
    "EU-DEC",
    "PROM-PUB",
    "SN1-AVCE",
    "SN1-COM",
    "SN1-DEBATS",
    "SN1-DEPOT",
    "SN1-DPTLETTRECT",
    "SN1-ETI",
    "SN1-PROCACC",
    "SN1-RTRINI",
    "SN2-COM",
    "SN2-DEBATS",
    "SN2-DEPOT",
    "SN3-COM",
    "SN3-DEBATS",
    "SN3-DEPOT",
    "SNNLEC-COM",
    "SNNLEC-DEBATS",
    "SNNLEC-DEPOT",
  ],
  CodeActeNiveau2: [
    "AN1-COM-AVIS",
    "AN1-COM-FOND",
    "AN1-DEBATS-DEC",
    "AN1-DEBATS-MOTION-VOTE",
    "AN1-DEBATS-MOTION",
    "AN1-DEBATS-SEANCE",
    "AN2-COM-AVIS",
    "AN2-COM-FOND",
    "AN2-DEBATS-DEC",
    "AN2-DEBATS-SEANCE",
    "AN21-DEBATS-MOTION-VOTE",
    "AN21-DEBATS-SEANCE",
    "ANLDEF-COM-FOND",
    "ANLDEF-DEBATS-DEC",
    "ANLDEF-DEBATS-SEANCE",
    "ANLUNI-COM-CAE",
    "ANLUNI-COM-FOND",
    "ANLUNI-DEBATS-DEC",
    "ANLUNI-DEBATS-SEANCE",
    "ANNLEC-COM-AVIS",
    "ANNLEC-COM-FOND",
    "ANNLEC-DEBATS-DEC",
    "ANNLEC-DEBATS-MOTION-VOTE",
    "ANNLEC-DEBATS-SEANCE",
    "CMP-COM-NOMIN",
    "CMP-COM-RAPPORT-AN",
    "CMP-COM-RAPPORT-SN",
    "CMP-DEBATS-AN-DEC",
    "CMP-DEBATS-AN-SEANCE",
    "CMP-DEBATS-SN-DEC",
    "CMP-DEBATS-SN-SEANCE",
    "SN1-COM-AVIS",
    "SN1-COM-AVIS",
    "SN1-COM-FOND",
    "SN1-DEBATS-DEC",
    "SN1-DEBATS-MOTION-VOTE",
    "SN1-DEBATS-MOTION",
    "SN1-DEBATS-SEANCE",
    "SN2-COM-AVIS",
    "SN2-COM-FOND",
    "SN2-DEBATS-DEC",
    "SN2-DEBATS-SEANCE",
    "SN3-COM-FOND",
    "SN3-DEBATS-DEC",
    "SN3-DEBATS-SEANCE",
    "SNNLEC-COM-FOND",
    "SNNLEC-DEBATS-DEC",
    "SNNLEC-DEBATS-SEANCE",
  ],
  CodeActeNiveau3: [
    "AN1-COM-AVIS-NOMIN",
    "AN1-COM-AVIS-RAPPORT",
    "AN1-COM-AVIS-REUNION",
    "AN1-COM-AVIS-SAISIE",
    "AN1-COM-FOND-NOMIN",
    "AN1-COM-FOND-RAPPORT",
    "AN1-COM-FOND-REUNION",
    "AN1-COM-FOND-SAISIE",
    "AN2-COM-AVIS-RAPPORT",
    "AN2-COM-AVIS-REUNION",
    "AN2-COM-AVIS-SAISIE",
    "AN2-COM-FOND-NOMIN",
    "AN2-COM-FOND-RAPPORT",
    "AN2-COM-FOND-REUNION",
    "AN2-COM-FOND-SAISIE",
    "AN20-COMENQ-CREA",
    "AN20-COMENQ-NOMIN",
    "AN20-COMENQ-RAPPORT",
    "AN20-MISINF-CREA",
    "AN20-MISINF-NOMIN",
    "AN20-MISINF-RAPPORT",
    "ANLDEF-COM-FOND-RAPPORT",
    "ANLDEF-COM-FOND-REUNION",
    "ANLDEF-COM-FOND-SAISIE",
    "ANLUNI-COM-CAE-DEC",
    "ANLUNI-COM-CAE-NOMIN",
    "ANLUNI-COM-CAE-RAPPORT",
    "ANLUNI-COM-CAE-REUNION",
    "ANLUNI-COM-CAE-SAISIE",
    "ANLUNI-COM-FOND-NOMIN",
    "ANLUNI-COM-FOND-RAPPORT",
    "ANLUNI-COM-FOND-REUNION",
    "ANLUNI-COM-FOND-SAISIE",
    "ANNLEC-COM-AVIS-NOMIN",
    "ANNLEC-COM-AVIS-RAPPORT",
    "ANNLEC-COM-AVIS-REUNION",
    "ANNLEC-COM-AVIS-SAISIE",
    "ANNLEC-COM-FOND-NOMIN",
    "ANNLEC-COM-FOND-RAPPORT",
    "ANNLEC-COM-FOND-REUNION",
    "ANNLEC-COM-FOND-SAISIE",
    "SN1-COM-AVIS-NOMIN",
    "SN1-COM-AVIS-RAPPORT",
    "SN1-COM-AVIS-SAISIE",
    "SN1-COM-FOND-NOMIN",
    "SN1-COM-FOND-RAPPORT",
    "SN1-COM-FOND-SAISIE",
    "SN2-COM-AVIS-NOMIN",
    "SN2-COM-AVIS-RAPPORT",
    "SN2-COM-AVIS-SAISIE",
    "SN2-COM-FOND-NOMIN",
    "SN2-COM-FOND-RAPPORT",
    "SN2-COM-FOND-SAISIE",
    "SN3-COM-FOND-RAPPORT",
    "SN3-COM-FOND-SAISIE",
    "SNNLEC-COM-FOND-NOMIN",
    "SNNLEC-COM-FOND-RAPPORT",
    "SNNLEC-COM-FOND-SAISIE",
  ],
  FamCode: [
    "Art.49.3",
    "ETTD01",
    "TCCMP01",
    "TCCMP02",
    "TCD01",
    "TCD02",
    "TCD03",
    "TCD04",
    "02",
    "TMP02",
    "TMP03",
    "TMP05",
    "TMP06",
    "TMRC01",
    "TSCCONT01",
    "TSCCONT02",
    "TSCCONT03",
    "TSCCONT04",
    "TSCCONT05",
    "TSCCONT06",
    "TSCCONT07",
    "TSORTF01",
    "TSORTF02",
    "TSORTF03",
    "TSORTF04",
    "TSORTF05",
    "TSORTF06",
    "TSORTF07",
    "TSORTF14",
    "TSORTF18",
    "TSORTF19",
    "TSORTF20",
    "TSORTF21",
    "TSORTF23",
    "TSORTMOT01",
    "TSORTMOT02",
  ],
  TypeTexte: ["BTA", "TAP"],
  Motif: ["En application de l'article 61§2 de la Constitution"],
  TypeJo: ["JO_LOI_DECRET"],
  Cause: ["Dossier absorbé", "Examen commun"],
  TypeDocument: [
    "accordInternational_Type",
    "avisConseilEtat_Type",
    "documentEtudeImpact_Type",
    "rapportParlementaire_Type",
    "texteLoi_Type",
  ],
  StatutAdoption: ["ADOPTCOM"],
  TypeCorrection: ["Rectifié"],
  DocumentDenominationStructurelle: [
    "Avis",
    "Déclaration",
    "Lettre",
    "Projet de loi",
    "Proposition de loi",
    "Proposition de résolution",
    "Rapport",
    "Rapport d'information",
  ],
  Provenance: ["Commission", "Texte Déposé"],
}
