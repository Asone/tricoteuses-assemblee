// To parse this data:
//
//   import { Convert, Reunion, Agendas } from "./file";
//
//   const reunion = Convert.toReunion(json);
//   const agendas = Convert.toAgendas(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

import { Acteur, Organe } from "./acteurs_et_organes"

export interface Agendas {
  reunions: Reunion[]
}

export interface Reunion {
  schemaVersion?: string
  xsiType?: ReunionXsiType
  uid: string
  timestampDebut: Date
  timestampFin?: Date
  lieu?: Lieu
  cycleDeVie: CycleDeVie
  demandeurs?: Demandeurs
  organeReuniRef?: string
  organeReuni?: Organe // Added by Tricoteuses
  typeReunion?: TypeReunion
  participants?: Participants
  sessionRef?: string
  ouverturePresse?: boolean
  odj?: Odj
  compteRenduRef?: string
  identifiants?: Identifiants
  formatReunion?: FormatReunion
  infosReunionsInternationale?: InfosReunionsInternationale
  captationVideo?: boolean
}

export enum ReunionXsiType {
  ReunionCommissionType = "reunionCommission_type",
  ReunionInitParlementaireType = "reunionInitParlementaire_type",
  SeanceType = "seance_type",
}

export interface Odj {
  convocationOdj?: string[]
  resumeOdj?: string[]
  pointsOdj?: PointOdj[]
}

export interface PointOdj {
  xsiType: PointOdjXsiType
  uid: string
  cycleDeVie: CycleDeVie
  objet: string
  procedure?: Procedure
  dossiersLegislatifsRefs?: string[]
  typePointOdj: TypePointOdj
  comiteSecret: boolean
  natureTravauxOdj?: NatureTravauxOdj
  dateConfPres?: Date
  dateLettreMinistre?: Date
}

export enum PointOdjXsiType {
  PodjReunionType = "podjReunion_type",
  PodjSeanceConfPresType = "podjSeanceConfPres_type",
}

export interface CycleDeVie {
  etat: EtatCycleDeVie
  chrono: Chrono
}

export interface Chrono {
  creation: Date
  cloture?: Date
}

export enum EtatCycleDeVie {
  Annulé = "Annulé",
  Confirmé = "Confirmé",
  Eventuel = "Eventuel",
  Supprimé = "Supprimé",
}

export enum NatureTravauxOdj {
  Odjpr = "ODJPR",
  Odjsn = "ODJSN",
}

export enum Procedure {
  DiscussionGénéraleCommune = "discussion générale commune",
  ProcédureDExamenSimplifiéeArticle103 = "procédure d'examen simplifiée-Article 103",
  ProcédureDExamenSimplifiéeArticle106 = "procédure d'examen simplifiée-Article 106",
  ProcédureDeLégislationEnCommissionArticle1071 = "procédure de législation en commission-Article 107-1",
}

export enum TypePointOdj {
  AmendementsArt88 = "Amendements (Art. 88)",
  AmendementsArt91 = "Amendements (Art. 91)",
  Audition = "Audition",
  AuditionMinistre = "Audition ministre",
  AuditionMinistreOuverteÀLaPresse = "Audition ministre ouverte à la presse",
  AuditionOuverteÀLaPresse = "Audition ouverte à la presse",
  Communication = "Communication",
  ConstitutionDeMissionDInformation = "Constitution de mission d'information",
  Discussion = "Discussion",
  DébatDInitiativeParlementaire = "Débat d'initiative parlementaire",
  DéclarationDuGouvernementSuivieDUnDébat = "Déclaration du Gouvernement suivie d'un débat",
  EchangesDeVues = "Echanges de vues",
  Examen = "Examen",
  ExplicationsDeVoteDesGroupesEtVoteParScrutinPublic = "Explications de vote des groupes et vote par scrutin public",
  ExplicationsDeVoteEtVoteParScrutinPublic = "Explications de vote et vote par scrutin public",
  FixationDeLOrdreDuJour = "Fixation de l'ordre du jour",
  NominationBureau = "Nomination bureau",
  NominationCandidatsOrganismeExtraparlementaire = "Nomination candidats organisme extraparlementaire",
  NominationDUnMembreDUneMissionDInformation = "Nomination d'un membre d'une mission d'information",
  NominationRapporteur = "Nomination rapporteur",
  NominationRapporteurDApplication = "Nomination rapporteur d'application",
  NominationRapporteurDInformation = "Nomination rapporteur d'information",
  NominationRapporteurPourAvis = "Nomination rapporteur pour avis",
  OuvertureEtClôtureDeSession = "Ouverture et clôture de session",
  QuestionsAuGouvernement = "Questions au Gouvernement",
  QuestionsOralesSansDébat = "Questions orales sans débat",
  Rapport = "Rapport",
  RapportDInformation = "Rapport d'information",
  RapportPourAvis = "Rapport pour avis",
  SuiteDeLaDiscussion = "Suite de la discussion",
  TableRonde = "Table ronde",
  VoteParScrutinPublic = "Vote par scrutin public",
  VoteSolennel = "Vote solennel",
}

export interface Demandeurs {
  acteurs?: DemandeurActeur[]
  organe?: DemandeurOrgane
}

export interface DemandeurActeur {
  nom?: string
  acteurRef: string
  acteur?: Acteur // Added by Tricoteuses
}

export interface DemandeurOrgane {
  nom: string
  organeRef: string
  organe?: Organe // Added by Tricoteuses
}

export enum FormatReunion {
  AuditionExterne = "AuditionExterne",
  AuditionParPresidentCommission = "AuditionParPresidentCommission",
  Ordinaire = "Ordinaire",
}

export interface Identifiants {
  numSeanceJo?: string
  idJo?: string
  quantieme: Quantieme
  dateSeance: string
}

export enum Quantieme {
  Deuxième = "Deuxième",
  Première = "Première",
  Troisième = "Troisième",
  Unique = "Unique",
}

export interface InfosReunionsInternationale {
  estReunionInternationale: boolean
  listePays?: string[]
  informationsComplementaires?: string
}

export interface Lieu {
  code?: string
  libelleCourt?: string
  libelleLong?: string
}

export interface Participants {
  participantsInternes?: ParticipantInterne[]
  personnesAuditionnees?: PersonneAuditionnee[]
}

export interface ParticipantInterne {
  acteurRef: string
  acteur?: Acteur // Added by Tricoteuses
  presence: Presence
}

export enum Presence {
  Absent = "absent",
  Excusé = "excusé",
  Présent = "présent",
}

export interface PersonneAuditionnee {
  xsiType: PersonneAuditionneeXsiType
  uid: string
  ident: Ident
  dateNais?: Date
}

export interface Ident {
  civ: Civ
  prenom: string
  nom: string
  alpha?: string
  trigramme?: string
}

export enum Civ {
  M = "M.",
  Mme = "Mme",
}

export enum PersonneAuditionneeXsiType {
  IdActeurType = "IdActeur_type",
  IdPersonneExterneType = "IdPersonneExterne_type",
}

export enum TypeReunion {
  Dep = "DEP", // Député
  Ga = "GA", // Groupe d'amitié
  Ge = "GE", // Groupe d'étude
  Gevi = "GEVI", // Groupe d'étude à vocation internationale
  Gp = "GP", // Groupe parlementaire
  HéAurélien = "HÉ Aurélien",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
  public static toAgendas(json: string): Agendas {
    return cast(JSON.parse(json), r("Agendas"))
  }

  public static agendasToJson(value: Agendas): string {
    return JSON.stringify(uncast(value, r("Agendas")), null, 2)
  }

  public static toReunion(json: string): Reunion {
    return cast(JSON.parse(json), r("Reunion"))
  }

  public static reunionToJson(value: Reunion): string {
    return JSON.stringify(uncast(value, r("Reunion")), null, 2)
  }
}

function invalidValue(typ: any, val: any): never {
  throw Error(
    `Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`,
  )
}

function jsonToJSProps(typ: any): any {
  if (typ.jsonToJS === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.json] = { key: p.js, typ: p.typ }))
    typ.jsonToJS = map
  }
  return typ.jsonToJS
}

function jsToJSONProps(typ: any): any {
  if (typ.jsToJSON === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.js] = { key: p.json, typ: p.typ }))
    typ.jsToJSON = map
  }
  return typ.jsToJSON
}

function transform(val: any, typ: any, getProps: any): any {
  function transformPrimitive(typ: string, val: any): any {
    if (typeof typ === typeof val) return val
    return invalidValue(typ, val)
  }

  function transformUnion(typs: any[], val: any): any {
    // val must validate against one typ in typs
    var l = typs.length
    for (var i = 0; i < l; i++) {
      var typ = typs[i]
      try {
        return transform(val, typ, getProps)
      } catch (_) {}
    }
    return invalidValue(typs, val)
  }

  function transformEnum(cases: string[], val: any): any {
    if (cases.indexOf(val) !== -1) return val
    return invalidValue(cases, val)
  }

  function transformArray(typ: any, val: any): any {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue("array", val)
    return val.map(el => transform(el, typ, getProps))
  }

  function transformDate(_typ: any, val: any): any {
    if (val === null) {
      return null
    }
    const d = new Date(val)
    if (isNaN(d.valueOf())) {
      return invalidValue("Date", val)
    }
    return d
  }

  function transformObject(
    props: { [k: string]: any },
    additional: any,
    val: any,
  ): any {
    if (val === null || typeof val !== "object" || Array.isArray(val)) {
      return invalidValue("object", val)
    }
    var result: any = {}
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key]
      const v = Object.prototype.hasOwnProperty.call(val, key)
        ? val[key]
        : undefined
      result[prop.key] = transform(v, prop.typ, getProps)
    })
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps)
      }
    })
    return result
  }

  if (typ === "any") return val
  if (typ === null) {
    if (val === null) return val
    return invalidValue(typ, val)
  }
  // if (typ === false) return invalidValue(typ, val)
  while (typeof typ === "object" && typ.ref !== undefined) {
    typ = typeMap[typ.ref]
  }
  if (Array.isArray(typ)) return transformEnum(typ, val)
  if (typeof typ === "object") {
    return typ.hasOwnProperty("unionMembers")
      ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty("arrayItems")
      ? transformArray(typ.arrayItems, val)
      : typ.hasOwnProperty("props")
      ? transformObject(getProps(typ), typ.additional, val)
      : invalidValue(typ, val)
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== "number") return transformDate(typ, val)
  return transformPrimitive(typ, val)
}

function cast<T>(val: any, typ: any): T {
  return transform(val, typ, jsonToJSProps)
}

function uncast<T>(val: T, typ: any): any {
  return transform(val, typ, jsToJSONProps)
}

function a(typ: any) {
  return { arrayItems: typ }
}

function u(...typs: any[]) {
  return { unionMembers: typs }
}

function o(props: any[], additional: any) {
  return { props, additional }
}

// function m(additional: any) {
//   return { props: [], additional }
// }

function r(name: string) {
  return { ref: name }
}

const typeMap: any = {
  Agendas: o(
    [{ json: "reunions", js: "reunions", typ: a(r("Reunion")) }],
    false,
  ),
  Reunion: o(
    [
      { json: "schemaVersion", js: "schemaVersion", typ: u(undefined, "") },
      {
        json: "xsiType",
        js: "xsiType",
        typ: u(undefined, r("ReunionXsiType")),
      },
      { json: "uid", js: "uid", typ: "" },
      { json: "timestampDebut", js: "timestampDebut", typ: Date },
      { json: "timestampFin", js: "timestampFin", typ: u(undefined, Date) },
      { json: "lieu", js: "lieu", typ: u(undefined, r("Lieu")) },
      { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
      {
        json: "demandeurs",
        js: "demandeurs",
        typ: u(undefined, r("Demandeurs")),
      },
      { json: "organeReuniRef", js: "organeReuniRef", typ: u(undefined, "") },
      {
        json: "typeReunion",
        js: "typeReunion",
        typ: u(undefined, r("TypeReunion")),
      },
      {
        json: "participants",
        js: "participants",
        typ: u(undefined, r("Participants")),
      },
      { json: "sessionRef", js: "sessionRef", typ: u(undefined, "") },
      {
        json: "ouverturePresse",
        js: "ouverturePresse",
        typ: u(undefined, false),
      },
      { json: "odj", js: "odj", typ: u(undefined, r("Odj")) },
      { json: "compteRenduRef", js: "compteRenduRef", typ: u(undefined, "") },
      {
        json: "identifiants",
        js: "identifiants",
        typ: u(undefined, r("Identifiants")),
      },
      {
        json: "formatReunion",
        js: "formatReunion",
        typ: u(undefined, r("FormatReunion")),
      },
      {
        json: "infosReunionsInternationale",
        js: "infosReunionsInternationale",
        typ: u(undefined, r("InfosReunionsInternationale")),
      },
      {
        json: "captationVideo",
        js: "captationVideo",
        typ: u(undefined, false),
      },
    ],
    false,
  ),
  Odj: o(
    [
      {
        json: "convocationOdj",
        js: "convocationOdj",
        typ: u(undefined, a("")),
      },
      { json: "resumeOdj", js: "resumeOdj", typ: u(undefined, a("")) },
      {
        json: "pointsOdj",
        js: "pointsOdj",
        typ: u(undefined, a(r("PointOdj"))),
      },
    ],
    false,
  ),
  PointOdj: o(
    [
      { json: "xsiType", js: "xsiType", typ: r("PointOdjXsiType") },
      { json: "uid", js: "uid", typ: "" },
      { json: "cycleDeVie", js: "cycleDeVie", typ: r("CycleDeVie") },
      { json: "objet", js: "objet", typ: "" },
      { json: "procedure", js: "procedure", typ: u(undefined, r("Procedure")) },
      {
        json: "dossiersLegislatifsRefs",
        js: "dossiersLegislatifsRefs",
        typ: u(undefined, a("")),
      },
      { json: "typePointOdj", js: "typePointOdj", typ: r("TypePointOdj") },
      { json: "comiteSecret", js: "comiteSecret", typ: false },
      {
        json: "natureTravauxOdj",
        js: "natureTravauxOdj",
        typ: u(undefined, r("NatureTravauxOdj")),
      },
      { json: "dateConfPres", js: "dateConfPres", typ: u(undefined, Date) },
      {
        json: "dateLettreMinistre",
        js: "dateLettreMinistre",
        typ: u(undefined, Date),
      },
    ],
    false,
  ),
  CycleDeVie: o(
    [
      { json: "etat", js: "etat", typ: r("EtatCycleDeVie") },
      { json: "chrono", js: "chrono", typ: r("Chrono") },
    ],
    false,
  ),
  Chrono: o(
    [
      { json: "creation", js: "creation", typ: Date },
      { json: "cloture", js: "cloture", typ: u(undefined, Date) },
    ],
    false,
  ),
  Demandeurs: o(
    [
      {
        json: "acteurs",
        js: "acteurs",
        typ: u(undefined, a(r("DemandeurActeur"))),
      },
      { json: "organe", js: "organe", typ: u(undefined, r("DemandeurOrgane")) },
    ],
    false,
  ),
  DemandeurActeur: o(
    [
      { json: "nom", js: "nom", typ: u(undefined, "") },
      { json: "acteurRef", js: "acteurRef", typ: "" },
    ],
    false,
  ),
  DemandeurOrgane: o(
    [
      { json: "nom", js: "nom", typ: "" },
      { json: "organeRef", js: "organeRef", typ: "" },
    ],
    false,
  ),
  Identifiants: o(
    [
      { json: "numSeanceJo", js: "numSeanceJo", typ: u(undefined, "") },
      { json: "idJo", js: "idJo", typ: u(undefined, "") },
      { json: "quantieme", js: "quantieme", typ: r("Quantieme") },
      { json: "dateSeance", js: "dateSeance", typ: Date },
    ],
    false,
  ),
  InfosReunionsInternationale: o(
    [
      {
        json: "estReunionInternationale",
        js: "estReunionInternationale",
        typ: false,
      },
      { json: "listePays", js: "listePays", typ: u(undefined, a("")) },
      {
        json: "informationsComplementaires",
        js: "informationsComplementaires",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  Lieu: o(
    [
      { json: "code", js: "code", typ: u(undefined, "") },
      { json: "libelleCourt", js: "libelleCourt", typ: u(undefined, "") },
      { json: "libelleLong", js: "libelleLong", typ: u(undefined, "") },
    ],
    false,
  ),
  Participants: o(
    [
      {
        json: "participantsInternes",
        js: "participantsInternes",
        typ: u(undefined, a(r("ParticipantInterne"))),
      },
      {
        json: "personnesAuditionnees",
        js: "personnesAuditionnees",
        typ: u(undefined, a(r("PersonneAuditionnee"))),
      },
    ],
    false,
  ),
  ParticipantInterne: o(
    [
      { json: "acteurRef", js: "acteurRef", typ: "" },
      { json: "presence", js: "presence", typ: r("Presence") },
    ],
    false,
  ),
  PersonneAuditionnee: o(
    [
      { json: "xsiType", js: "xsiType", typ: r("PersonneAuditionneeXsiType") },
      { json: "uid", js: "uid", typ: "" },
      { json: "ident", js: "ident", typ: r("Ident") },
      { json: "dateNais", js: "dateNais", typ: u(undefined, Date) },
    ],
    false,
  ),
  Ident: o(
    [
      { json: "civ", js: "civ", typ: r("Civ") },
      { json: "prenom", js: "prenom", typ: "" },
      { json: "nom", js: "nom", typ: "" },
      { json: "alpha", js: "alpha", typ: u(undefined, "") },
      { json: "trigramme", js: "trigramme", typ: u(undefined, "") },
    ],
    false,
  ),
  ReunionXsiType: [
    "reunionCommission_type",
    "reunionInitParlementaire_type",
    "seance_type",
  ],
  PointOdjXsiType: ["podjReunion_type", "podjSeanceConfPres_type"],
  EtatCycleDeVie: ["Annulé", "Confirmé", "Eventuel", "Supprimé"],
  NatureTravauxOdj: ["ODJPR", "ODJSN"],
  Procedure: [
    "discussion générale commune",
    "procédure d'examen simplifiée-Article 103",
    "procédure d'examen simplifiée-Article 106",
    "procédure de législation en commission-Article 107-1",
  ],
  TypePointOdj: [
    "Amendements (Art. 88)",
    "Amendements (Art. 91)",
    "Audition",
    "Audition ministre",
    "Audition ministre ouverte à la presse",
    "Audition ouverte à la presse",
    "Communication",
    "Constitution de mission d'information",
    "Discussion",
    "Débat d'initiative parlementaire",
    "Déclaration du Gouvernement suivie d'un débat",
    "Echanges de vues",
    "Examen",
    "Explications de vote des groupes et vote par scrutin public",
    "Explications de vote et vote par scrutin public",
    "Fixation de l'ordre du jour",
    "Nomination bureau",
    "Nomination candidats organisme extraparlementaire",
    "Nomination d'un membre d'une mission d'information",
    "Nomination rapporteur",
    "Nomination rapporteur d'application",
    "Nomination rapporteur d'information",
    "Nomination rapporteur pour avis",
    "Ouverture et clôture de session",
    "Questions au Gouvernement",
    "Questions orales sans débat",
    "Rapport",
    "Rapport d'information",
    "Rapport pour avis",
    "Suite de la discussion",
    "Table ronde",
    "Vote par scrutin public",
    "Vote solennel",
  ],
  FormatReunion: [
    "AuditionExterne",
    "AuditionParPresidentCommission",
    "Ordinaire",
  ],
  Quantieme: ["Deuxième", "Première", "Troisième", "Unique"],
  Presence: ["absent", "excusé", "présent"],
  Civ: ["M.", "Mme"],
  PersonneAuditionneeXsiType: ["IdActeur_type", "IdPersonneExterne_type"],
  TypeReunion: ["DEP", "GA", "GE", "GEVI", "GP", "HÉ Aurélien"],
}
