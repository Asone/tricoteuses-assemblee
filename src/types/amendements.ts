// To parse this data:
//
//   import { Convert, Amendement, Amendements } from "./file";
//
//   const amendement = Convert.toAmendement(json);
//   const amendements = Convert.toAmendements(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

import { Acteur, Organe } from "./acteurs_et_organes"
import { Document } from "./dossiers_legislatifs"

export interface Amendements {
  textesLegislatifs: TexteLegislatif[]
}

export interface TexteLegislatif {
  refTexteLegislatif: string
  amendements: Amendement[]
}

export interface Amendement {
  schemaVersion?: string
  uid: string
  identifiant: Identifiant
  numeroLong: string
  etapeTexte: EtapeTexte
  triAmendement: string
  cardinaliteAmdtMultiples: string
  amendementParent?: string
  etat: EtatAmendement
  signataires: Signataires
  pointeurFragmentTexte: PointeurFragmentTexte
  corps: Corps
  representations: Representation[]
  seanceDiscussion?: string
  sort?: SortAmendementAvecDate
  dateDepot?: Date
  dateDistribution?: Date
  article99: string
  loiReference: LoiReference
}

export interface Corps {
  dispositif?: string
  exposeSommaire?: string
  cartoucheDelaiDepotDepasse?: string
  avantAppel?: AvantAppel
  dispositifAmdtCredit?: DispositifAmdtCredit
}

export interface AvantAppel {
  dispositif?: string
}

export interface DispositifAmdtCredit {
  listeProgrammes: Programme[]
  totalAE: Total
  totalCP: Total
}

export interface Programme {
  libelle: string
  id: string
  AE: Ae
  CP: Ae
  action: Action
  lignesCredits?: LigneCredit[]
}

export interface Ae {
  montantPositif: string
  montantNegatif: string
}

export enum Action {
  Creation = "creation",
  Modification = "modification",
  Suppression = "suppression",
}

export interface LigneCredit {
  id: string
  libelle: string
  AE: Ae
  CP: Ae
}

export interface Total {
  montantPositif: string
  montantNegatif: string
  solde: string
}

export enum EtapeTexte {
  DeuxièmeLecture = "deuxième lecture",
  LectureDéfinitive = "Lecture définitive",
  LectureTexteCmp = "Lecture texte CMP",
  LectureUnique = "Lecture unique",
  NouvelleLecture = "Nouvelle Lecture",
  The1ÈreLecture1ÈreAssembléeSaisie = "1ère lecture (1ère assemblée saisie)",
  The1ÈreLecture2ÈmeAssembléeSaisie = "1ère lecture (2ème assemblée saisie)",
}

export enum EtatAmendement {
  ADéposer = "A déposer",
  ADiscuter = "A discuter",
  Discuté = "Discuté",
  EnRecevabilité = "En recevabilité",
  EnTraitement = "En traitement",
  Irrecevable = "Irrecevable",
  Retiré = "Retiré",
}

export interface Identifiant {
  legislature: string
  numero: string
  numRect: string
  saisine: Saisine
}

export interface Saisine {
  texteLegislatifRef: string
  texteLegislatif?: Document // Added by Tricoteuses
  numeroPartiePLF: string
  organeExamen: string
  mentionSecondeDeliberation: string
}

export interface LoiReference {
  codeLoi?: string
  divisionCodeLoi?: string
}

export interface PointeurFragmentTexte {
  missionVisee?: MissionVisee
  division: Division
  alinea?: Alinea
}

export interface Alinea {
  avantAApres?: AvantAApres
  numero: string
  alineaDesignation?: string
}

export enum AvantAApres {
  A = "A",
  Apres = "Apres",
  Avant = "Avant",
}

export interface Division {
  titre: string
  articleDesignationCourte: string
  type: DivisionType
  avantAApres: AvantAApres
  divisionRattachee: string
  articleAdditionnel?: string
  chapitreAdditionnel?: string
  urlDivisionTexteVise?: string
}

export enum DivisionType {
  Annexe = "ANNEXE",
  Article = "ARTICLE",
  Chapitre = "CHAPITRE",
  Titre = "TITRE",
}

export interface MissionVisee {
  idMissionAN: string
  libelleMission: string
  codeMissionPLF: CodeMissionPlf
  libelleMissionPLF: string
}

export enum CodeMissionPlf {
  B = "B",
  C = "C",
  D = "D",
}

export interface Representation {
  nom: Nom
  typeMime: TypeMime
  statutRepresentation: StatutRepresentation
  contenu: Contenu
}

export interface Contenu {
  documentURI: string
}

export enum Nom {
  Pdf = "PDF",
}

export interface StatutRepresentation {
  verbatim: boolean
  canonique: boolean
  officielle: boolean
  transcription: boolean
  enregistrement: boolean
}

export interface TypeMime {
  type: TypeMimeType
  subType: Nom
}

export enum TypeMimeType {
  Application = "application",
}

export interface Signataires {
  auteur: Auteur
  cosignatairesRefs?: string[]
  cosignataires?: Acteur[] // Added by Tricoteuses
  texteAffichable: string
}

export interface Auteur {
  typeAuteur: TypeAuteur
  acteurRef?: string
  acteur?: Acteur // Added by Tricoteuses
  organeRef?: string
  organe?: Organe // Added by Tricoteuses
  groupePolitiqueRef?: string
  groupePolitique?: Organe // Added by Tricoteuses
}

export enum TypeAuteur {
  Depute = "Depute",
  Gouvernement = "Gouvernement",
  Rapporteur = "Rapporteur",
}

export interface SortAmendementAvecDate {
  dateSaisie?: Date
  sortEnSeance: SortAmendement
}

export enum SortAmendement {
  Adopté = "Adopté",
  NonSoutenu = "Non soutenu",
  Rejeté = "Rejeté",
  Retiré = "Retiré",
  Tombé = "Tombé",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
  public static toAmendement(json: string): Amendement {
    return cast(JSON.parse(json), r("Amendement"))
  }

  public static amendementToJson(value: Amendement): string {
    return JSON.stringify(uncast(value, r("Amendement")), null, 2)
  }

  public static toAmendements(json: string): Amendements {
    return cast(JSON.parse(json), r("Amendements"))
  }

  public static amendementsToJson(value: Amendements): string {
    return JSON.stringify(uncast(value, r("Amendements")), null, 2)
  }

  public static toTexteLegislatif(json: string): TexteLegislatif {
    return cast(JSON.parse(json), r("TexteLegislatif"))
  }

  public static texteLegislatifToJson(value: TexteLegislatif): string {
    return JSON.stringify(uncast(value, r("TexteLegislatif")), null, 2)
  }
}

function invalidValue(typ: any, val: any): never {
  throw Error(
    `Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`,
  )
}

function jsonToJSProps(typ: any): any {
  if (typ.jsonToJS === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.json] = { key: p.js, typ: p.typ }))
    typ.jsonToJS = map
  }
  return typ.jsonToJS
}

function jsToJSONProps(typ: any): any {
  if (typ.jsToJSON === undefined) {
    var map: any = {}
    typ.props.forEach((p: any) => (map[p.js] = { key: p.json, typ: p.typ }))
    typ.jsToJSON = map
  }
  return typ.jsToJSON
}

function transform(val: any, typ: any, getProps: any): any {
  function transformPrimitive(typ: string, val: any): any {
    if (typeof typ === typeof val) return val
    return invalidValue(typ, val)
  }

  function transformUnion(typs: any[], val: any): any {
    // val must validate against one typ in typs
    var l = typs.length
    for (var i = 0; i < l; i++) {
      var typ = typs[i]
      try {
        return transform(val, typ, getProps)
      } catch (_) {}
    }
    return invalidValue(typs, val)
  }

  function transformEnum(cases: string[], val: any): any {
    if (cases.indexOf(val) !== -1) return val
    return invalidValue(cases, val)
  }

  function transformArray(typ: any, val: any): any {
    // val must be an array with no invalid elements
    if (!Array.isArray(val)) return invalidValue("array", val)
    return val.map(el => transform(el, typ, getProps))
  }

  function transformDate(_typ: any, val: any): any {
    if (val === null) {
      return null
    }
    const d = new Date(val)
    if (isNaN(d.valueOf())) {
      return invalidValue("Date", val)
    }
    return d
  }

  function transformObject(
    props: { [k: string]: any },
    additional: any,
    val: any,
  ): any {
    if (val === null || typeof val !== "object" || Array.isArray(val)) {
      return invalidValue("object", val)
    }
    var result: any = {}
    Object.getOwnPropertyNames(props).forEach(key => {
      const prop = props[key]
      const v = Object.prototype.hasOwnProperty.call(val, key)
        ? val[key]
        : undefined
      result[prop.key] = transform(v, prop.typ, getProps)
    })
    Object.getOwnPropertyNames(val).forEach(key => {
      if (!Object.prototype.hasOwnProperty.call(props, key)) {
        result[key] = transform(val[key], additional, getProps)
      }
    })
    return result
  }

  if (typ === "any") return val
  if (typ === null) {
    if (val === null) return val
    return invalidValue(typ, val)
  }
  // if (typ === false) return invalidValue(typ, val)
  while (typeof typ === "object" && typ.ref !== undefined) {
    typ = typeMap[typ.ref]
  }
  if (Array.isArray(typ)) return transformEnum(typ, val)
  if (typeof typ === "object") {
    return typ.hasOwnProperty("unionMembers")
      ? transformUnion(typ.unionMembers, val)
      : typ.hasOwnProperty("arrayItems")
      ? transformArray(typ.arrayItems, val)
      : typ.hasOwnProperty("props")
      ? transformObject(getProps(typ), typ.additional, val)
      : invalidValue(typ, val)
  }
  // Numbers can be parsed by Date but shouldn't be.
  if (typ === Date && typeof val !== "number") return transformDate(typ, val)
  return transformPrimitive(typ, val)
}

function cast<T>(val: any, typ: any): T {
  return transform(val, typ, jsonToJSProps)
}

function uncast<T>(val: T, typ: any): any {
  return transform(val, typ, jsToJSONProps)
}

function a(typ: any) {
  return { arrayItems: typ }
}

function u(...typs: any[]) {
  return { unionMembers: typs }
}

function o(props: any[], additional: any) {
  return { props, additional }
}

// function m(additional: any) {
//   return { props: [], additional }
// }

function r(name: string) {
  return { ref: name }
}

const typeMap: any = {
  Amendements: o(
    [
      {
        json: "textesLegislatifs",
        js: "textesLegislatifs",
        typ: a(r("TexteLegislatif")),
      },
    ],
    false,
  ),
  TexteLegislatif: o(
    [
      { json: "refTexteLegislatif", js: "refTexteLegislatif", typ: "" },
      { json: "amendements", js: "amendements", typ: a(r("Amendement")) },
    ],
    false,
  ),
  Amendement: o(
    [
      { json: "schemaVersion", js: "schemaVersion", typ: u(undefined, "") },
      { json: "uid", js: "uid", typ: "" },
      { json: "identifiant", js: "identifiant", typ: r("Identifiant") },
      { json: "numeroLong", js: "numeroLong", typ: "" },
      { json: "etapeTexte", js: "etapeTexte", typ: r("EtapeTexte") },
      { json: "triAmendement", js: "triAmendement", typ: "" },
      {
        json: "cardinaliteAmdtMultiples",
        js: "cardinaliteAmdtMultiples",
        typ: "",
      },
      {
        json: "amendementParent",
        js: "amendementParent",
        typ: u(undefined, ""),
      },
      { json: "etat", js: "etat", typ: r("EtatAmendement") },
      { json: "signataires", js: "signataires", typ: r("Signataires") },
      {
        json: "pointeurFragmentTexte",
        js: "pointeurFragmentTexte",
        typ: r("PointeurFragmentTexte"),
      },
      { json: "corps", js: "corps", typ: r("Corps") },
      {
        json: "representations",
        js: "representations",
        typ: a(r("Representation")),
      },
      {
        json: "seanceDiscussion",
        js: "seanceDiscussion",
        typ: u(undefined, ""),
      },
      {
        json: "sort",
        js: "sort",
        typ: u(undefined, r("SortAmendementAvecDate")),
      },
      { json: "dateDepot", js: "dateDepot", typ: u(undefined, Date) },
      {
        json: "dateDistribution",
        js: "dateDistribution",
        typ: u(undefined, Date),
      },
      { json: "article99", js: "article99", typ: "" },
      { json: "loiReference", js: "loiReference", typ: r("LoiReference") },
    ],
    false,
  ),
  Corps: o(
    [
      { json: "dispositif", js: "dispositif", typ: u(undefined, "") },
      { json: "exposeSommaire", js: "exposeSommaire", typ: u(undefined, "") },
      {
        json: "cartoucheDelaiDepotDepasse",
        js: "cartoucheDelaiDepotDepasse",
        typ: u(undefined, ""),
      },
      {
        json: "avantAppel",
        js: "avantAppel",
        typ: u(undefined, r("AvantAppel")),
      },
      {
        json: "dispositifAmdtCredit",
        js: "dispositifAmdtCredit",
        typ: u(undefined, r("DispositifAmdtCredit")),
      },
    ],
    false,
  ),
  AvantAppel: o(
    [{ json: "dispositif", js: "dispositif", typ: u(undefined, "") }],
    false,
  ),
  DispositifAmdtCredit: o(
    [
      {
        json: "listeProgrammes",
        js: "listeProgrammes",
        typ: a(r("Programme")),
      },
      { json: "totalAE", js: "totalAE", typ: r("Total") },
      { json: "totalCP", js: "totalCP", typ: r("Total") },
    ],
    false,
  ),
  Programme: o(
    [
      { json: "libelle", js: "libelle", typ: "" },
      { json: "id", js: "id", typ: "" },
      { json: "AE", js: "AE", typ: r("Ae") },
      { json: "CP", js: "CP", typ: r("Ae") },
      { json: "action", js: "action", typ: r("Action") },
      {
        json: "lignesCredits",
        js: "lignesCredits",
        typ: u(undefined, a(r("LigneCredit"))),
      },
    ],
    false,
  ),
  Ae: o(
    [
      { json: "montantPositif", js: "montantPositif", typ: "" },
      { json: "montantNegatif", js: "montantNegatif", typ: "" },
    ],
    false,
  ),
  LigneCredit: o(
    [
      { json: "id", js: "id", typ: "" },
      { json: "libelle", js: "libelle", typ: "" },
      { json: "AE", js: "AE", typ: r("Ae") },
      { json: "CP", js: "CP", typ: r("Ae") },
    ],
    false,
  ),
  Total: o(
    [
      { json: "montantPositif", js: "montantPositif", typ: "" },
      { json: "montantNegatif", js: "montantNegatif", typ: "" },
      { json: "solde", js: "solde", typ: "" },
    ],
    false,
  ),
  Identifiant: o(
    [
      { json: "legislature", js: "legislature", typ: "" },
      { json: "numero", js: "numero", typ: "" },
      { json: "numRect", js: "numRect", typ: "" },
      { json: "saisine", js: "saisine", typ: r("Saisine") },
    ],
    false,
  ),
  Saisine: o(
    [
      { json: "texteLegislatifRef", js: "texteLegislatifRef", typ: "" },
      { json: "numeroPartiePLF", js: "numeroPartiePLF", typ: "" },
      { json: "organeExamen", js: "organeExamen", typ: "" },
      {
        json: "mentionSecondeDeliberation",
        js: "mentionSecondeDeliberation",
        typ: "",
      },
    ],
    false,
  ),
  LoiReference: o(
    [
      { json: "codeLoi", js: "codeLoi", typ: u(undefined, "") },
      { json: "divisionCodeLoi", js: "divisionCodeLoi", typ: u(undefined, "") },
    ],
    false,
  ),
  PointeurFragmentTexte: o(
    [
      {
        json: "missionVisee",
        js: "missionVisee",
        typ: u(undefined, r("MissionVisee")),
      },
      { json: "division", js: "division", typ: r("Division") },
      { json: "alinea", js: "alinea", typ: u(undefined, r("Alinea")) },
    ],
    false,
  ),
  Alinea: o(
    [
      {
        json: "avantAApres",
        js: "avantAApres",
        typ: u(undefined, r("AvantAApres")),
      },
      { json: "numero", js: "numero", typ: "" },
      {
        json: "alineaDesignation",
        js: "alineaDesignation",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  Division: o(
    [
      { json: "titre", js: "titre", typ: "" },
      {
        json: "articleDesignationCourte",
        js: "articleDesignationCourte",
        typ: "",
      },
      { json: "type", js: "type", typ: r("DivisionType") },
      {
        json: "avantAApres",
        js: "avantAApres",
        typ: r("AvantAApres"),
      },
      { json: "divisionRattachee", js: "divisionRattachee", typ: "" },
      {
        json: "articleAdditionnel",
        js: "articleAdditionnel",
        typ: u(undefined, ""),
      },
      {
        json: "chapitreAdditionnel",
        js: "chapitreAdditionnel",
        typ: u(undefined, ""),
      },
      {
        json: "urlDivisionTexteVise",
        js: "urlDivisionTexteVise",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  MissionVisee: o(
    [
      { json: "idMissionAN", js: "idMissionAN", typ: "" },
      { json: "libelleMission", js: "libelleMission", typ: "" },
      {
        json: "codeMissionPLF",
        js: "codeMissionPLF",
        typ: r("CodeMissionPlf"),
      },
      { json: "libelleMissionPLF", js: "libelleMissionPLF", typ: "" },
    ],
    false,
  ),
  Representation: o(
    [
      { json: "nom", js: "nom", typ: r("Nom") },
      { json: "typeMime", js: "typeMime", typ: r("TypeMime") },
      {
        json: "statutRepresentation",
        js: "statutRepresentation",
        typ: r("StatutRepresentation"),
      },
      { json: "contenu", js: "contenu", typ: r("Contenu") },
    ],
    false,
  ),
  Contenu: o([{ json: "documentURI", js: "documentURI", typ: "" }], false),
  StatutRepresentation: o(
    [
      { json: "verbatim", js: "verbatim", typ: false },
      { json: "canonique", js: "canonique", typ: false },
      { json: "officielle", js: "officielle", typ: false },
      { json: "transcription", js: "transcription", typ: false },
      { json: "enregistrement", js: "enregistrement", typ: false },
    ],
    false,
  ),
  TypeMime: o(
    [
      { json: "type", js: "type", typ: r("TypeMimeType") },
      { json: "subType", js: "subType", typ: r("Nom") },
    ],
    false,
  ),
  Signataires: o(
    [
      { json: "auteur", js: "auteur", typ: r("Auteur") },
      {
        json: "cosignatairesRefs",
        js: "cosignatairesRefs",
        typ: u(undefined, a("")),
      },
      { json: "texteAffichable", js: "texteAffichable", typ: "" },
    ],
    false,
  ),
  Auteur: o(
    [
      { json: "typeAuteur", js: "typeAuteur", typ: r("TypeAuteur") },
      {
        json: "acteurRef",
        js: "acteurRef",
        typ: u(undefined, ""),
      },
      { json: "organeRef", js: "organeRef", typ: u(undefined, "") },
      {
        json: "groupePolitiqueRef",
        js: "groupePolitiqueRef",
        typ: u(undefined, ""),
      },
    ],
    false,
  ),
  SortAmendementAvecDate: o(
    [
      { json: "dateSaisie", js: "dateSaisie", typ: u(undefined, Date) },
      { json: "sortEnSeance", js: "sortEnSeance", typ: r("SortAmendement") },
    ],
    false,
  ),
  Action: ["creation", "modification", "suppression"],
  EtapeTexte: [
    "deuxième lecture",
    "Lecture définitive",
    "Lecture texte CMP",
    "Lecture unique",
    "Nouvelle Lecture",
    "1ère lecture (1ère assemblée saisie)",
    "1ère lecture (2ème assemblée saisie)",
  ],
  EtatAmendement: [
    "A déposer",
    "A discuter",
    "Discuté",
    "En recevabilité",
    "En traitement",
    "Irrecevable",
    "Retiré",
  ],
  AvantAApres: ["A", "Apres", "Avant"],
  DivisionType: ["ANNEXE", "ARTICLE", "CHAPITRE", "TITRE"],
  CodeMissionPlf: ["B", "C", "D"],
  Nom: ["PDF"],
  TypeMimeType: ["application"],
  TypeAuteur: ["Depute", "Gouvernement", "Rapporteur"],
  SortAmendement: ["Adopté", "Non soutenu", "Rejeté", "Retiré", "Tombé"],
}
