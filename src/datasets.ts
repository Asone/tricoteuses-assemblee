import fs from "fs-extra"
import path from "path"

import { Legislature } from "./types/legislatures"

interface Dataset {
  filename: string
  ignoreForWeb?: boolean
  legislature: Legislature
  repairZip?: (dataset: Dataset, dataDir: string) => void
  structure: DatasetStructure
  title: string
  url: string
}

export interface Datasets {
  acteursEtOrganes: Dataset[]
  agendas: Dataset[]
  amendements: Dataset[]
  dossiersLegislatifs: Dataset[]
  scrutins: Dataset[]
}

export enum DatasetStructure {
  SingleFile,
  SegmentedFiles,
}

export enum EnabledDatasets {
  None = 0,
  ActeursEtOrganes = 1 << 0,
  Agendas = 1 << 1,
  Amendements = 1 << 2,
  DossiersLegislatifs = 1 << 3,
  Photos = 1 << 4,
  Scrutins = 1 << 5,
  All = ActeursEtOrganes |
    Agendas |
    Amendements |
    DossiersLegislatifs |
    Photos |
    Scrutins,
}

export const datasets: Datasets = {
  acteursEtOrganes: [
    {
      // AMO10
      //
      // Contient les députés actifs
      // * avec tous les mandats en cours
      // * y compris avec leur parti politique (typeOrgane: "PARPOL")
      // * mais sans les mandats achevés (y compris dans cette législature)
      //
      // Organes :
      // * Contient seulement les organes correspondants aux mandats en cours
      // * avec le xsiType de l'organe
      // * avec des commissions sénatoriales (permanentes et spéciales) en cours
      // * avec des délégations sénatoriales en cours
      // * avec des groupes sénatoriaux en cours
      // * avec des ministères en cours
      // * avec les Présidences de la République (y compris Sarkozy)
      // * avec le Conseil constitutionnel
      //
      // Ce fichier est un sous-ensemble à tous les niveaux du fichier AMO20
      // (qui est lui même un sous-ensemble de l'AMO30).
      // Il n'apporte donc absolument rien par rapport à AMO20 ou l'AMO30.
      filename: "AMO10_deputes_actifs_mandats_actifs_organes_XV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Députés actifs et organes de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes/AMO10_deputes_actifs_mandats_actifs_organes_XV.json.zip",
    },
    {
      // AMO20 (XV)
      //
      // Contient les députés de la législature, actifs ou non (invalidés, etc), les sénateurs actuels
      // Pour les députés :
      // * avec tous leurs mandats de la législature
      // * y compris les mandats achevés
      // * y compris leurs partis politiques  (typeOrgane: "PARPOL")
      // * mais sans les mandats des législatures précédentes (qui sont dans AMO40)
      //
      // Contient tous les organes référencés par les mandats
      // * avec le xsiType de l'organe
      // * avec des commissions sénatoriales (permanentes et spéciales) en cours
      // * avec des délégations sénatoriales en cours
      // * avec des groupes sénatoriaux en cours
      // * avec des ministères en cours
      // * avec les Présidences de la République (y compris Sarkozy)
      // * avec le Conseil constitutionnel
      //
      // Ce fichier est un sous-ensemble à tous les niveaux du fichier AMO30.
      // Il n'apporte donc absolument rien par rapport à l'AMO30.
      filename: "AMO20_dep_sen_min_tous_mandats_et_organes_XV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Acteurs et organes de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_senateurs_ministres_legislature/AMO20_dep_sen_min_tous_mandats_et_organes_XV.json.zip",
    },
    {
      // AMO20 (XIV)
      //
      // Comme AMO20 (XV) mais pour la XIVème législature
      // Tous les acteurs sauf un (PA429892) sont présents en plus à jour dans AMO30.
      // Cet acteur PA429892 (ancienne Sénatrice) n'est présent dans aucun des autres
      // fichiers AMO.
      // Tous les organes sont présents en plus complets dans AMO30.
      //
      // Ce fichier est donc à un détail prêt (PA429892) un sous-ensemble d'AMO30.
      // Il n'apporte donc quasiment rien par rapport à l'AMO30.
      filename: "AMO20_dep_sen_min_tous_mandats_et_organes_XIV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quatorze,
      structure: DatasetStructure.SingleFile,
      title: "Acteurs et organes de la XIVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/14/amo/deputes_senateurs_ministres_legislatures_XIV/AMO20_dep_sen_min_tous_mandats_et_organes_XIV.json.zip",
    },
    {
      // Ce fichier contient tous les acteurs et organes.
      // C'est un sur-ensemble des fichiers AMO20.
      filename: "AMO30_tous_acteurs_tous_mandats_tous_organes_historique.json",
      legislature: Legislature.All,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Historique de tous les acteurs et organes depuis la XIème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/tous_acteurs_mandats_organes_xi_legislature/AMO30_tous_acteurs_tous_mandats_tous_organes_historique.json.zip",
    },
    {
      // AMO40
      //
      // Contient les députés actifs (les mêmes qu'AMO20)
      // * avec presque tous leurs mandats
      // * y compris les mandats achevés et ceux des législatures précédentes
      // * y compris leurs partis politiques précédents
      // * mais sans leur parti politique actuel (typeOrgane: "PARPOL")
      //
      // Contient beaucoup d'organes référencés, y compris ceux des législatures
      // précédentes
      // * mais il manque le xsiType des organes
      // * ne contient pas les organes sénatoriaux
      // * ne contient pas les ministères
      // * ne contient pas la Présidence de la République
      // * ne contient pas le Conseil constitutionnel
      //
      // Pour les acteurs, ce dépôt est un sous-ensemble strict de AMO30 :
      // Il a moins d'acteurs et il sont moins complets.
      //
      // Pour les organes :
      // Quand l'organe est aussi présent dans AMO30, il vaut mieux prendre celui
      // d'AMO30, car il contient le xsiType (c'est la seule différence semble-t-i).
      // Sinon, il faut le prendre seulement si on est intéressé par les législatures
      // précédentes.
      filename: "AMO40_deputes_actifs_mandats_actifs_organes_divises_XV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.mkdirSync(targetDir)
        fs.moveSync(path.join(dataDir, "acteur"), path.join(targetDir, "acteur"))
        fs.moveSync(path.join(dataDir, "organe"), path.join(targetDir, "organe"))
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Députés actifs et organes de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/deputes_actifs_mandats_actifs_organes_divises/AMO40_deputes_actifs_mandats_actifs_organes_divises_XV.json.zip",
    },
    {
      // Attention, ce fichier n'est pas mis à jour régulièrement !
      //
      // Pour les acteurs :
      // * c'est un sous ensemble obsolète d'AMO30
      // * il manque les xsiType aux mandats
      //
      // Pour les organes :
      // * c'est un sous ensemble obsolète d'AMO30
      // * il manque les xsiType
      //
      // Ce fichier n'apporte donc absolument rien par rapport à l'AMO30,
      // dont il est un sous ensemble obsolète.
      filename: "AMO50_acteurs_mandats_organes_divises_XV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.mkdirSync(targetDir)
        fs.moveSync(path.join(dataDir, "mandat"), path.join(targetDir, "mandat"))
        fs.moveSync(path.join(dataDir, "acteur"), path.join(targetDir, "acteur"))
        fs.moveSync(path.join(dataDir, "organe"), path.join(targetDir, "organe"))
      },
      structure: DatasetStructure.SegmentedFiles,
      title:
        "Acteurs, mandats et organes de la XVème législature (fichiers séparés, y compris mandats)",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/amo/acteurs_mandats_organes_divises/AMO50_acteurs_mandats_organes_divises_XV.json.zip",
    },
  ],
  agendas: [
    {
      filename: "Agenda_XV.json",
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Agenda de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/vp/reunions/Agenda_XV.json.zip",
    },
    {
      filename: "Agenda_XIV.json",
      legislature: Legislature.Quatorze,
      structure: DatasetStructure.SingleFile,
      title: "Agenda de la XIVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/14/vp/reunions/Agenda_XIV.json.zip",
    },
  ],
  amendements: [
    {
      filename: "Amendements_XV.json",
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Amendements de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/loi/amendements_legis/Amendements_XV.json.zip",
    },
    // {
    //   filename: "Amendements_XIV.json",
    //   ignoreForWeb: true,
    //   legislature: Legislature.Quatorze,
    //   structure: DatasetStructure.SingleFile,
    //   title: "Amendements de la XIVème législature",
    //   url:
    //     "http://data.assemblee-nationale.fr/static/openData/repository/14/loi/amendements_legis_XIV/Amendements_XIV.json.zip",
    // },
  ],
  dossiersLegislatifs: [
    {
      filename: "Dossiers_Legislatifs_XV.json",
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Dossiers législatifs de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/loi/dossiers_legislatifs/Dossiers_Legislatifs_XV.json.zip",
    },
    {
      filename: "Dossiers_Legislatifs_XIV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quatorze,
      structure: DatasetStructure.SingleFile,
      title: "Dossiers législatifs de la XIVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/14/loi/dossiers_legislatifs/Dossiers_Legislatifs_XIV.json.zip",
    },
  ],
  scrutins: [
    {
      filename: "Scrutins_XV.json",
      legislature: Legislature.Quinze,
      repairZip: (dataset: Dataset, dataDir: string) => {
        const targetDir = path.join(dataDir, dataset.filename)
        if (fs.existsSync(targetDir)) {
          fs.removeSync(targetDir)
        }
        fs.moveSync(path.join(dataDir, "json"), targetDir)
      },
      structure: DatasetStructure.SegmentedFiles,
      title: "Scrutins de la XVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/15/loi/scrutins/Scrutins_XV.json.zip",
    },
    {
      filename: "Scrutins_XIV.json",
      ignoreForWeb: true,
      legislature: Legislature.Quatorze,
      structure: DatasetStructure.SingleFile,
      title: "Scrutins de la XIVème législature",
      url:
        "http://data.assemblee-nationale.fr/static/openData/repository/14/loi/scrutins/Scrutins_XIV.json.zip",
    },
  ],
}
