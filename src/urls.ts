import { Division, Document } from "./types/dossiers_legislatifs"
import { Scrutin } from "./types/scrutins"

export enum DocumentUrlFormat {
  Html = "html",
  Pdf = "pdf",
  RawHtml = "raw-html", // The version of the document that can be parsed to extract divisions & articles
}

const documentUrlFragmentsByType: {
  [type: string]: {
    directory: string
    prefix: string
    suffix: string
  }
} = {
  ACIN: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ai",
  },
  AVCE: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ace",
  },
  AVIS: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  ETDI: {
    directory: "projets",
    prefix: "pl",
    suffix: "-ei",
  },
  LETT: {
    directory: "projets",
    prefix: "pl",
    suffix: "-l",
  },
  PION: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRE: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREAPPART341: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRECOMENQ: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNREMODREGLTAN: {
    directory: "propositions",
    prefix: "pion",
    suffix: "",
  },
  PNRETVXINSTITEUROP: {
    directory: "europe/resolutions",
    prefix: "ppe",
    suffix: "",
  },
  PRJL: {
    directory: "projets",
    prefix: "pl",
    suffix: "",
  },
  RAPP: {
    directory: "rapports",
    prefix: "r",
    suffix: "",
  },
  RINF: {
    directory: "rap-inf",
    prefix: "i",
    suffix: "",
  },
  RION: {
    directory: "",
    prefix: "",
    suffix: "",
  },
  TADO: {
    directory: "ta",
    prefix: "ta",
    suffix: "",
  },
  TCOM: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMCOMENQ: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMMODREGLTAN: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
  TCOMTVXINSTITEUROP: {
    directory: "ta-commission",
    prefix: "r",
    suffix: "-a0",
  },
}

function extensionFromDocumentUrlFormat(format: DocumentUrlFormat): string {
  switch (format) {
    case DocumentUrlFormat.Html:
      return ".asp"
    case DocumentUrlFormat.Pdf:
      return ".pdf"
    case DocumentUrlFormat.RawHtml:
      return ".asp"
  }
}

export function urlFromDocument(
  document: Document | Division,
  format: DocumentUrlFormat,
): string | null {
  // Code taken from function `an_text_url` in project anpy:
  // https://github.com/regardscitoyens/anpy/blob/master/anpy/dossier_from_opendata.py
  // See http://www.assemblee-nationale.fr/opendata/Implementation_Referentiel/Identifiants_Referentiels.html
  // for a description of the format of an UID.

  const extension = extensionFromDocumentUrlFormat(format)
  const match = /^(.{4})([ANS]*)(R[0-9])([LS]*)([0-9]*)([BTACP]*)(.*)$/.exec(document.uid)
  if (match === null) {
    console.log(
      `Unable to generate URL from document with unexpected UID: "${document.uid}"`,
    )
    return null
  }
  const chambre = match[2]
  if (chambre === "SN") {
    console.log(`Unable to generate URL for document in Sénat. UID: "${document.uid}"`)
    return null
  }
  const legislature = match[5]
  let number = match[7]
  if (format === DocumentUrlFormat.RawHtml) {
    return `http://www.assemblee-nationale.fr/${legislature}/textes/${number}${extension}`
  }
  const documentType =
    ({
      BTC: "TCOM",
      BTA: "TADO",
      TAP: "TADO",
    } as { [key: string]: string })[match[6]] || document.classification.type.code
  if (
    document.classification.sousType !== undefined &&
    document.classification.sousType.code === "COMPA"
  ) {
    // Annexe au rapport : Texte comparatif COMPA
    number = number.replace(/-COMPA/, "-aCOMPA")
  }
  const fragments = documentUrlFragmentsByType[documentType]
  if (fragments === undefined) {
    // For example: ALCNANR5L15B0002 (allocution du président)
    console.log(
      `Unable to generate URL from document of UID "${document.uid}" and type "${documentType}"`,
    )
    return null
  }
  const directory =
    format === DocumentUrlFormat.Pdf ? `pdf/${fragments.directory}` : fragments.directory
  return `http://www.assemblee-nationale.fr/${legislature}/${directory}/${fragments.prefix}${number}${fragments.suffix}${extension}`
}

export function urlFromScrutin(scrutin: Scrutin) {
  // Sample UIDs: VTANR5L15V389
  const match = /^VTANR([0-9]+)L([0-9]+)V([0-9]+)$/.exec(scrutin.uid)
  if (match === null) {
    console.log(
      `Unable to generate URL from scrutin with unexpected UID: "${scrutin.uid}"`,
    )
    return null
  }
  const legislature = match[2]
  const numero = match[3]
  return `http://www2.assemblee-nationale.fr/scrutins/detail/(legislature)/${legislature}/(num)/${numero}`
}
