/// Load data from files

import fs from "fs"
import path from "path"

export { EnabledDatasets } from "./datasets"
import { datasets, EnabledDatasets } from "./datasets"
import { walkDir } from "./file_systems"
import {
  Acteur,
  Convert as ActeursEtOrganesConvert,
  Organe,
  Photo,
} from "./types/acteurs_et_organes"
import { Convert as AgendasConvert, Reunion } from "./types/agendas"
import { Amendement, Convert as AmendementsConvert } from "./types/amendements"
import {
  Convert as DossiersLegislatifsConvert,
  Document,
  DossierParlementaire,
} from "./types/dossiers_legislatifs"
import { Legislature } from "./types/legislatures"
import { Convert as ScrutinsConvert, Scrutin } from "./types/scrutins"

export interface Data {
  readonly acteurByUid: { [uid: string]: Acteur }
  readonly amendementByUid: { [uid: string]: Amendement }
  readonly documentByUid: { [uid: string]: Document }
  readonly dossierParlementaireBySenatChemin: {
    [uid: string]: DossierParlementaire
  }
  readonly dossierParlementaireByTitreChemin: {
    [uid: string]: DossierParlementaire
  }
  readonly dossierParlementaireByUid: { [uid: string]: DossierParlementaire }
  readonly organeByUid: { [uid: string]: Organe }
  readonly photoByUid: { [uid: string]: Photo }
  readonly reunionByUid: { [uid: string]: Reunion }
  readonly reunionsByDay: { [day: string]: Reunion[] }
  readonly reunionsByDossierUid: { [dossierUid: string]: Reunion[] }
  readonly scrutinByUid: { [uid: string]: Scrutin }
}

/** Load data from split files. */
export function loadAssembleeData(
  dataDir: string,
  enabledDatasets: EnabledDatasets,
  legislature: Legislature,
  { log = false } = {},
): Data {
  const acteurByUid: { [uid: string]: Acteur } = {}
  const organeByUid: { [uid: string]: Organe } = {}
  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const datasetDir = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye"),
      )

      for (const acteurSplitPath of walkDir(datasetDir, ["acteurs"])) {
        const acteurFilename = acteurSplitPath[acteurSplitPath.length - 1]
        if (!acteurFilename.endsWith(".json")) {
          continue
        }
        const acteurFilePath = path.join(datasetDir, ...acteurSplitPath)
        if (log) {
          console.log(`Loading file: ${acteurFilePath}…`)
        }
        const acteurJson: string = fs.readFileSync(acteurFilePath, {
          encoding: "utf8",
        })
        const acteur = ActeursEtOrganesConvert.toActeur(acteurJson)
        // Keep the first existing "acteur", because the first wrapper is the most complete.
        if (acteurByUid[acteur.uid] === undefined) {
          acteurByUid[acteur.uid] = acteur
        }
      }

      for (const organeSplitPath of walkDir(datasetDir, ["organes"])) {
        const organeFilename = organeSplitPath[organeSplitPath.length - 1]
        if (!organeFilename.endsWith(".json")) {
          continue
        }
        const organeFilePath = path.join(datasetDir, ...organeSplitPath)
        if (log) {
          console.log(`Loading file: ${organeFilePath}…`)
        }
        const organeJson: string = fs.readFileSync(organeFilePath, {
          encoding: "utf8",
        })
        const organe = ActeursEtOrganesConvert.toOrgane(organeJson)
        // Keep the first existing "organe", because the first wrapper is the most complete.
        if (organeByUid[organe.uid] === undefined) {
          organeByUid[organe.uid] = organe
        }
      }
    }
  }

  const reunionByUid: { [uid: string]: Reunion } = {}
  const reunionsByDay: { [day: string]: Reunion[] } = {}
  const reunionsByDossierUid: { [dossierUid: string]: Reunion[] } = {}
  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const datasetDir = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye"),
      )
      for (const reunionSplitPath of walkDir(datasetDir)) {
        const reunionFilename = reunionSplitPath[reunionSplitPath.length - 1]
        if (!reunionFilename.endsWith(".json")) {
          continue
        }
        const reunionFilePath = path.join(datasetDir, ...reunionSplitPath)
        if (log) {
          console.log(`Loading file: ${reunionFilePath}…`)
        }
        const reunionJson: string = fs.readFileSync(reunionFilePath, {
          encoding: "utf8",
        })
        const reunion = AgendasConvert.toReunion(reunionJson)
        if (reunionByUid[reunion.uid] === undefined) {
          reunionByUid[reunion.uid] = reunion
        }

        const day = reunion.timestampDebut.toISOString().split("T")[0]
        let reunionsAtDay = reunionsByDay[day]
        if (reunionsAtDay === undefined) {
          reunionsAtDay = reunionsByDay[day] = []
        }
        reunionsAtDay.push(reunion)

        const odj = reunion.odj
        if (odj !== undefined) {
          for (const pointOdj of odj.pointsOdj || []) {
            for (const dossierLegislatifRef of pointOdj.dossiersLegislatifsRefs || []) {
              let reunionsAtDossierUid = reunionsByDossierUid[dossierLegislatifRef]
              if (reunionsAtDossierUid === undefined) {
                reunionsAtDossierUid = reunionsByDossierUid[dossierLegislatifRef] = []
              }
              reunionsAtDossierUid.push(reunion)
            }
          }
        }
      }
    }
  }

  const amendementByUid: { [uid: string]: Amendement } = {}
  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const datasetDir = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye"),
      )

      for (const amendementSplitPath of walkDir(datasetDir)) {
        const amendementFilename = amendementSplitPath[amendementSplitPath.length - 1]
        if (!amendementFilename.endsWith(".json")) {
          continue
        }
        const amendementFilePath = path.join(datasetDir, ...amendementSplitPath)
        if (log) {
          console.log(`Loading file: ${amendementFilePath}…`)
        }
        const amendementJson: string = fs.readFileSync(amendementFilePath, {
          encoding: "utf8",
        })
        const amendement = AmendementsConvert.toAmendement(amendementJson)
        if (amendementByUid[amendement.uid] === undefined) {
          amendementByUid[amendement.uid] = amendement
        }
      }
    }
  }

  const documentByUid: { [uid: string]: Document } = {}
  const dossierParlementaireBySenatChemin: {
    [uid: string]: DossierParlementaire
  } = {}
  const dossierParlementaireByTitreChemin: {
    [uid: string]: DossierParlementaire
  } = {}
  const dossierParlementaireByUid: { [uid: string]: DossierParlementaire } = {}
  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const datasetDir = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye"),
      )

      for (const documentSplitPath of walkDir(datasetDir, ["documents"])) {
        const documentFilename = documentSplitPath[documentSplitPath.length - 1]
        if (!documentFilename.endsWith(".json")) {
          continue
        }
        const documentFilePath = path.join(datasetDir, ...documentSplitPath)
        if (log) {
          console.log(`Loading file: ${documentFilePath}…`)
        }
        const documentJson: string = fs.readFileSync(documentFilePath, {
          encoding: "utf8",
        })
        const document = DossiersLegislatifsConvert.toDocument(documentJson)
        if (documentByUid[document.uid] === undefined) {
          documentByUid[document.uid] = document
        }
      }

      for (const dossierSplitPath of walkDir(datasetDir, ["dossiers"])) {
        const dossierFilename = dossierSplitPath[dossierSplitPath.length - 1]
        if (!dossierFilename.endsWith(".json")) {
          continue
        }
        const dossierFilePath = path.join(datasetDir, ...dossierSplitPath)
        if (log) {
          console.log(`Loading file: ${dossierFilePath}…`)
        }
        const dossierJson: string = fs.readFileSync(dossierFilePath, {
          encoding: "utf8",
        })
        const dossier = DossiersLegislatifsConvert.toDossierParlementaire(dossierJson)
        const titreDossier = dossier.titreDossier
        if (
          titreDossier.senatChemin &&
          dossierParlementaireBySenatChemin[titreDossier.senatChemin] === undefined
        ) {
          dossierParlementaireBySenatChemin[titreDossier.senatChemin] = dossier
        }
        if (
          titreDossier.titreChemin &&
          dossierParlementaireByTitreChemin[titreDossier.titreChemin] === undefined
        ) {
          dossierParlementaireByTitreChemin[titreDossier.titreChemin] = dossier
        }
        if (dossierParlementaireByUid[dossier.uid] === undefined) {
          dossierParlementaireByUid[dossier.uid] = dossier
        }
      }
    }
  }

  let photoByUid: { [uid: string]: Photo } = {}
  if (enabledDatasets & EnabledDatasets.Photos) {
    {
      const jsonFilePath: string = path.join(
        dataDir,
        `photos_deputes_${
          // TODO: Handle Legislature.All.
          legislature === Legislature.All ? Legislature.Quinze : legislature
        }`,
        "deputes.json",
      )
      if (fs.existsSync(jsonFilePath)) {
        if (log) {
          console.log(`Loading file: ${jsonFilePath}`)
        }
        const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
        Object.assign(photoByUid, JSON.parse(json))
      } else if (log) {
        console.log(`Ignoring missing file: ${jsonFilePath}`)
      }
    }

    {
      const jsonFilePath: string = path.join(
        dataDir,
        "photos_senateurs",
        "senateurs.json",
      )
      if (fs.existsSync(jsonFilePath)) {
        if (log) {
          console.log(`Loading file: ${jsonFilePath}`)
        }
        const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
        Object.assign(photoByUid, JSON.parse(json))
      } else if (log) {
        console.log(`Ignoring missing file: ${jsonFilePath}`)
      }
    }
  }

  const scrutinByUid: { [uid: string]: Scrutin } = {}
  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const datasetDir = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye"),
      )

      for (const scrutinSplitPath of walkDir(datasetDir)) {
        const scrutinFilename = scrutinSplitPath[scrutinSplitPath.length - 1]
        if (!scrutinFilename.endsWith(".json")) {
          continue
        }
        const scrutinFilePath = path.join(datasetDir, ...scrutinSplitPath)
        if (log) {
          console.log(`Loading file: ${scrutinFilePath}…`)
        }
        const scrutinJson: string = fs.readFileSync(scrutinFilePath, {
          encoding: "utf8",
        })
        const scrutin = ScrutinsConvert.toScrutin(scrutinJson)
        if (scrutinByUid[scrutin.uid] === undefined) {
          scrutinByUid[scrutin.uid] = scrutin
        }
      }
    }
  }

  return {
    acteurByUid,
    amendementByUid,
    documentByUid,
    dossierParlementaireBySenatChemin,
    dossierParlementaireByTitreChemin,
    dossierParlementaireByUid,
    organeByUid,
    photoByUid,
    reunionByUid,
    reunionsByDay,
    reunionsByDossierUid,
    scrutinByUid,
  }
}

/** Load data from big monolytic files. */
export function loadAssembleeDataFromBigFiles(
  dataDir: string,
  enabledDatasets: EnabledDatasets,
  legislature: Legislature,
  { log = false } = {},
): Data {
  const acteurByUid: { [uid: string]: Acteur } = {}
  const organeByUid: { [uid: string]: Organe } = {}
  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      if (log) {
        console.log(`Loading file: ${jsonFilePath}`)
      }
      const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
      const acteursEtOrganes = ActeursEtOrganesConvert.toActeursEtOrganes(json)
      for (const acteur of acteursEtOrganes.acteurs) {
        // Keep the first existing "acteur", because the first wrapper is the most complete.
        if (acteurByUid[acteur.uid] === undefined) {
          acteurByUid[acteur.uid] = acteur
        }
      }
      for (const organe of acteursEtOrganes.organes) {
        // Keep the first existing "organe", because the first wrapper is the most complete.
        if (organeByUid[organe.uid] === undefined) {
          organeByUid[organe.uid] = organe
        }
      }
    }
  }

  const reunionByUid: { [uid: string]: Reunion } = {}
  const reunionsByDay: { [day: string]: Reunion[] } = {}
  const reunionsByDossierUid: { [dossierUid: string]: Reunion[] } = {}
  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      if (log) {
        console.log(`Loading file: ${jsonFilePath}`)
      }
      const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
      const agendas = AgendasConvert.toAgendas(json)
      for (const reunion of agendas.reunions) {
        if (reunionByUid[reunion.uid] === undefined) {
          reunionByUid[reunion.uid] = reunion
        }

        const day = reunion.timestampDebut.toISOString().split("T")[0]
        let reunionsAtDay = reunionsByDay[day]
        if (reunionsAtDay === undefined) {
          reunionsAtDay = reunionsByDay[day] = []
        }
        reunionsAtDay.push(reunion)

        const odj = reunion.odj
        if (odj !== undefined) {
          for (const pointOdj of odj.pointsOdj || []) {
            for (const dossierLegislatifRef of pointOdj.dossiersLegislatifsRefs || []) {
              let reunionsAtDossierUid = reunionsByDossierUid[dossierLegislatifRef]
              if (reunionsAtDossierUid === undefined) {
                reunionsAtDossierUid = reunionsByDossierUid[dossierLegislatifRef] = []
              }
              reunionsAtDossierUid.push(reunion)
            }
          }
        }
      }
    }
  }

  const amendementByUid: { [uid: string]: Amendement } = {}
  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      if (log) {
        console.log(`Loading file: ${jsonFilePath}`)
      }
      const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
      const amendements = AmendementsConvert.toAmendements(json)
      for (const texteLegislatif of amendements.textesLegislatifs) {
        for (const amendement of texteLegislatif.amendements) {
          if (amendementByUid[amendement.uid] === undefined) {
            amendementByUid[amendement.uid] = amendement
          }
        }
      }
    }
  }

  const documentByUid: { [uid: string]: Document } = {}
  const dossierParlementaireBySenatChemin: {
    [uid: string]: DossierParlementaire
  } = {}
  const dossierParlementaireByTitreChemin: {
    [uid: string]: DossierParlementaire
  } = {}
  const dossierParlementaireByUid: { [uid: string]: DossierParlementaire } = {}
  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      if (log) {
        console.log(`Loading file: ${jsonFilePath}`)
      }
      const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
      const dossiersLegislatifs = DossiersLegislatifsConvert.toDossiersLegislatifs(json)
      for (const document of dossiersLegislatifs.textesLegislatifs) {
        if (documentByUid[document.uid] === undefined) {
          documentByUid[document.uid] = document
        }
      }
      for (const dossier of dossiersLegislatifs.dossiersParlementaires) {
        const titreDossier = dossier.titreDossier
        if (
          titreDossier.senatChemin &&
          dossierParlementaireBySenatChemin[titreDossier.senatChemin] === undefined
        ) {
          dossierParlementaireBySenatChemin[titreDossier.senatChemin] = dossier
        }
        if (
          titreDossier.titreChemin &&
          dossierParlementaireByTitreChemin[titreDossier.titreChemin] === undefined
        ) {
          dossierParlementaireByTitreChemin[titreDossier.titreChemin] = dossier
        }
        if (dossierParlementaireByUid[dossier.uid] === undefined) {
          dossierParlementaireByUid[dossier.uid] = dossier
        }
      }
    }
  }

  const photoByUid: { [uid: string]: Photo } = {}
  if (enabledDatasets & EnabledDatasets.Photos) {
    const photoByUid = {}

    {
      const jsonFilePath: string = path.join(
        dataDir,
        `photos_deputes_${
          // TODO: Handle Legislature.All.
          legislature === Legislature.All ? Legislature.Quinze : legislature
        }`,
        "deputes.json",
      )
      if (fs.existsSync(jsonFilePath)) {
        if (log) {
          console.log(`Loading file: ${jsonFilePath}`)
        }
        const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
        Object.assign(photoByUid, JSON.parse(json))
      } else if (log) {
        console.log(`Ignoring missing file: ${jsonFilePath}`)
      }
    }

    {
      const jsonFilePath: string = path.join(
        dataDir,
        "photos_senateurs",
        "senateurs.json",
      )
      if (fs.existsSync(jsonFilePath)) {
        if (log) {
          console.log(`Loading file: ${jsonFilePath}`)
        }
        const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
        Object.assign(photoByUid, JSON.parse(json))
      } else if (log) {
        console.log(`Ignoring missing file: ${jsonFilePath}`)
      }
    }
  }

  const scrutinByUid: { [uid: string]: Scrutin } = {}
  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      if (dataset.ignoreForWeb) {
        continue
      }
      if (
        legislature !== Legislature.All &&
        dataset.legislature !== Legislature.All &&
        legislature !== dataset.legislature
      ) {
        continue
      }
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      if (log) {
        console.log(`Loading file: ${jsonFilePath}`)
      }
      const json: string = fs.readFileSync(jsonFilePath, { encoding: "utf8" })
      const scrutins = ScrutinsConvert.toScrutins(json)
      for (const scrutin of scrutins.scrutins) {
        if (scrutinByUid[scrutin.uid] === undefined) {
          scrutinByUid[scrutin.uid] = scrutin
        }
      }
    }
  }

  return {
    acteurByUid,
    amendementByUid,
    documentByUid,
    dossierParlementaireBySenatChemin,
    dossierParlementaireByTitreChemin,
    dossierParlementaireByUid,
    organeByUid,
    photoByUid,
    reunionByUid,
    reunionsByDay,
    reunionsByDossierUid,
    scrutinByUid,
  }
}
