import glob from "glob"
import fs from "fs"
import path from "path"

export function* walkDir(
  rootDir: string,
  relativeSplitDir: string[] = [],
): Iterable<string[]> {
  const dir = path.join(rootDir, ...relativeSplitDir)
  for (const filename of fs.readdirSync(dir)) {
    if (filename[0] === ".") {
      continue
    }
    const filePath = path.join(dir, filename)
    const relativeSplitPath = [...relativeSplitDir, filename]
    if (fs.statSync(filePath).isDirectory()) {
      yield* walkDir(rootDir, relativeSplitPath)
    } else {
      yield relativeSplitPath
    }
  }
}

export function load(path: string): any {
  const rawjson: string = fs.readFileSync(path, {
    encoding: "utf8",
  })
  return JSON.parse(rawjson)
}

export function getFiles(args: any): any {
  let files: string[] = []
  function _getFiles(fileOrPattern: any) {
    if (glob.hasMagic(fileOrPattern)) {
      const dataFiles = glob.sync(fileOrPattern, { cwd: process.cwd() })
      files = files.concat(dataFiles)
    } else {
      files.push(fileOrPattern)
    }
  }
  args.forEach(_getFiles)
  return files
}
