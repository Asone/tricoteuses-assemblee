import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"
import {detailedDiff } from 'deep-object-diff';
import { datasets, DatasetStructure } from "../datasets"
import { walkDir } from "../file_systems"
const diffedAmendments: Array<any> = [];
const newAmendments: Array<any> = [];

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
      alias: "m",
      help: "Diff modes.",
      name: "mode",
      defaultValue: 'merged',
      type: String
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
  {
    alias: 'p',
    help: 'Directory containing the previous dump of amendments',
    name: 'previous',
    type: String
  },
  {
    alias: 'o',
    help: 'Output directory for diff files',
    name: 'output',
    defaultValue: './diff_outputs/',
    type: String
  }
  
]
const options = commandLineArgs(optionsDefinitions)

function diffAmendements(dataDir: string) {
  if(!options.previous) {
    console.error('No previous data folder specified. You must provide one with the -p or --previous option. Exiting...');
    process.exit();
  }

  if(!fs.existsSync(options.output)) {
    console.error('The output dir does not exists. You must provide an existing directory. Default directory is ./diff_outputs/')
    process.exit();
  }

  for (const dataset of datasets.amendements) {
    switch (dataset.structure) {
      case DatasetStructure.SegmentedFiles:
        {
          const originalJsonDir: string = path.join(dataDir, dataset.filename)
          if (!options.silent) {
            console.log(`Diffing ${originalJsonDir} folder...`);
          }

          for (const amendementSplitPath of walkDir(originalJsonDir)) {

            const newFilePath = originalJsonDir + '/' +  amendementSplitPath.join('/');
            const newFile = JSON.parse(fs.readFileSync(newFilePath).toLocaleString());

            const oldFilePath = options.previous + dataset.filename + '/'  + amendementSplitPath.join('/');
            const oldFileExists = fs.existsSync(oldFilePath);
            if(!oldFileExists) { // If file found does not exist in the previous dump
              if(!options.silent) {
                console.log('File did not exists previously. Guessing it\'s a new amendment. Adding to the diff list for new objects')
              }
              addToQueueForNewAmendments(newFile,amendementSplitPath);
              continue;
            }

            // File already existed in previous dump
            let oldFile: JSON = JSON.parse('{}');
            try{
              // Gather data of the file and retranscript as a Json object
              oldFile = JSON.parse(fs.readFileSync(oldFilePath).toLocaleString());
            }catch(e){
              // Skip the process to the next iteration in case of corrupt JSON data
              continue;
            }

            // Generate the diff beetween the two JSON objects (old & new dump)
            const diff: any = detailedDiff(oldFile,newFile);

            // If no changes beetween the two dumps
            if(Object.keys(diff.added).length === 0 
              && Object.keys(diff.updated).length === 0 
              && Object.keys(diff.deleted).length === 0 ) {
                // Skip to next iteration as there is no need to generate a file
                continue;
            } else {
              if(!options.silent) {
                console.log('Changes found !');
              }
              addToQueueForDiffedAmendments(diff,amendementSplitPath);
            }
            break;
          }

        }
      }

      if(!options.silent){
        console.log('Generating output(s)');
      }

      generateDiffFiles();
  }
}

/**
 * General process for output of diffs.
 * The script handles two different modes : 
 * - merged : Will output a single file for new amendements and a single file 
 *            for updated amendements
 * - splitted : Will output a single file for each amendments, either its new or updated.
 * 
 */
function generateDiffFiles() {

  // Handles merged mode
  if(options.mode === 'merged') {
    if(!options.silent) {
      console.log('Generating merged files');
    }

    if(diffedAmendments.length > 0){
      if(!options.silent){
        console.log('Generating diffed amendments files');
      }
      fs.writeFileSync(options.output + 'amendements_XV_diffed.json' ,JSON.stringify(diffedAmendments),{encoding:'utf8',flag:'w'})
    } else {
      if(!options.silent){
        console.log('No diff in the already existing amendments');
      }
    }

    if(newAmendments.length > 0){
      if(!options.silent){
        console.log('Generating new amendments file');
      }
      fs.writeFileSync(options.output + 'amendements_XV_new.json' ,JSON.stringify(newAmendments),{encoding:'utf8',flag:'w'})
    } else {
      if(!options.silent){
        console.log('No new amendement. Skipping file generation');
      }
    }
    
  } 
  else if(options.mode === 'splitted') // Handles splitted mode
  {
    if(!options.silent) {
      console.log('Generating separated files');
    }

    /**
     * Process the copy action of the new and diffed amendments
     */

    diffedAmendments.forEach((diffedAmendment: any) => {
      generateSingleDiffedAmendment(diffedAmendment);
    });

    newAmendments.forEach((newAmendment: any) => {
      generateSingleNewAmendment(newAmendment);
    });

  } else {
    console.error('Unknown output mode. Aborting...');
    process.exit();
  }
};

// Pushes an amendment to the queue that will be processed to copy new amendments files
function addToQueueForNewAmendments(file: any,path: any): void {
  const amendment: any = {
    path: path,
    data: file
  }
  newAmendments.push(amendment);
}
// Pushes an amendment to the queue that will be processed to copy updated amendments files
function addToQueueForDiffedAmendments(diffedData: any, path: any): void {
  const amendment: any = {
    path: path,
    data: diffedData
  }
  diffedAmendments.push(amendment);
}

// generates a file with a copy of the new amendement
function generateSingleNewAmendment(newAmendment: {path: Array<string>, data: any}) {
  newAmendment.path.shift();
  
  const shortDir: Array<any> = [...newAmendment.path];
  shortDir.pop();
  console.log(options.output + shortDir.join('/'));
  fs.mkdirSync(options.output + shortDir.join('/'),{
    recursive: true
  });
  fs.writeFileSync(options.output + newAmendment.path.join('/').replace(/(\.json)/,".new$1" ), JSON.stringify(newAmendment.data),{encoding:'utf8',flag:'w'});

}

// generates a file with a copy of the modified data of the previously existing amendment;
function generateSingleDiffedAmendment(diffedAmendment: { path: Array<string>, data: any }) {
  diffedAmendment.path.shift();
  
  const shortDir: Array<any> = [...diffedAmendment.path];
  shortDir.pop();
  fs.mkdirSync(options.output + shortDir.join('/'),{
    recursive: true
  });

  fs.writeFileSync(options.output + diffedAmendment.path.join('/').replace(/(\.json)/,".diff$1" ), JSON.stringify(diffedAmendment.data),{encoding:'utf8',flag:'w'});
}

diffAmendements(options.dataDir);
