import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
// import fetch from "node-fetch"
import path from "path"
// import stream from "stream"
// import util from "util"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeData } from "../loaders"
import {
  Acteur,
  CodeTypeOrgane,
  Mandat,
  Photo,
  TypeMandat,
} from "../types/acteurs_et_organes"

const optionsDefinitions = [
  {
    alias: "C",
    help: "clone repositories from given group (or organization) git URL",
    name: "clone",
    type: String,
  },
  {
    alias: "c",
    help: "commit photos",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "f",
    help: "fetch sénateurs' pictures instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "r",
    help: "push commit to given remote",
    multiple: true,
    name: "remote",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
// const pipeline = util.promisify(stream.pipeline)

function commitAndPush(
  repositoryDir: string,
  commit: boolean,
  remotes?: string[],
): boolean {
  let error = false
  if (commit) {
    execSync("git add .", {
      cwd: repositoryDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    try {
      execSync('git commit -m "Nouvelle moisson"', {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
      })
    } catch (childProcess) {
      if (
        childProcess.stderr === null ||
        !/nothing to commit/.test(childProcess.stdout)
      ) {
        console.error(childProcess.output)
        throw childProcess
      }
    }
    for (const remote of remotes || []) {
      try {
        execSync(`git push ${remote} master`, {
          cwd: repositoryDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      } catch (childProcess) {
        // Don't stop when push fails.
        console.error(childProcess.output)
        error = true
      }
    }
  }
  return error
}

async function retrievePhotosSenateurs(): Promise<boolean> {
  const dataDir = options.dataDir
  const { acteurByUid } = loadAssembleeData(
    dataDir,
    EnabledDatasets.ActeursEtOrganes,
    options.legislature,
  )
  const senateurs = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatSenateur = mandats.filter(
        (mandat: Mandat) =>
          mandat.xsiType === TypeMandat.MandatParlementaireType &&
          // mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeTypeOrgane.Senat,
      )[0]
      if (mandatSenateur === undefined) {
        return false
      }
      const { adresses } = acteur
      if (adresses === undefined) {
        return false
      }
      let url = null
      for (const adresse of adresses) {
        if (
          adresse.xsiType === "AdresseSiteWeb_Type" &&
          adresse.typeLibelle === "Url sénateur"
        ) {
          url = adresse.valElec
        }
      }
      return Boolean(url)
    })
    .sort((a, b) =>
      a.uid.length === b.uid.length
        ? a.uid.localeCompare(b.uid)
        : a.uid.length - b.uid.length,
    )

  const gitGroupUrl = options.clone ? options.clone.trim().replace(/\/+$/, "") : undefined
  const photosDirName = "photos_senateurs"
  if (gitGroupUrl !== undefined) {
    execSync(`git clone ${gitGroupUrl}/${photosDirName}.git`, {
      cwd: dataDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
  }
  const photosDir = path.join(dataDir, photosDirName)

  // Download photos.
  fs.ensureDirSync(photosDir)
  if (options.fetch) {
    const missingPhotoFilePath = "images/transparent_155x225.jpg"
    for (const senateur of senateurs) {
      const ident = senateur.etatCivil.ident
      const senateurStem = stemFromSenateur(senateur)
      const photoFilename = `${senateurStem}.jpg`
      const photoFilePath = path.join(photosDir, photoFilename)
      const photoTempFilename = `${senateurStem}_temp.jpg`
      const photoTempFilePath = path.join(photosDir, photoTempFilename)
      const urlPhoto = `https://www.senat.fr/senimg/${photoFilename}`
      if (!options.silent) {
        console.log(`Loading photo ${urlPhoto} for ${ident.prenom} ${ident.nom}…`)
      }
      // for (let retries = 0; retries < 3; retries++) {
      //   const response = await fetch(urlPhoto)
      //   if (response.ok) {
      //     await pipeline(response.body, fs.createWriteStream(photoTempFilePath))
      //     fs.renameSync(photoTempFilePath, photoFilePath)
      //     break
      //   }
      //   if (retries >= 2) {
      //     console.warn(`Fetch failed: ${urlPhoto} (${ident.prenom} ${ident.nom})`)
      //     console.warn(response.status, response.statusText)
      //     console.warn(await response.text())
      //     if (fs.existsSync(photoFilePath)) {
      //       console.warn("  => Reusing existing image")
      //     } else {
      //       console.warn("  => Using blank image")
      //       fs.copyFileSync(missingPhotoFilePath, photoFilePath)
      //     }
      //     break
      //   }
      // }
      try {
        execSync(`wget --quiet -O ${photoTempFilename} ${urlPhoto}`, {
          cwd: photosDir,
          env: process.env,
          encoding: "utf-8",
          // stdio: ["ignore", "ignore", "pipe"],
        })
        fs.renameSync(photoTempFilePath, photoFilePath)
      } catch (error) {
        console.error(error)
        if (fs.existsSync(photoFilePath)) {
          console.warn("  => Reusing existing image")
        } else {
          console.warn("  => Using blank image")
          fs.copyFileSync(missingPhotoFilePath, photoFilePath)
        }
      }
    }
  }

  // Resize photos to 155x225, because some haven't exactly this size.
  for (const senateur of senateurs) {
    const ident = senateur.etatCivil.ident
    const senateurStem = stemFromSenateur(senateur)
    if (!options.silent) {
      console.log(`Resizing photo ${senateurStem} for ${ident.prenom} ${ident.nom}…`)
    }
    execSync(
      `gm convert -resize 155x225! ${senateurStem}.jpg ${senateurStem}_155x225.jpg`,
      {
        cwd: photosDir,
      },
    )
  }

  // Create a mosaic of photos.
  const photoByIdSenateur: { [uid: string]: Photo } = {}
  const rowsFilenames: string[] = []
  for (
    let senateurIndex = 0, rowIndex = 0;
    senateurIndex < senateurs.length;
    senateurIndex += 25, rowIndex++
  ) {
    const row = senateurs.slice(senateurIndex, senateurIndex + 25)
    const photosFilenames: string[] = []
    for (const [columnIndex, senateur] of row.entries()) {
      const senateurStem = stemFromSenateur(senateur)
      const photoFilename = `${senateurStem}_155x225.jpg`
      photosFilenames.push(photoFilename)
      photoByIdSenateur[senateur.uid] = {
        chemin: `photos_senateurs/${photoFilename}`,
        cheminMosaique: `photos_senateurs/senateurs.jpg`,
        hauteur: 225,
        largeur: 155,
        xMosaique: columnIndex * 155,
        yMosaique: rowIndex * 225,
      }
    }
    const rowFilename = `row-${rowIndex}.jpg`
    execSync(`gm convert ${photosFilenames.join(" ")} +append ${rowFilename}`, {
      cwd: photosDir,
    })
    rowsFilenames.push(rowFilename)
  }
  execSync(`gm convert ${rowsFilenames.join(" ")} -append senateurs.jpg`, {
    cwd: photosDir,
  })
  for (const rowFilename of rowsFilenames) {
    fs.unlinkSync(path.join(photosDir, rowFilename))
  }

  const jsonFilePath = path.join(photosDir, "senateurs.json")
  fs.writeFileSync(jsonFilePath, JSON.stringify(photoByIdSenateur, null, 2))

  return commitAndPush(photosDir, options.commit, options.remote)
}

function stemFromSenateur(senateur: Acteur) {
  const { adresses } = senateur
  let url = null
  for (const adresse of adresses!) {
    if (
      adresse.xsiType === "AdresseSiteWeb_Type" &&
      adresse.typeLibelle === "Url sénateur"
    ) {
      url = adresse.valElec
    }
  }
  const match = url!.match(/^https:\/\/www\.senat\.fr\/senateur\/(.*)\.html$/)
  const senateurStem = match![1]
  return senateurStem
}

retrievePhotosSenateurs()
  .then(error => process.exit(error ? 1 : 0))
  .catch(error => {
    console.log(error)
    process.exit(2)
  })
