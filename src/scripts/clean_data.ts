import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs"
import path from "path"

import { cleanActeur } from "../cleaners/acteurs"
import { cleanTexteLegislatif } from "../cleaners/amendements"
import { cleanDocumentOrDivision } from "../cleaners/documents"
import { cleanDossierParlementaire } from "../cleaners/dossiers_legislatifs"
import { cleanOrgane } from "../cleaners/organes"
import { cleanReunion } from "../cleaners/reunions"
import { cleanScrutin } from "../cleaners/scrutins"
import { datasets, EnabledDatasets } from "../datasets"
import { existingDateToJson, patchedDateToJson } from "../dates"
import {
  ActeursEtOrganes,
  Convert as ActeursEtOrganesConvert,
} from "../types/acteurs_et_organes"
import { Agendas, Convert as AgendasConvert } from "../types/agendas"
import { Amendements, Convert as AmendementsConvert } from "../types/amendements"
import {
  DossiersLegislatifs,
  Convert as DossiersLegislatifsConvert,
} from "../types/dossiers_legislatifs"
import { Convert as ScrutinsConvert, Scrutins } from "../types/scrutins"
import { Convert as ActeursEtOrganesRawConvert } from "../raw_types/acteurs_et_organes"
import { Convert as AgendasRawConvert } from "../raw_types/agendas"
import { Convert as AmendementsRawConvert } from "../raw_types/amendements"
import {
  Convert as DossiersLegislatifsRawConvert,
  Dossier as DossierRaw,
} from "../raw_types/dossiers_legislatifs"
import { Convert as ScrutinsRawConvert } from "../raw_types/scrutins"

const optionsDefinitions = [
  {
    alias: "k",
    defaultValue: ["All"],
    help: "categories of datasets to reorganize",
    multiple: true,
    name: "categories",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function cleanData(dataDir: string): void {
  Date.prototype.toJSON = patchedDateToJson

  options.categories.map((datasetName: string) =>
    assert.notStrictEqual(
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
      undefined,
      `Ùnknown name of dataset: ${datasetName}`,
    ),
  )
  const enabledDatasets = options.categories.reduce(
    (enabledDatasets: EnabledDatasets, datasetName: string): EnabledDatasets =>
      enabledDatasets |
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
    EnabledDatasets.None,
  )

  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      const rawJsonFilePath: string = path.join(dataDir, dataset.filename)
      console.log(`Loading file: ${rawJsonFilePath}`)
      const rawJson: string = fs.readFileSync(rawJsonFilePath, {
        encoding: "utf8",
      })

      const acteursEtOrganes: any = ActeursEtOrganesRawConvert.toActeursEtOrganes(rawJson)
      // const acteurs = jp.query(acteursEtOrganes, "$.export.acteurs.acteur")[0]
      const organes = acteursEtOrganes.export.organes.organe
      acteursEtOrganes.organes = organes
      const acteurs = acteursEtOrganes.export.acteurs.acteur
      acteursEtOrganes.acteurs = acteurs
      delete acteursEtOrganes.export

      for (const acteur of acteurs) {
        cleanActeur(acteur)
      }

      for (const organe of organes) {
        cleanOrgane(organe)
      }

      const acteursEtOrganesJson: string = JSON.stringify(acteursEtOrganes, null, 2)
      // Validate generated JSON.
      const acteursEtOrganesClean: ActeursEtOrganes = ActeursEtOrganesConvert.toActeursEtOrganes(
        acteursEtOrganesJson,
      )
      const acteursEtOrganesCleanJson: string = ActeursEtOrganesConvert.acteursEtOrganesToJson(
        acteursEtOrganesClean,
      )
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      fs.writeFileSync(jsonFilePath, acteursEtOrganesCleanJson, {
        encoding: "utf8",
      })
    }
  }

  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      const rawJsonFilePath: string = path.join(dataDir, dataset.filename)
      console.log(`Loading file: ${rawJsonFilePath}`)
      const rawJson: string = fs.readFileSync(rawJsonFilePath, {
        encoding: "utf8",
      })

      const agendas: any = AgendasRawConvert.toAgendas(rawJson)
      agendas.reunions = agendas.reunions.reunion

      for (const reunion of agendas.reunions) {
        cleanReunion(reunion)
      }

      const agendasJson: string = JSON.stringify(agendas, null, 2)
      // Validate generated JSON.
      const agendasClean: Agendas = AgendasConvert.toAgendas(agendasJson)
      const agendasCleanJson: string = AgendasConvert.agendasToJson(agendasClean)
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      fs.writeFileSync(jsonFilePath, agendasCleanJson, { encoding: "utf8" })
    }
  }

  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      const rawJsonFilePath: string = path.join(dataDir, dataset.filename)
      console.log(`Loading file: ${rawJsonFilePath}`)
      const rawJson: string = fs.readFileSync(rawJsonFilePath, {
        encoding: "utf8",
      })

      const amendements: any = AmendementsRawConvert.toAmendements(rawJson)
      amendements.textesLegislatifs = amendements.textesEtAmendements.texteleg
      delete amendements.textesEtAmendements

      for (const texteLegislatif of amendements.textesLegislatifs) {
        cleanTexteLegislatif(texteLegislatif)
      }

      const amendementsJson: string = JSON.stringify(amendements, null, 2)
      // Validate generated JSON.
      const amendementsClean: Amendements = AmendementsConvert.toAmendements(
        amendementsJson,
      )
      const amendementsCleanJson: string = AmendementsConvert.amendementsToJson(
        amendementsClean,
      )
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      fs.writeFileSync(jsonFilePath, amendementsCleanJson, { encoding: "utf8" })
    }
  }

  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      const rawJsonFilePath: string = path.join(dataDir, dataset.filename)
      console.log(`Loading file: ${rawJsonFilePath}`)
      const rawJson: string = fs.readFileSync(rawJsonFilePath, {
        encoding: "utf8",
      })

      const dossiersLegislatifs: any = DossiersLegislatifsRawConvert.toDossiersLegislatifs(
        rawJson,
      )
      dossiersLegislatifs.textesLegislatifs =
        dossiersLegislatifs.export.textesLegislatifs.document
      dossiersLegislatifs.dossiersParlementaires = (dossiersLegislatifs.export
        .dossiersLegislatifs.dossier as DossierRaw[]).map(
        dossier => dossier.dossierParlementaire,
      )
      delete dossiersLegislatifs.export

      for (const document of dossiersLegislatifs.textesLegislatifs) {
        cleanDocumentOrDivision(document)
      }

      for (const dossierParlementaire of dossiersLegislatifs.dossiersParlementaires) {
        cleanDossierParlementaire(dossierParlementaire)
      }

      const dossiersLegislatifsJson: string = JSON.stringify(dossiersLegislatifs, null, 2)
      // Validate generated JSON.
      const dossiersLegislatifsClean: DossiersLegislatifs = DossiersLegislatifsConvert.toDossiersLegislatifs(
        dossiersLegislatifsJson,
      )
      const dossiersLegislatifsCleanJson: string = DossiersLegislatifsConvert.dossiersLegislatifsToJson(
        dossiersLegislatifsClean,
      )
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      fs.writeFileSync(jsonFilePath, dossiersLegislatifsCleanJson, {
        encoding: "utf8",
      })
    }
  }

  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      const rawJsonFilePath: string = path.join(dataDir, dataset.filename)
      console.log(`Loading file: ${rawJsonFilePath}`)
      const rawJson: string = fs.readFileSync(rawJsonFilePath, {
        encoding: "utf8",
      })

      const scrutins: any = ScrutinsRawConvert.toScrutins(rawJson)
      scrutins.scrutins = scrutins.scrutins.scrutin

      for (const scrutin of scrutins.scrutins) {
        cleanScrutin(scrutin)
      }

      const scrutinsJson: string = JSON.stringify(scrutins, null, 2)
      // Validate generated JSON.
      const scrutinsClean: Scrutins = ScrutinsConvert.toScrutins(scrutinsJson)
      const scrutinsCleanJson: string = ScrutinsConvert.scrutinsToJson(scrutinsClean)
      const jsonFilePath: string = path.join(
        dataDir,
        dataset.filename.replace(/\.json$/, "_nettoye.json"),
      )
      fs.writeFileSync(jsonFilePath, scrutinsCleanJson, { encoding: "utf8" })
    }
  }

  // Restore standard conversion of dates to JSON.
  Date.prototype.toJSON = existingDateToJson
}

cleanData(options.dataDir)
