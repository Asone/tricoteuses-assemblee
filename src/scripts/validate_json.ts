import Ajv from "ajv"
import commandLineArgs from "command-line-args"
import commandLineUsage from "command-line-usage"
import { execSync } from "child_process"
import { load, getFiles } from "../file_systems"
import temp from "temp"
import fs from "fs-extra"
import path from "path"

//temp.track()

function parseArgs(argv: string[]): any {
  const optionsDefinitions = [
    {
      description: "pathname to a JSON file or a glob pattern to JSON files",
      defaultOption: true,
      multiple: true,
      name: "data",
      type: String,
    },
    {
      description: "URL or path to the tricoteuses-assemblee git repository",
      defaultValue:
        "https://git.en-root.org/tricoteuses/tricoteuses-assemblee.git",
      name: "repository",
      type: String,
    },
    {
      description:
        "pathname to a schema referenced by --schema, relative to https://git.en-root.org/tricoteuses/tricoteuses-assemblee/tree/master/src/schemas",
      multiple: true,
      name: "reference",
      alias: "r",
      type: String,
    },
    {
      description:
        "pathname to the schema against which the JSON data must validate, relative to https://git.en-root.org/tricoteuses/tricoteuses-assemblee/tree/master/src/schemas",
      name: "schema",
      alias: "s",
      type: String,
    },
    {
      description:
        "ignore schemaVersion from JSON files and use the current directory instead of the --repository",
      name: "dev",
      type: Boolean,
    },
    {
      description: "increase verbosity",
      name: "verbose",
      type: Boolean,
    },
    {
      name: "help",
      description: "Print this usage guide.",
    },
  ]
  const sections = [
    {
      header: "Validate JSON files",
      content: "Validate JSON files",
    },
    {
      header: "Options",
      optionList: optionsDefinitions,
    },
  ]
  const options = commandLineArgs(optionsDefinitions, {
    argv: argv,
  })
  if (!("help" in options) && !("data" in options)) {
    console.error("--data is mandatory")
    options.help = true
  }
  if ("help" in options) {
    const usage = commandLineUsage(sections)
    console.warn(usage)
    return null
  }
  return options
}

class Validate {
  options: any
  version2schema: any
  tmpDir: string

  constructor(options: any) {
    this.options = options
    this.version2schema = {}
    this.tmpDir = temp.mkdirSync("validation")
  }

  getSchemaDir(tag: string) {
    if (this.options.dev == true) {
      if (this.options.verbose) console.log("--dev: use src/schemas")
      return "src/schemas"
    }
    const repositoryDir = path.join(this.tmpDir, "tricoteuses-assemblee")
    const tagDir = path.join(this.tmpDir, tag)
    if (!fs.existsSync(tagDir)) {
      if (!fs.existsSync(repositoryDir)) {
        execSync(`git clone ${this.options.repository} ${repositoryDir}`, {
          cwd: this.tmpDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      execSync(`git checkout tags/${tag}`, {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
        stdio: ["ignore", "ignore", "pipe"],
      })
      fs.renameSync(path.join(repositoryDir, "src/schemas"), tagDir)
    }
    return tagDir
  }

  getValidator(version: string): any {
    if (!(version in this.version2schema)) {
      const ajv = new Ajv()
      const dir = this.getSchemaDir(`schema-${version}`)
      let references = []
      for (const reference of this.options.reference)
        references.push(path.join(dir, reference))
      for (const file of getFiles(references)) {
        if (this.options.verbose) console.log(`--reference ${file}`)
        ajv.addSchema(load(file))
      }
      const s = path.join(dir, this.options.schema)
      if (this.options.verbose) console.log(`--schema ${s}`)
      this.version2schema[version] = ajv.compile(load(s))
    }
    return this.version2schema[version]
  }

  getVersion(content: any): string {
    if (content.schemaVersion != undefined) return content.schemaVersion
    return this.options.schema.split("/")[0] + "-1.0"
  }

  getSchemaURL(version: string): any {
    if (this.options.dev == true) return "src/schemas"
    else {
      if (fs.existsSync(this.options.repository)) {
        return `${this.options.repository}/src/schemas at tag schema-${version}`
      } else {
        const repository = this.options.repository.replace(/\.git$/, "")
        return `${repository}/tree/schema-${version}/src/schemas`
      }
    }
  }

  validate(content: any, filename: string): number {
    const tag = this.getVersion(content)
    const validator = this.getValidator(tag)
    const valid = validator(content)
    const schemaURL = this.getSchemaURL(tag)
    if (!valid) {
      console.error(`${schemaURL} finds ${filename} is invalid`)
      console.error(validator.errors)
      return 1
    } else {
      if (this.options.verbose)
        console.log(`${schemaURL} finds ${filename} is valid`)
      return 0
    }
  }

  process() {
    let status = 0
    for (const file of getFiles(this.options.data)) {
      const content = load(file)
      status |= this.validate(content, file)
    }
    return status
  }
}

function main(argv: any): number {
  const options = parseArgs(argv)
  if (options === null) return 1
  const validate = new Validate(options)
  return validate.process()
}

/* istanbul ignore if */
if (process.argv[1].endsWith("validate_json.ts"))
  process.exit(main(process.argv))
