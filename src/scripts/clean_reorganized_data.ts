import assert from "assert"
import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { cleanActeur } from "../cleaners/acteurs"
import { cleanAmendement } from "../cleaners/amendements"
import { cleanDocumentOrDivision } from "../cleaners/documents"
import { cleanDossierParlementaire } from "../cleaners/dossiers_legislatifs"
import { cleanOrgane } from "../cleaners/organes"
import { cleanReunion } from "../cleaners/reunions"
import { cleanScrutin } from "../cleaners/scrutins"
import { datasets, EnabledDatasets } from "../datasets"
import { existingDateToJson, patchedDateToJson } from "../dates"
import { walkDir, load } from "../file_systems"
import {
  Acteur,
  Convert as ActeursEtOrganesConvert,
  Organe,
} from "../types/acteurs_et_organes"
import { Convert as AgendasConvert, Reunion } from "../types/agendas"
import { Amendement, Convert as AmendementsConvert } from "../types/amendements"
import {
  Convert as DossiersLegislatifsConvert,
  Document,
  DossierParlementaire,
} from "../types/dossiers_legislatifs"
import { Convert as ScrutinsConvert, Scrutin } from "../types/scrutins"

function parseArgs(argv: string[]): any {
  const optionsDefinitions = [
    {
      alias: "C",
      help: "clone repositories from given group (or organization) git URL",
      name: "clone",
      type: String,
    },
    {
      alias: "c",
      help: "commit clean files",
      name: "commit",
      type: Boolean,
    },
    {
      alias: "d",
      help: "single dataset to reorganize",
      name: "dataset",
      type: String,
    },
    {
      alias: "k",
      defaultValue: ["All"],
      help: "categories of datasets to reorganize",
      multiple: true,
      name: "categories",
      type: String,
    },
    {
      alias: "r",
      help: "push commit to given remote",
      multiple: true,
      name: "remote",
      type: String,
    },
    {
      alias: "s",
      help: "don't log anything",
      name: "silent",
      type: Boolean,
    },
    {
      alias: "v",
      help: "verbose logs",
      name: "verbose",
      type: Boolean,
    },
    {
      defaultOption: true,
      help: "directory containing Assemblée open data files",
      name: "dataDir",
      type: String,
    },
  ]
  const options = commandLineArgs(optionsDefinitions, {
    argv: argv,
  })
  return options
}

function cleanReorganizedData(options: any): boolean {
  const dataDir: string = options.dataDir

  Date.prototype.toJSON = patchedDateToJson

  let error = false
  const gitGroupUrl = options.clone
    ? options.clone.trim().replace(/\/+$/, "")
    : undefined

  options.categories.map((datasetName: string) =>
    assert.notStrictEqual(
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[
        datasetName
      ],
      undefined,
      `Unknown name of dataset: ${datasetName}`,
    ),
  )
  const enabledDatasets = options.categories.reduce(
    (enabledDatasets: EnabledDatasets, datasetName: string): EnabledDatasets =>
      enabledDatasets |
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[
        datasetName
      ],
    EnabledDatasets.None,
  )

  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      const datasetName = path.basename(dataset.filename, ".json")
      if (options.dataset && datasetName !== options.dataset) {
        continue
      }

      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}_nettoye.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      const filename2schemaVersion = analyzeAndRemove(datasetName, options)
      const datasetRawDir: string = path.join(dataDir, datasetName)
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      for (const acteurSplitPath of walkDir(datasetRawDir, ["acteurs"])) {
        const acteurFilename = acteurSplitPath[acteurSplitPath.length - 1]
        if (!acteurFilename.endsWith(".json")) {
          continue
        }
        const acteurRawFilePath = path.join(datasetRawDir, ...acteurSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${acteurRawFilePath}…`)
        }
        const acteurRawJson: string = fs.readFileSync(acteurRawFilePath, {
          encoding: "utf8",
        })
        const acteur = JSON.parse(acteurRawJson)
        setSchemaVersion(
          acteur,
          acteurFilename,
          "acteur",
          filename2schemaVersion,
        )
        cleanActeur(acteur)
        const acteurJson: string = JSON.stringify(acteur, null, 2)

        // Validate generated JSON.
        const acteurClean: Acteur = ActeursEtOrganesConvert.toActeur(acteurJson)
        const acteurCleanJson: string = ActeursEtOrganesConvert.acteurToJson(
          acteurClean,
        )

        const acteurCleanFilePath: string = path.join(
          datasetCleanDir,
          ...acteurSplitPath,
        )
        fs.ensureDirSync(path.dirname(acteurCleanFilePath))
        fs.writeFileSync(acteurCleanFilePath, acteurCleanJson, {
          encoding: "utf8",
        })
      }

      for (const organeSplitPath of walkDir(datasetRawDir, ["organes"])) {
        const organeFilename = organeSplitPath[organeSplitPath.length - 1]
        if (!organeFilename.endsWith(".json")) {
          continue
        }
        const organeRawFilePath = path.join(datasetRawDir, ...organeSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${organeRawFilePath}…`)
        }
        const organeRawJson: string = fs.readFileSync(organeRawFilePath, {
          encoding: "utf8",
        })
        const organe = JSON.parse(organeRawJson)
        setSchemaVersion(
          organe,
          organeFilename,
          "organe",
          filename2schemaVersion,
        )
        cleanOrgane(organe)
        const organeJson: string = JSON.stringify(organe, null, 2)

        // Validate generated JSON.
        const organeClean: Organe = ActeursEtOrganesConvert.toOrgane(organeJson)
        const organeCleanJson: string = ActeursEtOrganesConvert.organeToJson(
          organeClean,
        )

        const organeCleanFilePath: string = path.join(
          datasetCleanDir,
          ...organeSplitPath,
        )
        fs.ensureDirSync(path.dirname(organeCleanFilePath))
        fs.writeFileSync(organeCleanFilePath, organeCleanJson, {
          encoding: "utf8",
        })
      }

      if (commitAndPush(datasetCleanDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      const datasetName = path.basename(dataset.filename, ".json")
      if (options.dataset && datasetName !== options.dataset) {
        continue
      }

      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}_nettoye.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      const filename2schemaVersion = analyzeAndRemove(datasetName, options)
      const datasetRawDir: string = path.join(dataDir, datasetName)
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      for (const reunionSplitPath of walkDir(datasetRawDir)) {
        const reunionFilename = reunionSplitPath[reunionSplitPath.length - 1]
        if (!reunionFilename.endsWith(".json")) {
          continue
        }
        const reunionRawFilePath = path.join(datasetRawDir, ...reunionSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${reunionRawFilePath}…`)
        }
        const reunionRawJson: string = fs.readFileSync(reunionRawFilePath, {
          encoding: "utf8",
        })
        const reunion = JSON.parse(reunionRawJson)
        setSchemaVersion(
          reunion,
          reunionFilename,
          "agenda",
          filename2schemaVersion,
        )
        cleanReunion(reunion)
        const reunionJson: string = JSON.stringify(reunion, null, 2)

        // Validate generated JSON.
        const reunionClean: Reunion = AgendasConvert.toReunion(reunionJson)
        const reunionCleanJson: string = AgendasConvert.reunionToJson(
          reunionClean,
        )

        const reunionCleanFilePath: string = path.join(
          datasetCleanDir,
          ...reunionSplitPath,
        )
        fs.ensureDirSync(path.dirname(reunionCleanFilePath))
        fs.writeFileSync(reunionCleanFilePath, reunionCleanJson, {
          encoding: "utf8",
        })
      }

      if (commitAndPush(datasetCleanDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      const datasetName = path.basename(dataset.filename, ".json")
      if (options.dataset && datasetName !== options.dataset) {
        continue
      }

      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}_nettoye.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      const filename2schemaVersion = analyzeAndRemove(datasetName, options)
      const datasetRawDir: string = path.join(dataDir, datasetName)
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      for (const amendementSplitPath of walkDir(datasetRawDir)) {
        const amendementFilename =
          amendementSplitPath[amendementSplitPath.length - 1]
        if (!amendementFilename.endsWith(".json")) {
          continue
        }
        const amendementRawFilePath = path.join(
          datasetRawDir,
          ...amendementSplitPath,
        )
        if (options.verbose) {
          console.log(`  Cleaning file: ${amendementRawFilePath}…`)
        }
        const amendementRawJson: string = fs.readFileSync(
          amendementRawFilePath,
          {
            encoding: "utf8",
          },
        )
        const amendement = JSON.parse(amendementRawJson)
        setSchemaVersion(
          amendement,
          amendementFilename,
          "amendement",
          filename2schemaVersion,
        )
        cleanAmendement(amendement)
        const amendementJson: string = JSON.stringify(amendement, null, 2)

        // Validate generated JSON.
        const amendementClean: Amendement = AmendementsConvert.toAmendement(
          amendementJson,
        )
        const amendementCleanJson: string = AmendementsConvert.amendementToJson(
          amendementClean,
        )

        const amendementCleanFilePath: string = path.join(
          datasetCleanDir,
          ...amendementSplitPath,
        )
        fs.ensureDirSync(path.dirname(amendementCleanFilePath))
        fs.writeFileSync(amendementCleanFilePath, amendementCleanJson, {
          encoding: "utf8",
        })
      }

      if (commitAndPush(datasetCleanDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      const datasetName = path.basename(dataset.filename, ".json")
      if (options.dataset && datasetName !== options.dataset) {
        continue
      }

      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}_nettoye.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      const filename2schemaVersion = analyzeAndRemove(datasetName, options)
      const datasetRawDir: string = path.join(dataDir, datasetName)
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      for (const documentSplitPath of walkDir(datasetRawDir, ["documents"])) {
        const documentFilename = documentSplitPath[documentSplitPath.length - 1]
        if (!documentFilename.endsWith(".json")) {
          continue
        }
        const documentRawFilePath = path.join(
          datasetRawDir,
          ...documentSplitPath,
        )
        if (options.verbose) {
          console.log(`  Cleaning file: ${documentRawFilePath}…`)
        }
        const documentRawJson: string = fs.readFileSync(documentRawFilePath, {
          encoding: "utf8",
        })
        const document = JSON.parse(documentRawJson)
        setSchemaVersion(
          document,
          documentFilename,
          "document",
          filename2schemaVersion,
        )
        cleanDocumentOrDivision(document)
        const documentJson: string = JSON.stringify(document, null, 2)

        // Validate generated JSON.
        const documentClean: Document = DossiersLegislatifsConvert.toDocument(
          documentJson,
        )
        const documentCleanJson: string = DossiersLegislatifsConvert.documentToJson(
          documentClean,
        )

        const documentCleanFilePath: string = path.join(
          datasetCleanDir,
          ...documentSplitPath,
        )
        fs.ensureDirSync(path.dirname(documentCleanFilePath))
        fs.writeFileSync(documentCleanFilePath, documentCleanJson, {
          encoding: "utf8",
        })
      }

      for (const dossierSplitPath of walkDir(datasetRawDir, ["dossiers"])) {
        const dossierFilename = dossierSplitPath[dossierSplitPath.length - 1]
        if (!dossierFilename.endsWith(".json")) {
          continue
        }
        const dossierRawFilePath = path.join(datasetRawDir, ...dossierSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${dossierRawFilePath}…`)
        }
        const dossierRawJson: string = fs.readFileSync(dossierRawFilePath, {
          encoding: "utf8",
        })
        const dossier = JSON.parse(dossierRawJson)
        setSchemaVersion(
          dossier,
          dossierFilename,
          "dossier",
          filename2schemaVersion,
        )
        cleanDossierParlementaire(dossier)
        const dossierJson: string = JSON.stringify(dossier, null, 2)

        // Validate generated JSON.
        const dossierClean: DossierParlementaire = DossiersLegislatifsConvert.toDossierParlementaire(
          dossierJson,
        )
        const dossierCleanJson: string = DossiersLegislatifsConvert.dossierParlementaireToJson(
          dossierClean,
        )

        const dossierCleanFilePath: string = path.join(
          datasetCleanDir,
          ...dossierSplitPath,
        )
        fs.ensureDirSync(path.dirname(dossierCleanFilePath))
        fs.writeFileSync(dossierCleanFilePath, dossierCleanJson, {
          encoding: "utf8",
        })
      }

      if (commitAndPush(datasetCleanDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      const datasetName = path.basename(dataset.filename, ".json")
      if (options.dataset && datasetName !== options.dataset) {
        continue
      }

      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}_nettoye.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }
      analyzeAndRemove(datasetName, options)
      const datasetRawDir: string = path.join(dataDir, datasetName)
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      for (const scrutinSplitPath of walkDir(datasetRawDir)) {
        const scrutinFilename = scrutinSplitPath[scrutinSplitPath.length - 1]
        if (!scrutinFilename.endsWith(".json")) {
          continue
        }
        const scrutinRawFilePath = path.join(datasetRawDir, ...scrutinSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${scrutinRawFilePath}…`)
        }
        const scrutinRawJson: string = fs.readFileSync(scrutinRawFilePath, {
          encoding: "utf8",
        })
        const scrutin = JSON.parse(scrutinRawJson)
        cleanScrutin(scrutin)
        const scrutinJson: string = JSON.stringify(scrutin, null, 2)

        // Validate generated JSON.
        const scrutinClean: Scrutin = ScrutinsConvert.toScrutin(scrutinJson)
        const scrutinCleanJson: string = ScrutinsConvert.scrutinToJson(
          scrutinClean,
        )

        const scrutinCleanFilePath: string = path.join(
          datasetCleanDir,
          ...scrutinSplitPath,
        )
        fs.ensureDirSync(path.dirname(scrutinCleanFilePath))
        fs.writeFileSync(scrutinCleanFilePath, scrutinCleanJson, {
          encoding: "utf8",
        })
      }

      if (commitAndPush(datasetCleanDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  // Restore standard conversion of dates to JSON.
  Date.prototype.toJSON = existingDateToJson

  return error
}

function analyzeAndRemove(datasetName: string, options: any): any {
  const dataDir = options.dataDir
  const datasetRawDir: string = path.join(dataDir, datasetName)
  const datasetCleanDir: string = datasetRawDir + "_nettoye"
  const filename2schemaVersion: any = {}
  if (!options.silent) {
    console.log(`Cleaning directory: ${datasetCleanDir}…`)
  }
  fs.ensureDirSync(datasetCleanDir)
  for (const filename of fs.readdirSync(datasetCleanDir)) {
    if (filename[0] === ".") {
      continue
    }
    const pathname = path.join(datasetCleanDir, filename)
    for (const jsonSplitPath of walkDir(pathname)) {
      const jsonFilename = jsonSplitPath[jsonSplitPath.length - 1]
      if (!jsonFilename.endsWith(".json")) continue
      const jsonPathname = path.join(pathname, ...jsonSplitPath)
      collectSchemaVersion(jsonPathname, filename2schemaVersion)
    }
    fs.removeSync(pathname)
  }
  return filename2schemaVersion
}

function collectSchemaVersion(pathname: string, filename2schemaVersion: any) {
  const content = load(pathname)
  filename2schemaVersion[path.basename(pathname)] = content.schemaVersion
}

function setSchemaVersion(
  raw: any,
  filename: any,
  name: string,
  filename2schemaVersion: any,
) {
  if (filename in filename2schemaVersion) {
    if (filename2schemaVersion != undefined)
      raw.schemaVersion = filename2schemaVersion[filename]
  } else {
    raw.schemaVersion = getSchemaVersionDefault(name)
  }
}

let schemaVersionDefault: any = {}

function getSchemaVersionDefault(name: string): string {
  if (!(name in schemaVersionDefault)) {
    const tag = execSync(
      `git tag -l --sort=-version:refname 'schema-${name}*' | head -1`,
    ).toString()
    schemaVersionDefault[name] = tag.replace(/^schema-/, "").trim()
  }
  return schemaVersionDefault[name]
}

function commitAndPush(
  repositoryDir: string,
  commit: boolean,
  remotes?: string[],
): boolean {
  let error = false
  if (commit) {
    execSync("git add .", {
      cwd: repositoryDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    try {
      execSync('git commit -m "Nouvelle moisson"', {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
      })
    } catch (childProcess) {
      if (
        childProcess.stderr === null ||
        !/nothing to commit/.test(childProcess.stdout)
      ) {
        console.error(childProcess.output)
        throw childProcess
      }
    }
    for (const remote of remotes || []) {
      try {
        execSync(`git push ${remote} master`, {
          cwd: repositoryDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      } catch (childProcess) {
        // Don't stop when push fails.
        console.error(childProcess.output)
        error = true
      }
    }
  }
  return error
}

function main(argv: any): number {
  const options = parseArgs(argv)
  if (cleanReorganizedData(options)) {
    return 1
  } else {
    return 0
  }
}

/* istanbul ignore if */
if (process.argv[1].endsWith("clean_reorganized_data.ts"))
  process.exit(main(process.argv))
