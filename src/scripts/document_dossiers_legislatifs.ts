import commandLineArgs from "command-line-args"
import commandLineUsage from "command-line-usage"
import assert from "assert"
import { load, getFiles } from "../file_systems"

function parseArgs(argv: string[]): any {
  const optionsDefinitions = [
    {
      description: "pathname to a JSON file or a glob pattern to JSON files",
      defaultOption: true,
      multiple: true,
      name: "data",
      type: String,
    },
    {
      name: "no-sanity-checks",
      type: Boolean,
      description: "Do not perform sanity checks.",
    },
    {
      name: "verbose",
      type: Boolean,
      description: "increase verbosity",
    },
    {
      name: "help",
      description: "Print this usage guide.",
    },
  ]
  const sections = [
    {
      header: "Aide à la documentation du dossier législatif",
      content: "Génère une hiérarchie des actes législatifs",
    },
    {
      header: "Options",
      optionList: optionsDefinitions,
    },
  ]
  const options = commandLineArgs(optionsDefinitions, {
    argv: argv,
  })
  if (!("help" in options) && !("data" in options)) {
    console.error("--data is mandatory")
    options.help = true
  }
  if ("help" in options) {
    const usage = commandLineUsage(sections)
    console.warn(usage)
    return null
  }
  return options
}

function eqSet(as: Set<string>, bs: Set<string>): boolean {
  if (as.size !== bs.size) return false
  for (var a of as) if (!bs.has(a)) return false
  return true
}

class Code2Pattern {
  _pattern_: any
  _match_count_: number
  constructor(pattern: any, children: any) {
    this._pattern_ = pattern
    this._match_count_ = 0
    Object.assign(this, children)
  }
}

class Code2PatternComFond extends Code2Pattern {
  constructor() {
    super("^/SAISIE/(/NOMIN/)*(/REUNION/)*(/RAPPORT/)*$", {})
  }
}

class LectureMapping {
  libelle: any = {}
  codes: any = new Set()

  static codes_lecture: string[] = [
    "AN1",
    "SN1",
    "ANNLEC",
    "SNNLEC",
    "ANLDEF",
    "AN2",
    "SN2",
    "SN3",
  ]
  static codes_lecture_regexp: any = new RegExp(
    "^(?:" + LectureMapping.codes_lecture.join("|") + ")(-.*)?$",
  )

  static isLecture(acte: any) {
    return acte.codeActe.match(LectureMapping.codes_lecture_regexp)
  }

  static getType(acte: any) {
    if (acte.codeActe.match(/^AN/)) return "AN"
    else if (acte.codeActe.match(/^SN/)) return "SN"
    else throw Error(`${acte.codeActe} does not start with AN or SN`)
  }

  static map(codeActe: any) {
    return codeActe.replace(LectureMapping.codes_lecture_regexp, `lecture$1`)
  }

  static getCodeActe(acte: any) {
    if (!LectureMapping.isLecture(acte)) return

    return LectureMapping.map(acte.codeActe)
  }

  add(acte: any) {
    this.codes.add(acte.codeActe)

    const type = LectureMapping.getType(acte)

    if (!(acte.libelleActe.nomCanonique in this.libelle))
      this.libelle[acte.libelleActe.nomCanonique] = new Set()
    this.libelle[acte.libelleActe.nomCanonique].add(type)
  }

  remap(acte: any, codeActe: string) {
    acte.codeActe = codeActe

    let types = this.libelle[acte.libelleActe.nomCanonique]
    if (!(types.has("SN") && types.has("AN"))) {
      if (types.has("SN")) acte.libelleActe.nomCanonique += " (Sénat)"
      else if (types.has("AN"))
        acte.libelleActe.nomCanonique += " (Assemblée Nationale)"
    }
  }

  mergeCodes(codes: any) {
    for (const c of Array.from(this.codes)) codes.add(c)
  }

  printAnchors() {
    for (const code of Array.from(this.codes))
      console.log(`.. _${code}:`)
  }

  getCodes() {
    return Array.from(this.codes).join(",")
  }
}

class LectureMapper {
  mapping: any = {}

  has(codeActe: string) {
    return codeActe in this.mapping
  }

  collect(acte: any) {
    const codeActe = LectureMapping.getCodeActe(acte)
    if (codeActe != undefined) {
      let lectures = this.mapping
      if (!(codeActe in lectures)) lectures[codeActe] = new LectureMapping()
      lectures[codeActe].add(acte)
    }
  }

  remap(acte: any) {
    const codeActe = LectureMapping.getCodeActe(acte)
    if (codeActe != undefined) this.mapping[codeActe].remap(acte, codeActe)
  }

  mergeCodes(codes: any, codeActe: string) {
    this.mapping[codeActe].mergeCodes(codes)
  }

  printAnchors(codeActe: string) {
    this.mapping[codeActe].printAnchors()
  }

  getCodes(codeActe: string) {
    return this.mapping[codeActe].getCodes()
  }
}

class ActesDocumentation {
  verbose: boolean
  sanityChecks: boolean
  lectureMapper: any = new LectureMapper()
  remappedProcedures: any = {}
  anchorsPrinted: any = {}
  anchors: any = new Set()
  codes: any = {}
  procedures: any = null
  code2pattern: any = new Code2Pattern(null, {
    "Procédure législative": new Code2Pattern(
      "^(((/lecture/)+)|((/lecture/)+/CMP/(/lecture/)*))(/CC/)?(/PROM/)?(/AN-APPLI/)?$",
      {
        lecture: new Code2Pattern(
          "^(/DEPOT/)?(/DPTLETTRECT/)?(/ETI/)?(/AVCE/)?(/DGVT/)?(/ACIN/)?(/MOTION/)?(/PROCACC/)?(/COM/)?((/DEBATS/)+)?(/RTRINI/)?$",
          {
            "lecture-COM": new Code2Pattern("^(/FOND/|/AVIS/)+$", {
              "lecture-COM-FOND": new Code2PatternComFond(),
              "lecture-COM-AVIS": new Code2PatternComFond(),
            }),
            "lecture-DEBATS": new Code2Pattern(
              "^(/SEANCE/)*((/MOTION/)?/MOTION-VOTE/)?(/DEC/)?$",
              {},
            ),
          },
        ),
        CMP: new Code2Pattern(
          "^(/DEPOT/|/SAISIE/){1,2}(/COM/)?(/DEC/)?(/DEBATS-AN//DEBATS-SN/)?",
          {
            "CMP-COM": new Code2Pattern(
              "^(/NOMIN/)?(/RAPPORT-AN/|/RAPPORT-SN/){2}$",
              {},
            ),
            "CMP-DEBATS-AN": new Code2Pattern("^(/SEANCE/)*(/DEC/)?$", {}),
            "CMP-DEBATS-SN": new Code2Pattern("^(/SEANCE/)*(/DEC/)?$", {}),
          },
        ),
        CC: new Code2Pattern("^(/SAISIE/)+(/CONCLUSION/)?$", {}),
        PROM: new Code2Pattern("^(/PUB/)?$", {}),
        "AN-APPLI": new Code2Pattern("^(/RAPPORT/)?$", {}),
      },
    ),
    "Allocution du Président de l'Assemblée nationale": new Code2Pattern(
      "^/AN21/$",
      {
        AN21: new Code2Pattern("^(/APAN/)?$", {}),
      },
    ),
    "Résolution Article 34-1": new Code2Pattern("^(/ANLUNI/)?$", {
      ANLUNI: new Code2Pattern("^(/DEPOT/)?(/DEBATS/)?(/RTRINI/)?$", {
        "ANLUNI-DEBATS": new Code2Pattern("^(/SEANCE/)+(/DEC/)?$", {}),
      }),
    }),
    Résolution: new Code2Pattern("^(/ANLUNI/)?(/CC/)?$", {
      ANLUNI: new Code2Pattern("^(/DEPOT/)?(/COM/)?(/DEBATS/|/RTRINI/)?$", {
        "ANLUNI-COM": new Code2Pattern("^(/CAE/)?(/FOND/)?$", {
          "ANLUNI-COM-FOND": new Code2PatternComFond(),
          "ANLUNI-COM-CAE": new Code2Pattern(
            "^(/SAISIE/)?(/NOMIN/)?(/REUNION/)?(/RAPPORT/)?(/DEC/)?$",
            {},
          ),
        }),
        "ANLUNI-DEBATS": new Code2Pattern("^(/SEANCE/)*(/DEC/)?$", {}),
      }),
      CC: new Code2Pattern("^(/SAISIE/)?(/CONCLUSION/)?$", {}),
    }),
    "Rapport d'information sans mission": new Code2Pattern("^(/AN20/)?(/EU/)?$", {
      AN20: new Code2Pattern("^(/RAPPORT/)?$", {}),
      EU: new Code2Pattern("^(/DEC/)+$", {}),
    }),
    "Mission d'information": new Code2Pattern("^(/AN20/)?$", {
      AN20: new Code2Pattern("^(/MISINF/)?$", {
        "AN20-MISINF": new Code2Pattern("^(/CREA/)?(/NOMIN/)?(/RAPPORT/)?$", {}),
      }),
    }),
    "Commission d'enquête": new Code2Pattern("^(/AN20/)?$", {
      AN20: new Code2Pattern("^(/COMENQ/)?$", {
        "AN20-COMENQ": new Code2Pattern("^(/CREA/)?(/NOMIN/)?(/RAPPORT/)?$", {}),
      }),
    }),
    "Engagement de la responsabilité gouvernementale": new Code2Pattern(
      "^/AN21/$",
      {
        AN21: new Code2Pattern("^((/MOTION//DEBATS/)|(/DGVT/)){1,2}$", {
          "AN21-DEBATS": new Code2Pattern("^(/SEANCE/)?(/MOTION-VOTE/)?$", {}),
        }),
      },
    ),
    "Motion référendaire": new Code2Pattern("^(/lecture/)+$", {
      lecture: new Code2Pattern("^(/DEPOT/)?(/COM/)?(/DEBATS/)?$", {
        "lecture-COM": new Code2Pattern("^(/FOND/)?$", {
          "lecture-COM-FOND": new Code2Pattern(
            "^(/SAISIE/)?(/NOMIN/)*(/RAPPORT/)*$",
            {},
          ),
        }),
        "lecture-DEBATS": new Code2Pattern("^(/DEC/)?$", {}),
      }),
    }),
    Immunité: new Code2Pattern("^(/ANLUNI/)+$", {
      ANLUNI: new Code2Pattern("^(/DEPOT/)?(/COM/)?(/DEBATS/)?(/RTRINI/)?$", {
        "ANLUNI-COM": new Code2Pattern("^(/FOND/)?$", {
          "ANLUNI-COM-FOND": new Code2Pattern(
            "^(/SAISIE/)?(/NOMIN/)*(/RAPPORT/)*$",
            {},
          ),
        }),
        "ANLUNI-DEBATS": new Code2Pattern("^(/SEANCE/)?(/DEC/)$", {}),
      }),
    }),
    "Responsabilité pénale du président de la république": new Code2Pattern(
      "^/lecture/$",
      {
        lecture: new Code2Pattern("^(/DEPOT/)?(/RECBUREAU/)?$", {}),
      },
    ),
    "Proposition de loi présentée en application de l'article 11 de la Constitution": new Code2Pattern(
      "^/lecture/$",
      {
        lecture: new Code2Pattern("^(/DEPOT/)?$", {}),
      },
    ),
  })

  constructor(sanityChecks: boolean, verbose: boolean = false) {
    this.verbose = verbose
    this.sanityChecks = sanityChecks
    this.procedures = {}
    this.code2pattern._match_count_++
  }

  walkFiles(data: string[], action: any) {
    for (const file of getFiles(data)) {
      const dossier = load(file)
      let procedure: string = this.remapProcedure(
        dossier.procedureParlementaire.libelle,
      )
      if (!(procedure in this.procedures)) this.procedures[procedure] = {}
      action(
        this.procedures[procedure],
        dossier.actesLegislatifs,
        this.code2pattern[procedure],
        null,
      )
    }
  }

  collectFiles(data: string[]) {
    this.walkFiles(
      data,
      (_actes: any, actesLegislatifs: any, _c2p: any, _parent: string) => {
        this.collect(actesLegislatifs)
      },
    )
  }

  processFiles(data: string[]) {
    this.walkFiles(
      data,
      (actes: any, actesLegislatifs: any, c2p: any, parent: string) => {
        this.merge(actes, actesLegislatifs, c2p, parent)
      },
    )
    if (this.sanityChecks) this.validate()
  }

  remapActes(actes: any): any {
    let results = []
    for (const acte of actes) {
      this.lectureMapper.remap(acte)

      // CC-SAISIE- suffix does not imply a hierarchy
      acte.codeActe = acte.codeActe.replace(/^CC-SAISIE.*/, "CC-SAISIE")

      results.push(acte)
    }
    return results
  }

  collectActes(actes: any): any {
    let results = []
    for (const acte of actes) {
      this.lectureMapper.collect(acte)

      results.push(acte)
    }
    return results
  }

  remapProcedure(procedure: string): string {
    const merge_procedures: Map<string, any> = new Map([
      ["Proposition de loi ordinaire", "Procédure législative"],
      ["Projet de loi ordinaire", "Procédure législative"],
      [
        "Projet de ratification des traités et conventions",
        "Procédure législative",
      ],
      ["Projet ou proposition de loi organique", "Procédure législative"],
      [
        "Projet ou proposition de loi constitutionnelle",
        "Procédure législative",
      ],
      ["Projet de loi de finances de l'année", "Procédure législative"],
      ["Projet de loi de finances rectificative", "Procédure législative"],
      [
        "Projet de loi de financement de la sécurité sociale",
        "Procédure législative",
      ],
    ])
    if (merge_procedures.has(procedure)) {
      this.remappedProcedures[procedure] = null
      return merge_procedures.get(procedure)
    } else {
      return procedure
    }
  }

  // return the codeActes in the same order they appear in the regexp
  orderedCodes(codes: any, c2p: any, parent: string): Set<string> {
    codes = new Set(codes)
    if (!this.sanityChecks) return codes
    let ordered = c2p["_pattern_"].match(new RegExp("/.*?/", "g"))
    ordered = ordered.map((x: string) => x.replace(new RegExp("/(.*)/"), "$1"))
    if (parent != null) ordered = ordered.map((x: string) => `${parent}-${x}`)
    ordered = new Set(ordered)
    assert(eqSet(ordered, codes), `${[...ordered]} != ${[...codes]}`)
    return ordered
  }

  validateChronology(actesLegislatifs: any, c2p: any, parent: string): void {
    const pattern = c2p["_pattern_"]
    const sequence = actesLegislatifs
      .map((x: any) => `/${x.codeActe}/`.replace(`/${parent}-`, "/"))
      .join("")
    assert(
      sequence.match(new RegExp(pattern)),
      `${pattern} does not match ${sequence}`,
    )
    c2p._match_count_++
  }

  mergeActe(actes: any, acte: any, c2p: any, parent: string): void {
    let code: string = acte.codeActe
    let shortCode: string = code.replace(new RegExp(`^${parent}-`), "")
    shortCode = shortCode.replace(/-(AN|SN)$/, "")
    this.codes[shortCode] = null
    let name: string = acte.libelleActe.nomCanonique
    if (!(code in actes)) {
      actes[code] = {
        type: acte.xsiType,
        name: new Set([name]),
        shortCode: shortCode,
        actes: {},
      }
    } else {
      actes[code].name.add(name)
    }
    if ("actesLegislatifs" in acte)
      this.merge(actes[code].actes, acte.actesLegislatifs, c2p[code], code)
  }

  merge(actes: any, actesLegislatifs: any, c2p: any, parent: any): void {
    const l = this.remapActes(actesLegislatifs)
    this.validateChronology(l, c2p, parent)
    for (const acte of l) this.mergeActe(actes, acte, c2p, parent)
  }

  collect(actesLegislatifs: any): void {
    for (const acte of this.collectActes(actesLegislatifs))
      if ("actesLegislatifs" in acte) this.collect(acte.actesLegislatifs)
  }

  validateLevel(c2p: any, path: any) {
    assert(c2p._match_count_ > 0, `${path} is never used`)
    for (const [key, value] of Object.entries(c2p) as [string, any][]) {
      if (value != null && value.constructor.name == "Code2Pattern")
        this.validateLevel(value, path.concat([key]))
    }
  }

  validate() {
    this.validateLevel(this.code2pattern, [])
  }

  printEntry(sections: string, entry: string, codes: string) {
    const title = `\`${entry}\`__`
    console.log(title)
    const underline = new Array(title.length + 1).join(sections[0])
    console.log(underline)
    console.log(codes)
  }

  addAnchors(key: string) {
    if (key in this.anchorsPrinted) return
    if (this.lectureMapper.has(key))
      this.lectureMapper.mergeCodes(this.anchors, key)
    else this.anchors.add(key)
  }

  printAnchors(key: string) {
    if (key in this.anchorsPrinted) return
    this.anchorsPrinted[key] = null
    if (this.lectureMapper.has(key))
      this.lectureMapper.printAnchors(key)
    else console.log(`.. _${key}:`)
  }

  getCodes(key: string, shortCode: string) {
    if (key in this.anchorsPrinted) return `cf \\*${shortCode}\\*`
    if (this.lectureMapper.has(key)) return this.lectureMapper.getCodes(key)
    else return key
  }

  renameCMP(key: string, name: string) {
    if (key.startsWith("CMP")) {
      if (key.endsWith("-SN")) return `${name} (Sénat)`
      else if (key.endsWith("-AN")) return `${name} (Assemblée Nationale)`
      else return name
    } else return name
  }

  printLevel(actes: any, sections: string, c2p: any, parent: any) {
    const keys: Set<string> = this.orderedCodes(Object.keys(actes), c2p, parent)
    for (const key of keys) {
      const value = actes[key]
      let name = Array.from(value.name).join(", ")
      name = this.renameCMP(key, name)
      const codes = this.getCodes(key, value.shortCode)
      this.addAnchors(key)
      this.printAnchors(key)
      console.log("")
      this.printEntry(sections, `${name} <${value.shortCode}TOC_>`, codes)
      console.log("")
      if (Object.keys(value.actes).length > 0)
        this.printLevel(value.actes, sections.substring(1), c2p[key], key)
    }
  }

  printCodes() {
    for (const key of Object.keys(this.codes))
      console.log(`.. _${key}TOC: http://`)
  }

  printAnchorsIndex() {
    for (const code of Array.from(this.anchors))
      console.log(`* :ref:\`${code} <${code}>\``)
  }

  getAggregated(procedure: string) {
    if (procedure == "Procédure législative") {
      const remapped = Object.keys(this.remappedProcedures).join(", ")
      return ` (agrégation de ${remapped})`
    } else return ""
  }

  print(): void {
    this.anchorsPrinted = {}
    for (const [key, actes] of Object.entries(this.procedures) as [
      string,
      any,
    ][]) {
      const code = key.replace(/ +/g, "_")
      this.codes[code] = null
      const aggregated = this.getAggregated(key)
      const title = `\`${key} <${code}TOC_>\`__${aggregated}`
      console.log(title)
      const underline = new Array(title.length + 1).join('=')
      console.log(underline)
      this.printLevel(actes, "-~.'", this.code2pattern[key], null)
    }
    this.printCodes()
    this.printAnchorsIndex()
  }
}

function main(argv: any): number {
  const options = parseArgs(argv)
  if (options === null) return 1
  let doc = new ActesDocumentation(
    !options["no-sanity-checks"],
    options.verbose,
  )
  doc.collectFiles(options.data)
  doc.processFiles(options.data)
  doc.print()
  return 0
}

/* istanbul ignore if */
if (process.argv[1].endsWith("document_dossiers_legislatifs.ts"))
  process.exit(main(process.argv))
