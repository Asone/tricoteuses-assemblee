import assert from "assert"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import fetch from "node-fetch"
import path from "path"
import stream from "stream"
import StreamZip from "node-stream-zip"
import util from "util"

import { datasets, EnabledDatasets } from "../datasets"
import { walkDir } from "../file_systems"

const optionsDefinitions = [
  {
    alias: "f",
    help: "fetch datasets instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "k",
    defaultValue: ["All"],
    help: "categories of datasets to reorganize",
    multiple: true,
    name: "categories",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

const pipeline = util.promisify(stream.pipeline)

async function retrieveOpenData(dataDir: string): Promise<void> {
  options.categories.map((datasetName: string) =>
    assert.notStrictEqual(
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
      undefined,
      `Ùnknown name of dataset: ${datasetName}`,
    ),
  )
  const enabledDatasets = options.categories.reduce(
    (enabledDatasets: EnabledDatasets, datasetName: string): EnabledDatasets =>
      enabledDatasets |
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
    EnabledDatasets.None,
  )

  const choosenDatasets = [
    ...(enabledDatasets & EnabledDatasets.ActeursEtOrganes
      ? datasets.acteursEtOrganes
      : []),
    ...(enabledDatasets & EnabledDatasets.Agendas ? datasets.agendas : []),
    ...(enabledDatasets & EnabledDatasets.Amendements ? datasets.amendements : []),
    ...(enabledDatasets & EnabledDatasets.DossiersLegislatifs
      ? datasets.dossiersLegislatifs
      : []),
    ...(enabledDatasets & EnabledDatasets.Scrutins ? datasets.scrutins : []),
  ]
  for (const dataset of choosenDatasets) {
    const zipFilePath = path.join(dataDir, `${dataset.filename}.zip`)
    if (options.fetch) {
      // Fetch & save ZIP file.
      if (!options.silent) {
        console.log(`Loading ${dataset.title}: ${dataset.filename}.zip`)
      }
      const response = await fetch(dataset.url)
      if (!response.ok) {
        console.error(response.status, response.statusText)
        console.error(await response.text())
        throw new Error(`Fetch failed: ${dataset.url}`)
      }
      await pipeline(response.body, fs.createWriteStream(zipFilePath))
    }
    if (!options.silent) {
      console.log(`Unzipping ${dataset.title}: ${dataset.filename}.zip`)
    }
    const dataDirOrFilePath = path.join(dataDir, dataset.filename)
    fs.removeSync(dataDirOrFilePath)
    const zip = new StreamZip({
      file: zipFilePath,
      storeEntries: true,
    })
    await new Promise((resolve, reject) => {
      zip.on("ready", () => {
        zip.extract(null, dataDir, (err: any, _count: number) => {
          zip.close()
          if (err) {
            reject(err)
          } else {
            resolve()
          }
        })
      })
    })

    if (dataset.repairZip !== undefined) {
      if (!options.silent) {
        console.log(`Repairing ${dataset.title}: ${dataset.filename}`)
      }
      dataset.repairZip(dataset, dataDir)
    }
    // Reindent JSON file.
    if (!options.silent) {
      console.log(`Reidenting ${dataset.title}: ${dataset.filename}`)
    }
    if (fs.statSync(dataDirOrFilePath).isDirectory()) {
      for (const dataFileSplitPath of walkDir(dataDirOrFilePath)) {
        const dataFilePath = path.join(dataDirOrFilePath, ...dataFileSplitPath)
        if (dataFilePath.endsWith(".json")) {
          const data = JSON.parse(fs.readFileSync(dataFilePath, { encoding: "utf-8" }))
          fs.writeFileSync(dataFilePath, JSON.stringify(data, null, 2))
        }
      }
    } else {
      const data = JSON.parse(fs.readFileSync(dataDirOrFilePath, { encoding: "utf-8" }))
      fs.writeFileSync(dataDirOrFilePath, JSON.stringify(data, null, 2))
    }
  }
}

retrieveOpenData(options.dataDir).catch(error => {
  console.log(error)
  process.exit(1)
})
