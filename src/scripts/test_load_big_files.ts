/** Load all big non-split open data files in RAM as a test. */

import assert from "assert"
import commandLineArgs from "command-line-args"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeDataFromBigFiles } from "../loaders"
import { Legislature } from "../types/legislatures"

const optionsDefinitions = [
  {
    alias: "k",
    defaultValue: ["All"],
    help: "categories of datasets to reorganize",
    multiple: true,
    name: "categories",
    type: String,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

options.categories.map((datasetName: string) =>
  assert.notStrictEqual(
    ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
    undefined,
    `Ùnknown name of dataset: ${datasetName}`,
  ),
)
const enabledDatasets = options.categories.reduce(
  (enabledDatasets: EnabledDatasets, datasetName: string): EnabledDatasets =>
    enabledDatasets |
    ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
  EnabledDatasets.None,
)

loadAssembleeDataFromBigFiles(options.dataDir, enabledDatasets, Legislature.All, {
  log: true,
})
