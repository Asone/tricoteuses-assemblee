import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import fetch from "node-fetch"
import path from "path"
import stream from "stream"
import util from "util"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeData } from "../loaders"
import { CodeTypeOrgane, Mandat, Photo, TypeMandat } from "../types/acteurs_et_organes"

const optionsDefinitions = [
  {
    alias: "C",
    help: "clone repositories from given group (or organization) git URL",
    name: "clone",
    type: String,
  },
  {
    alias: "c",
    help: "commit photos",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "f",
    help: "fetch députés' pictures instead of retrieving them from files",
    name: "fetch",
    type: Boolean,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "r",
    help: "push commit to given remote",
    multiple: true,
    name: "remote",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)
const pipeline = util.promisify(stream.pipeline)

function commitAndPush(
  repositoryDir: string,
  commit: boolean,
  remotes?: string[],
): boolean {
  let error = false
  if (commit) {
    execSync("git add .", {
      cwd: repositoryDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    try {
      execSync('git commit -m "Nouvelle moisson"', {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
      })
    } catch (childProcess) {
      if (
        childProcess.stderr === null ||
        !/nothing to commit/.test(childProcess.stdout)
      ) {
        console.error(childProcess.output)
        throw childProcess
      }
    }
    for (const remote of remotes || []) {
      try {
        execSync(`git push ${remote} master`, {
          cwd: repositoryDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      } catch (childProcess) {
        // Don't stop when push fails.
        console.error(childProcess.output)
        error = true
      }
    }
  }
  return error
}

async function retrievePhotosDeputes(): Promise<boolean> {
  const dataDir = options.dataDir
  const { acteurByUid } = loadAssembleeData(
    dataDir,
    EnabledDatasets.ActeursEtOrganes,
    options.legislature,
  )
  const deputesLegislature = Object.values(acteurByUid)
    .filter(acteur => {
      const { mandats } = acteur
      if (!mandats) {
        return false
      }
      const mandatDeputeLegislature = mandats.filter(
        (mandat: Mandat) =>
          mandat.xsiType === TypeMandat.MandatParlementaireType &&
          mandat.legislature === options.legislature &&
          mandat.typeOrgane === CodeTypeOrgane.Assemblee,
      )[0]
      return mandatDeputeLegislature !== undefined
    })
    .sort((a, b) =>
      a.uid.length === b.uid.length
        ? a.uid.localeCompare(b.uid)
        : a.uid.length - b.uid.length,
    )

  const gitGroupUrl = options.clone ? options.clone.trim().replace(/\/+$/, "") : undefined
  const photosDirName = `photos_deputes_${options.legislature}`
  if (gitGroupUrl !== undefined) {
    execSync(`git clone ${gitGroupUrl}/${photosDirName}.git`, {
      cwd: dataDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
  }
  const photosDir = path.join(dataDir, photosDirName)

  // Download photos.
  fs.ensureDirSync(photosDir)
  if (options.fetch) {
    const missingPhotoFilePath = "images/transparent_150x192.jpg"
    for (const depute of deputesLegislature) {
      const ident = depute.etatCivil.ident
      const numeroDepute = depute.uid.substring(2)
      const photoFilename = `${numeroDepute}.jpg`
      const photoFilePath = path.join(photosDir, photoFilename)
      const photoTempFilename = `${numeroDepute}_temp.jpg`
      const photoTempFilePath = path.join(photosDir, photoTempFilename)
      const urlPhoto = `http://www2.assemblee-nationale.fr/static/tribun/${options.legislature}/photos/${photoFilename}`
      if (!options.silent) {
        console.log(`Loading photo ${urlPhoto} for ${ident.prenom} ${ident.nom}…`)
      }
      for (let retries = 0; retries < 3; retries++) {
        const response = await fetch(urlPhoto)
        if (response.ok) {
          await pipeline(response.body, fs.createWriteStream(photoTempFilePath))
          fs.renameSync(photoTempFilePath, photoFilePath)
          break
        }
        if (retries >= 2) {
          console.warn(`Fetch failed: ${urlPhoto} (${ident.prenom} ${ident.nom})`)
          console.warn(response.status, response.statusText)
          console.warn(await response.text())
          if (fs.existsSync(photoFilePath)) {
            console.warn("  => Reusing existing image")
          } else {
            console.warn("  => Using blank image")
            fs.copyFileSync(missingPhotoFilePath, photoFilePath)
          }
          break
        }
      }
    }
  }

  // Resize photos to 150x192, because some haven't exactly this size.
  for (const depute of deputesLegislature) {
    const ident = depute.etatCivil.ident
    const numeroDepute = depute.uid.substring(2)
    if (!options.silent) {
      console.log(`Resizing photo ${numeroDepute} for ${ident.prenom} ${ident.nom}…`)
    }
    execSync(
      `gm convert -resize 150x192! ${numeroDepute}.jpg ${numeroDepute}_150x192.jpg`,
      {
        cwd: photosDir,
      },
    )
  }

  // Create a mosaic of photos.
  const photoByIdDepute: { [uid: string]: Photo } = {}
  const rowsFilenames: string[] = []
  for (
    let deputeIndex = 0, rowIndex = 0;
    deputeIndex < deputesLegislature.length;
    deputeIndex += 25, rowIndex++
  ) {
    const row = deputesLegislature.slice(deputeIndex, deputeIndex + 25)
    const photosFilenames: string[] = []
    for (const [columnIndex, depute] of row.entries()) {
      const numeroDepute = depute.uid.substring(2)
      const photoFilename = `${numeroDepute}_150x192.jpg`
      photosFilenames.push(photoFilename)
      photoByIdDepute[depute.uid] = {
        chemin: `photos_deputes_${options.legislature}/${photoFilename}`,
        cheminMosaique: `photos_deputes_${options.legislature}/deputes.jpg`,
        hauteur: 192,
        largeur: 150,
        xMosaique: columnIndex * 150,
        yMosaique: rowIndex * 192,
      }
    }
    const rowFilename = `row-${rowIndex}.jpg`
    execSync(`gm convert ${photosFilenames.join(" ")} +append ${rowFilename}`, {
      cwd: photosDir,
    })
    rowsFilenames.push(rowFilename)
  }
  execSync(`gm convert ${rowsFilenames.join(" ")} -append deputes.jpg`, {
    cwd: photosDir,
  })
  for (const rowFilename of rowsFilenames) {
    fs.unlinkSync(path.join(photosDir, rowFilename))
  }

  const jsonFilePath = path.join(photosDir, "deputes.json")
  fs.writeFileSync(jsonFilePath, JSON.stringify(photoByIdDepute, null, 2))

  return commitAndPush(photosDir, options.commit, options.remote)
}

retrievePhotosDeputes()
  .then(error => process.exit(error ? 1 : 0))
  .catch(error => {
    console.log(error)
    process.exit(2)
  })
