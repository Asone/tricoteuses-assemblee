import assert from "assert"
// import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

// import { EnabledDatasets } from "../datasets"
// import { loadAssembleeData } from "../loaders"
import { parseTexteLoi } from "../parsers/documents"
// import { Document, TypeDocument } from "../types/dossiers_legislatifs"
// import { DocumentUrlFormat, urlFromDocument } from "../urls"

const optionsDefinitions = [
  {
    alias: "c",
    help: "commit split files",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "f",
    help: "Filename of Assemblée's document to parse",
    name: "filename",
    type: String,
  },
  {
    alias: "l",
    defaultValue: "15",
    name: "legislature",
    type: String,
  },
  {
    alias: "r",
    help: "push commit to given remote",
    multiple: true,
    name: "remote",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

// function commitAndPush(repositoryDir: string, commit: boolean, remotes?: string[]): boolean {
//   let error = false
//   if (commit) {
//     execSync(`git add .`, {
//       cwd: repositoryDir,
//       env: process.env,
//       encoding: "utf-8",
//       stdio: ["ignore", "ignore", "pipe"],
//     })
//     try {
//       execSync('git commit -m "Nouvelle moisson"', {
//         cwd: repositoryDir,
//         env: process.env,
//         encoding: "utf-8",
//       })
//     } catch (childProcess) {
//       if (
//         childProcess.stderr === null ||
//         !/nothing to commit/.test(childProcess.stdout)
//       ) {
//         console.error(childProcess.output)
//         throw childProcess
//       }
//     }
//     for (const remote of remotes || []) {
//       try {
//         execSync(`git push ${remote} master`, {
//           cwd: repositoryDir,
//           env: process.env,
//           encoding: "utf-8",
//           stdio: ["ignore", "ignore", "pipe"],
//         })
//       } catch (childProcess) {
//         // Don't stop when push fails.
//         console.error(childProcess.output)
//         error = true
//       }
//     }
//   }
//   return error
// }

function parseTextesLois(): void {
  assert(
    !options.commit || options.filename,
    'Options "commit" & "filename" are incompatible',
  )

  const dataDir = options.dataDir
  // const { documentByUid } = loadAssembleeData(
  //   dataDir,
  //   EnabledDatasets.DossiersLegislatifs,
  //   options.legislature,
  // )

  const textesLoisDir = path.join(
    dataDir,
    "www.assemblee-nationale.fr",
    options.legislature,
    "textes",
  )
  for (const filename of fs.readdirSync(textesLoisDir)) {
    if (filename[0] === ".") {
      continue
    }
    if (!filename.endsWith(".asp")) {
      continue
    }
    if (options.filename && !filename.startsWith(options.filename)) {
      continue
    }
    const texteLoiPath = path.join(textesLoisDir, filename)
    if (!options.silent) {
      console.log(`Parsing "texte de loi" ${texteLoiPath}…`)
    }
    const texteLoiPage = fs.readFileSync(texteLoiPath, { encoding: "utf-8" })
    const texteLoiUrl = `http://www.assemblee-nationale.fr/${options.legislature}/textes/${filename}`
    const texteLoiParsed = parseTexteLoi(texteLoiUrl, texteLoiPage)
    if (options.filename && options.verbose) {
      console.log(JSON.stringify(texteLoiParsed.subdivisions, null, 2))
    }
    const { error } = texteLoiParsed
    if (error) {
      console.error(
        `  Error while parsing page "${texteLoiPath}":\n\nError:\n${JSON.stringify(
          error,
          null,
          2,
        )}`,
      )
      continue
    }
  }

  // fs.ensureDirSync(textesLoisDir)
  // if (!options.uid) {
  //   for (const filename of fs.readdirSync(textesLoisDir)) {
  //     if (filename[0] === ".") {
  //       continue
  //     }
  //     fs.removeSync(path.join(textesLoisDir, filename))
  //   }
  // }

  // const textesLois = Object.values(documentByUid)
  //   .filter(document => document.xsiType === TypeDocument.TexteLoiType)
  //   .filter(
  //     document => document.uid.startsWith("PIONAN") || document.uid.startsWith("PRJLAN"),
  //   )
  //   .map(document => {
  //     return {
  //       ...document,
  //       rawDocumentAssembleeUrl: urlFromDocument(document, DocumentUrlFormat.RawHtml),
  //     }
  //   })
  //   .sort((a, b) =>
  //     a.rawDocumentAssembleeUrl === null
  //       ? -1
  //       : b.rawDocumentAssembleeUrl === null
  //       ? 1
  //       : a.rawDocumentAssembleeUrl.localeCompare(b.rawDocumentAssembleeUrl),
  //   )

  // const firstUid = options.uid
  // let skip = !!firstUid
  // for (const texteLoi of textesLois) {
  //   if (skip) {
  //     if (texteLoi.uid === firstUid) {
  //       skip = false
  //     } else {
  //       continue
  //     }
  //   }
  //   if (!options.silent) {
  //     console.log(
  //       `Retrieving bill ${texteLoi.uid} at ${texteLoi.rawDocumentAssembleeUrl}`,
  //     )
  //   }
  //   if (texteLoi.rawDocumentAssembleeUrl === null) {
  //     continue
  //   }
  //   const url = new URL(texteLoi.rawDocumentAssembleeUrl)
  //   const filePath = path.join(dataDir, url.hostname, ...url.pathname.split("/"))
  //   assert.strictEqual(textesLoisDir, path.dirname(filePath))

  //   let page = undefined
  //   try {
  //     page = fs.readFileSync(filePath, { encoding: "utf-8" })
  //   } catch (e) {
  //     if (e.code === "ENOENT") {
  //       continue
  //     }
  //     throw e
  //   }

  //   if (options.parse) {
  //     const texteLoiParsed = parseTexteLoi(texteLoi.rawDocumentAssembleeUrl, page)
  //     const { error } = texteLoiParsed
  //     if (error) {
  //       console.error(
  //         `Error while parsing page "${texteLoi.rawDocumentAssembleeUrl}" (uid: ${
  //           texteLoi.uid
  //         }):\n\nError:\n${JSON.stringify(error, null, 2)}`,
  //       )
  //       continue
  //     }
  //   }
  // }
  // if commitAndPush(textesLoisDir, options.commit, options.remote) {
  //   process.exit(1)
  // }
  // return textesLois
}

parseTextesLois()
