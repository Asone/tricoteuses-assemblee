import assert from "assert"
import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { datasets, DatasetStructure, EnabledDatasets } from "../datasets"
import { existingDateToJson, patchedDateToJson } from "../dates"
import { walkDir } from "../file_systems"
import { Convert as ActeursEtOrganesRawConvert } from "../raw_types/acteurs_et_organes"
import { Convert as AgendasRawConvert } from "../raw_types/agendas"
import { Convert as AmendementsRawConvert } from "../raw_types/amendements"
import { Convert as DossiersLegislatifsRawConvert } from "../raw_types/dossiers_legislatifs"
import { Convert as ScrutinsRawConvert } from "../raw_types/scrutins"

const optionsDefinitions = [
  {
    alias: "C",
    help: "clone repositories from given group (or organization) git URL",
    name: "clone",
    type: String,
  },
  {
    alias: "c",
    help: "commit split files",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "k",
    defaultValue: ["All"],
    help: "categories of datasets to reorganize",
    multiple: true,
    name: "categories",
    type: String,
  },
  {
    alias: "r",
    help: "push commit to given remote",
    multiple: true,
    name: "remote",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "V",
    help: "don't validate raw data",
    name: "no-validate-raw",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

const documentUidRegex = /^(ACIN|AVCE|AVIS|DECL|ETDI|LETT|PION|PNRE|PRJL|RAPP|RINF)(AN|SN)(R\d+)[LS](\d+)(B|BTC)(\d+)$/
const dossierUidRegex = /^DL(R\d+)(L\d+)N(\d+)$/
const reunionUidRegex = /^RU(AN|CG|SN)(R\d+)(L\d+)(S\d{4})(IDC|IDFL?|IDS)(\d+)$/
const scrutinUidRegex = /^VTAN(R\d+)(L\d+)V(\d+)$/

function commitAndPush(
  repositoryDir: string,
  commit: boolean,
  remotes?: string[],
): boolean {
  let error = false
  if (commit) {
    execSync(`git add .`, {
      cwd: repositoryDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    try {
      execSync('git commit -m "Nouvelle moisson"', {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
      })
    } catch (childProcess) {
      if (
        childProcess.stderr === null ||
        !/nothing to commit/.test(childProcess.stdout)
      ) {
        console.error(childProcess.output)
        throw childProcess
      }
    }
    for (const remote of remotes || []) {
      try {
        execSync(`git push ${remote} master`, {
          cwd: repositoryDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      } catch (childProcess) {
        // Don't stop when push fails.
        console.error(childProcess.output)
        error = true
      }
    }
  }
  return error
}

function reorganizeData(dataDir: string): boolean {
  Date.prototype.toJSON = patchedDateToJson

  let error = false
  const gitGroupUrl = options.clone ? options.clone.trim().replace(/\/+$/, "") : undefined
  const validateRawData = !options["no-validate-raw"]

  options.categories.map((datasetName: string) =>
    assert.notStrictEqual(
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
      undefined,
      `Ùnknown name of dataset: ${datasetName}`,
    ),
  )
  const enabledDatasets = options.categories.reduce(
    (enabledDatasets: EnabledDatasets, datasetName: string): EnabledDatasets =>
      enabledDatasets |
      ((EnabledDatasets as any) as { [name: string]: EnabledDatasets })[datasetName],
    EnabledDatasets.None,
  )

  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      const originalJsonDirectoryOrFilePath: string = path.join(dataDir, dataset.filename)
      if (!options.silent) {
        console.log(`Reorganizing ${originalJsonDirectoryOrFilePath}`)
      }

      const datasetName = path.basename(originalJsonDirectoryOrFilePath, ".json")
      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }

      const datasetReorganizedDir = path.join(dataDir, datasetName)
      fs.ensureDirSync(datasetReorganizedDir)
      for (const filename of fs.readdirSync(datasetReorganizedDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetReorganizedDir, filename))
      }
      const acteursReorganizedDir = path.join(datasetReorganizedDir, "acteurs")
      fs.mkdirSync(acteursReorganizedDir)
      const organesReorganizedDir = path.join(datasetReorganizedDir, "organes")
      fs.mkdirSync(organesReorganizedDir)

      switch (dataset.structure) {
        case DatasetStructure.SingleFile:
          {
            const originalJson: string = fs.readFileSync(
              originalJsonDirectoryOrFilePath,
              {
                encoding: "utf8",
              },
            )
            const originalData: any = validateRawData
              ? ActeursEtOrganesRawConvert.toActeursEtOrganes(originalJson)
              : JSON.parse(originalJson)

            for (const acteur of originalData.export.acteurs.acteur) {
              const reorganizedJsonFilePath: string = path.join(
                acteursReorganizedDir,
                acteur.uid["#text"] + ".json",
              )
              fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(acteur, null, 2), {
                encoding: "utf8",
              })
            }

            for (const organe of originalData.export.organes.organe) {
              const reorganizedJsonFilePath: string = path.join(
                organesReorganizedDir,
                organe.uid + ".json",
              )
              fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(organe, null, 2), {
                encoding: "utf8",
              })
            }
          }
          break
        case DatasetStructure.SegmentedFiles:
          {
            const mandatsByActeurUid: {
              [acteurUid: string]: { uid: string }[]
            } = {}
            if (fs.existsSync(path.join(originalJsonDirectoryOrFilePath, "mandat"))) {
              for (const mandatSplitPath of walkDir(originalJsonDirectoryOrFilePath, [
                "mandat",
              ])) {
                const mandatFilename = mandatSplitPath[mandatSplitPath.length - 1]
                if (!mandatFilename.endsWith(".json")) {
                  continue
                }
                const mandatOriginalFilePath = path.join(
                  originalJsonDirectoryOrFilePath,
                  ...mandatSplitPath,
                )
                if (options.verbose) {
                  console.log(`  Loading "mandat" file: ${mandatOriginalFilePath}…`)
                }
                const mandatOriginalJson: string = fs.readFileSync(
                  mandatOriginalFilePath,
                  {
                    encoding: "utf8",
                  },
                )
                const mandatOriginal: any = validateRawData
                  ? ActeursEtOrganesRawConvert.toMandat(mandatOriginalJson)
                  : JSON.parse(mandatOriginalJson)
                const mandat = mandatOriginal.mandat
                let mandats = mandatsByActeurUid[mandat.acteurRef]
                if (mandats === undefined) {
                  mandats = mandatsByActeurUid[mandat.acteurRef] = []
                }
                mandats.push(mandat)
              }

              for (const mandats of Object.values(mandatsByActeurUid)) {
                mandats.sort((a, b) =>
                  a.uid.length === b.uid.length
                    ? a.uid.localeCompare(b.uid)
                    : a.uid.length - b.uid.length,
                )
              }
            }

            for (const acteurSplitPath of walkDir(originalJsonDirectoryOrFilePath, [
              "acteur",
            ])) {
              const acteurFilename = acteurSplitPath[acteurSplitPath.length - 1]
              if (!acteurFilename.endsWith(".json")) {
                continue
              }
              const acteurOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...acteurSplitPath,
              )
              if (options.verbose) {
                console.log(`  Reorganizing "acteur" file: ${acteurOriginalFilePath}…`)
              }
              const acteurOriginalJson: string = fs.readFileSync(acteurOriginalFilePath, {
                encoding: "utf8",
              })
              const acteurOriginal: any = validateRawData
                ? ActeursEtOrganesRawConvert.toActeur(acteurOriginalJson)
                : JSON.parse(acteurOriginalJson)
              const acteur = acteurOriginal.acteur
              const acteurUid = acteur.uid["#text"]
              const mandats = mandatsByActeurUid[acteurUid]
              if (mandats !== undefined) {
                assert.strictEqual(acteur.mandats, undefined)
                acteur.mandats = { mandat: mandats }
              }
              const acteurReorganizedJsonFilePath: string = path.join(
                acteursReorganizedDir,
                acteurUid + ".json",
              )
              fs.writeFileSync(
                acteurReorganizedJsonFilePath,
                JSON.stringify(acteur, null, 2),
                {
                  encoding: "utf8",
                },
              )
            }

            for (const organeSplitPath of walkDir(originalJsonDirectoryOrFilePath, [
              "organe",
            ])) {
              const organeFilename = organeSplitPath[organeSplitPath.length - 1]
              if (!organeFilename.endsWith(".json")) {
                continue
              }
              const organeOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...organeSplitPath,
              )
              if (options.verbose) {
                console.log(`  Reorganizing "organe" file: ${organeOriginalFilePath}…`)
              }
              const organeOriginalJson: string = fs.readFileSync(organeOriginalFilePath, {
                encoding: "utf8",
              })
              const organeOriginal: any = validateRawData
                ? ActeursEtOrganesRawConvert.toOrgane(organeOriginalJson)
                : JSON.parse(organeOriginalJson)
              const organe = organeOriginal.organe
              const organeReorganizedJsonFilePath: string = path.join(
                organesReorganizedDir,
                organe.uid + ".json",
              )
              fs.writeFileSync(
                organeReorganizedJsonFilePath,
                JSON.stringify(organe, null, 2),
                {
                  encoding: "utf8",
                },
              )
            }
          }
          break
      }

      if (commitAndPush(datasetReorganizedDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      const originalJsonDirectoryOrFilePath: string = path.join(dataDir, dataset.filename)
      if (!options.silent) {
        console.log(`Reorganizing ${originalJsonDirectoryOrFilePath}`)
      }

      const datasetName = path.basename(originalJsonDirectoryOrFilePath, ".json")
      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }

      const datasetReorganizedDir = path.join(dataDir, datasetName)
      fs.ensureDirSync(datasetReorganizedDir)
      for (const filename of fs.readdirSync(datasetReorganizedDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetReorganizedDir, filename))
      }

      switch (dataset.structure) {
        case DatasetStructure.SingleFile:
          {
            const originalJson: string = fs.readFileSync(
              originalJsonDirectoryOrFilePath,
              {
                encoding: "utf8",
              },
            )
            const originalData: any = validateRawData
              ? AgendasRawConvert.toAgendas(originalJson)
              : JSON.parse(originalJson)

            for (const reunion of originalData.reunions.reunion) {
              writeReunionReorganizedJson(datasetReorganizedDir, reunion)
            }
          }
          break
        case DatasetStructure.SegmentedFiles:
          {
            for (const reunionSplitPath of walkDir(originalJsonDirectoryOrFilePath, [
              "reunion",
            ])) {
              const reunionFilename = reunionSplitPath[reunionSplitPath.length - 1]
              if (!reunionFilename.endsWith(".json")) {
                continue
              }
              const reunionOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...reunionSplitPath,
              )
              if (options.verbose) {
                console.log(`  Reorganizing "reunion" file: ${reunionOriginalFilePath}…`)
              }
              const reunionOriginalJson: string = fs.readFileSync(
                reunionOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const reunionOriginal: any = validateRawData
                ? AgendasRawConvert.toReunion(reunionOriginalJson)
                : JSON.parse(reunionOriginalJson)
              const reunion = reunionOriginal.reunion
              writeReunionReorganizedJson(datasetReorganizedDir, reunion)
            }
          }
          break
      }

      if (commitAndPush(datasetReorganizedDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      const originalJsonDirectoryOrFilePath: string = path.join(dataDir, dataset.filename)
      if (!options.silent) {
        console.log(`Reorganizing ${originalJsonDirectoryOrFilePath}`)
      }

      const datasetName = path.basename(originalJsonDirectoryOrFilePath, ".json")
      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }

      const datasetReorganizedDir = path.join(dataDir, datasetName)
      fs.ensureDirSync(datasetReorganizedDir)
      for (const filename of fs.readdirSync(datasetReorganizedDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetReorganizedDir, filename))
      }

      switch (dataset.structure) {
        case DatasetStructure.SingleFile:
          {
            const originalJson: string = fs.readFileSync(
              originalJsonDirectoryOrFilePath,
              {
                encoding: "utf8",
              },
            )
            const originalData: any = validateRawData
              ? AmendementsRawConvert.toAmendements(originalJson)
              : JSON.parse(originalJson)

            for (const texteLegislatif of originalData.textesEtAmendements.texteleg) {
              for (const amendement of texteLegislatif.amendements) {
                writeAmendementReorganizedJson(datasetReorganizedDir, amendement)
              }
            }
          }
          break
        case DatasetStructure.SegmentedFiles:
          {
            for (const amendementSplitPath of walkDir(originalJsonDirectoryOrFilePath)) {
              const amendementFilename =
                amendementSplitPath[amendementSplitPath.length - 1]
              if (!amendementFilename.endsWith(".json")) {
                continue
              }
              const amendementOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...amendementSplitPath,
              )
              if (options.verbose) {
                console.log(
                  `  Reorganizing "amendement" file: ${amendementOriginalFilePath}…`,
                )
              }
              const amendementOriginalJson: string = fs.readFileSync(
                amendementOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const amendementOriginal: any = validateRawData
                ? AmendementsRawConvert.toAmendementWrapper(amendementOriginalJson)
                : JSON.parse(amendementOriginalJson)
              const amendement = amendementOriginal.amendement
              writeAmendementReorganizedJson(datasetReorganizedDir, amendement)
            }
          }
          break
      }

      if (commitAndPush(datasetReorganizedDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      const originalJsonDirectoryOrFilePath: string = path.join(dataDir, dataset.filename)
      if (!options.silent) {
        console.log(`Reorganizing ${originalJsonDirectoryOrFilePath}`)
      }

      const datasetName = path.basename(originalJsonDirectoryOrFilePath, ".json")
      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }

      const datasetReorganizedDir = path.join(dataDir, datasetName)
      fs.ensureDirSync(datasetReorganizedDir)
      for (const filename of fs.readdirSync(datasetReorganizedDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetReorganizedDir, filename))
      }
      const documentsDir = path.join(datasetReorganizedDir, "documents")
      fs.mkdirSync(documentsDir)
      const dossiersDir = path.join(datasetReorganizedDir, "dossiers")
      fs.mkdirSync(dossiersDir)

      switch (dataset.structure) {
        case DatasetStructure.SingleFile:
          {
            const originalJson: string = fs.readFileSync(
              originalJsonDirectoryOrFilePath,
              {
                encoding: "utf8",
              },
            )
            const originalData: any = validateRawData
              ? DossiersLegislatifsRawConvert.toDossiersLegislatifs(originalJson)
              : JSON.parse(originalJson)

            for (const document of originalData.export.textesLegislatifs.document) {
              writeDocumentReorganizedJson(documentsDir, document)
            }

            for (const { dossierParlementaire } of originalData.export.dossiersLegislatifs
              .dossier) {
              writeDossierParlementaireReorganizedJson(dossiersDir, dossierParlementaire)
            }
          }
          break
        case DatasetStructure.SegmentedFiles:
          {
            for (const documentSplitPath of walkDir(originalJsonDirectoryOrFilePath, [
              "document",
            ])) {
              const documentFilename = documentSplitPath[documentSplitPath.length - 1]
              if (!documentFilename.endsWith(".json")) {
                continue
              }
              const documentOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...documentSplitPath,
              )
              if (options.verbose) {
                console.log(
                  `  Reorganizing "document" file: ${documentOriginalFilePath}…`,
                )
              }
              const documentOriginalJson: string = fs.readFileSync(
                documentOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const documentOriginal: any = validateRawData
                ? DossiersLegislatifsRawConvert.toDocument(documentOriginalJson)
                : JSON.parse(documentOriginalJson)
              const document = documentOriginal.document
              writeDocumentReorganizedJson(documentsDir, document)
            }

            for (const dossierParlementaireSplitPath of walkDir(
              originalJsonDirectoryOrFilePath,
              ["dossierParlementaire"],
            )) {
              const dossierParlementaireFilename =
                dossierParlementaireSplitPath[dossierParlementaireSplitPath.length - 1]
              if (!dossierParlementaireFilename.endsWith(".json")) {
                continue
              }
              const dossierParlementaireOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...dossierParlementaireSplitPath,
              )
              if (options.verbose) {
                console.log(
                  `  Reorganizing "dossierParlementaire" file: ${dossierParlementaireOriginalFilePath}…`,
                )
              }
              const dossierParlementaireOriginalJson: string = fs.readFileSync(
                dossierParlementaireOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const dossierParlementaireOriginal: any = validateRawData
                ? DossiersLegislatifsRawConvert.toDossierParlementaire(
                    dossierParlementaireOriginalJson,
                  )
                : JSON.parse(dossierParlementaireOriginalJson)
              const dossierParlementaire =
                dossierParlementaireOriginal.dossierParlementaire
              writeDossierParlementaireReorganizedJson(dossiersDir, dossierParlementaire)
            }
          }
          break
      }

      if (commitAndPush(datasetReorganizedDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      const originalJsonDirectoryOrFilePath: string = path.join(dataDir, dataset.filename)
      if (!options.silent) {
        console.log(`Reorganizing ${originalJsonDirectoryOrFilePath}`)
      }

      const datasetName = path.basename(originalJsonDirectoryOrFilePath, ".json")
      if (gitGroupUrl !== undefined) {
        execSync(`git clone ${gitGroupUrl}/${datasetName}.git`, {
          cwd: dataDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      }

      const datasetReorganizedDir = path.join(dataDir, datasetName)
      fs.ensureDirSync(datasetReorganizedDir)
      for (const filename of fs.readdirSync(datasetReorganizedDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetReorganizedDir, filename))
      }

      switch (dataset.structure) {
        case DatasetStructure.SingleFile:
          {
            const originalJson: string = fs.readFileSync(
              originalJsonDirectoryOrFilePath,
              {
                encoding: "utf8",
              },
            )
            const originalData: any = validateRawData
              ? ScrutinsRawConvert.toScrutins(originalJson)
              : JSON.parse(originalJson)

            for (const scrutin of originalData.scrutins.scrutin) {
              writeScrutinReorganizedJson(datasetReorganizedDir, scrutin)
            }
          }
          break
        case DatasetStructure.SegmentedFiles:
          {
            for (const scrutinSplitPath of walkDir(originalJsonDirectoryOrFilePath)) {
              const scrutinFilename = scrutinSplitPath[scrutinSplitPath.length - 1]
              if (!scrutinFilename.endsWith(".json")) {
                continue
              }
              const scrutinOriginalFilePath = path.join(
                originalJsonDirectoryOrFilePath,
                ...scrutinSplitPath,
              )
              if (options.verbose) {
                console.log(`  Reorganizing "scrutin" file: ${scrutinOriginalFilePath}…`)
              }
              const scrutinOriginalJson: string = fs.readFileSync(
                scrutinOriginalFilePath,
                {
                  encoding: "utf8",
                },
              )
              const scrutinOriginal: any = validateRawData
                ? ScrutinsRawConvert.toScrutinWrapper(scrutinOriginalJson)
                : JSON.parse(scrutinOriginalJson)
              const scrutin = scrutinOriginal.scrutin
              writeScrutinReorganizedJson(datasetReorganizedDir, scrutin)
            }
          }
          break
      }

      if (commitAndPush(datasetReorganizedDir, options.commit, options.remote)) {
        error = true
      }
    }
  }

  // Restore standard conversion of dates to JSON.
  Date.prototype.toJSON = existingDateToJson

  return error
}

function writeAmendementReorganizedJson(
  datasetReorganizedDir: string,
  amendement: {
    uid: string
    identifiant: { saisine: { refTexteLegislatif: string } }
  },
): void {
  const uidTexteLegislatif = amendement.identifiant.saisine.refTexteLegislatif
  assert.notStrictEqual(uidTexteLegislatif, undefined)
  const texteLegislatifDir = path.join(datasetReorganizedDir, uidTexteLegislatif)
  fs.ensureDirSync(texteLegislatifDir)
  const reorganizedJsonFilePath = path.join(texteLegislatifDir, amendement.uid + ".json")
  fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(amendement, null, 2), {
    encoding: "utf8",
  })
}

function writeDocumentReorganizedJson(
  documentsDir: string,
  document: { uid: string },
): void {
  const match = documentUidRegex.exec(document.uid)
  assert.notStrictEqual(
    match,
    null,
    `Unexpected structure for document UID: ${document.uid}`,
  )
  const documentType = path.join(documentsDir, match![1])
  const chambreDir = path.join(documentType, match![2])
  const republiqueDir = path.join(chambreDir, match![3])
  const legislatureDir = path.join(republiqueDir, match![4])
  const etatDir = path.join(legislatureDir, match![5])
  let numero = "000000" + match![6]
  numero = numero.substr(numero.length - 6)
  const numeroMilliersDir = path.join(etatDir, numero.substr(0, 3))
  fs.ensureDirSync(numeroMilliersDir)
  const reorganizedJsonFilePath: string = path.join(
    numeroMilliersDir,
    document.uid + ".json",
  )
  fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(document, null, 2), {
    encoding: "utf8",
  })
}

function writeDossierParlementaireReorganizedJson(
  dossiersDir: string,
  dossierParlementaire: { uid: string },
): void {
  const match = dossierUidRegex.exec(dossierParlementaire.uid)
  assert.notStrictEqual(
    match,
    null,
    `Unexpected structure for dossier parlementaire UID: ${dossierParlementaire.uid}`,
  )
  const republiqueDir = path.join(dossiersDir, match![1])
  const legislatureDir = path.join(republiqueDir, match![2])
  let numero = "000000" + match![3]
  numero = numero.substr(numero.length - 6)
  const numeroMilliersDir = path.join(legislatureDir, numero.substr(0, 3))
  fs.ensureDirSync(numeroMilliersDir)
  const reorganizedJsonFilePath: string = path.join(
    numeroMilliersDir,
    dossierParlementaire.uid + ".json",
  )
  fs.writeFileSync(
    reorganizedJsonFilePath,
    JSON.stringify(dossierParlementaire, null, 2),
    {
      encoding: "utf8",
    },
  )
}

function writeReunionReorganizedJson(
  datasetReorganizedDir: string,
  reunion: { uid: string },
): void {
  const match = reunionUidRegex.exec(reunion.uid)
  assert.notStrictEqual(
    match,
    null,
    `Unexpected structure for reunion UID: ${reunion.uid}`,
  )
  const chambreDir = path.join(datasetReorganizedDir, match![1])
  const republiqueDir = path.join(chambreDir, match![2])
  const legislatureDir = path.join(republiqueDir, match![3])
  const sessionDir = path.join(legislatureDir, match![4])
  const idDir = path.join(sessionDir, match![5])
  let numero = "000000000" + match![6]
  numero = numero.substr(numero.length - 9)
  const numeroMillionsDir = path.join(idDir, numero.substr(0, 3))
  const numeroMilliersDir = path.join(numeroMillionsDir, numero.substr(3, 3))
  fs.ensureDirSync(numeroMilliersDir)
  const reorganizedJsonFilePath = path.join(numeroMilliersDir, reunion.uid + ".json")
  fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(reunion, null, 2), {
    encoding: "utf8",
  })
}

function writeScrutinReorganizedJson(
  datasetReorganizedDir: string,
  scrutin: { uid: string },
): void {
  const match = scrutinUidRegex.exec(scrutin.uid)
  assert.notStrictEqual(
    match,
    null,
    `Unexpected structure for scrutin UID: ${scrutin.uid}`,
  )
  const republiqueDir = path.join(datasetReorganizedDir, match![1])
  const legislatureDir = path.join(republiqueDir, match![2])
  let numero = "000000" + match![3]
  numero = numero.substr(numero.length - 6)
  const numeroMilliersDir = path.join(legislatureDir, numero.substr(0, 3))
  fs.ensureDirSync(numeroMilliersDir)
  const reorganizedJsonFilePath: string = path.join(
    numeroMilliersDir,
    scrutin.uid + ".json",
  )
  fs.writeFileSync(reorganizedJsonFilePath, JSON.stringify(scrutin, null, 2), {
    encoding: "utf8",
  })
}

if (reorganizeData(options.dataDir)) {
  process.exit(1)
} else {
  process.exit(0)
}
