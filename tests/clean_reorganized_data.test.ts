let rewire = require("rewire")
let sinon = require("sinon")
import { assert } from "chai"
import temp from "temp"
import fs from "fs-extra"
import { load } from "../src/file_systems"

let clean_reorganized_data = rewire("../src/scripts/clean_reorganized_data")

suite("clean_reorganized_data")

test("#cleanReorganizedData-all", function() {
  const tmpDir = temp.mkdirSync("clean")
  fs.copySync("tests/clean_reorganized_data", `${tmpDir}`)
  const expected = [
    `${tmpDir}/Agenda_XV_nettoye/AN/R5/L15/S2017/IDC/000/427/RUANR5L15S2017IDC427052.json`,
    `${tmpDir}/Amendements_XV_nettoye/PRJLANR5L15BTC0902/AMANR5L15SEA717460BTC0902P0D1N2549.json`,
    `${tmpDir}/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/organes/PO191887.json`,
    `${tmpDir}/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/acteurs/PA2304.json`,
    `${tmpDir}/Dossiers_Legislatifs_XV_nettoye/documents/ACIN/AN/R5/15/B/000/ACINANR5L15B0012.json`,
    `${tmpDir}/Dossiers_Legislatifs_XV_nettoye/dossiers/R5/L15/038/DLR5L15N38150.json`,
  ]

  for (const pathname of expected) assert(!fs.existsSync(pathname), pathname)

  const cleanReorganizedData = clean_reorganized_data.__get__(
    "cleanReorganizedData",
  )

  for (const dataset of [
    "Agenda_XV",
    "Amendements_XV",
    "AMO30_tous_acteurs_tous_mandats_tous_organes_historique",
    "Dossiers_Legislatifs_XV",
  ]) {
    const errors = cleanReorganizedData({
      dataDir: tmpDir,
      categories: ["All"],
      dataset: dataset,
      silent: true,
    })
    assert.equal(errors, false)
  }

  for (const pathname of expected) assert(fs.existsSync(pathname), pathname)
})

test("#cleanReorganizedData-organe", function() {
  const tmpDir = temp.mkdirSync("clean")
  fs.copySync("tests/clean_reorganized_data", `${tmpDir}`)
  const existing = `${tmpDir}/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/organes/PO730940.json`
  const existingNoVersion = `${tmpDir}/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/organes/PO60186.json`
  const created = `${tmpDir}/AMO30_tous_acteurs_tous_mandats_tous_organes_historique_nettoye/organes/PO191887.json`

  const cleanReorganizedData = clean_reorganized_data.__get__(
    "cleanReorganizedData",
  )

  assert(!fs.existsSync(created), created)
  assert(fs.existsSync(existing), existing)
  assert(fs.existsSync(existingNoVersion), existing)

  const errors = cleanReorganizedData({
    dataDir: tmpDir,
    dataset: "AMO30_tous_acteurs_tous_mandats_tous_organes_historique",
    categories: ["ActeursEtOrganes"],
    silent: true,
  })
  assert.equal(errors, false)

  const getSchemaVersionDefault = clean_reorganized_data.__get__(
    "getSchemaVersionDefault",
  )
  const defaultVersion = getSchemaVersionDefault("organe")
  assert.equal(load(created).schemaVersion, defaultVersion)

  assert(!("schemaVersion" in load(existingNoVersion)), existingNoVersion)
})

test("#getSchemaVersionDefault", function() {
  const getSchemaVersionDefault = clean_reorganized_data.__get__(
    "getSchemaVersionDefault",
  )
  assert.include(getSchemaVersionDefault("acteur"), "acteur-")
})

test("#main", function() {
  const main = clean_reorganized_data.__get__("main")
  const log = sinon.stub(console, "log")
  const error = sinon.stub(console, "error")

  // good run doing nothing
  let status = main([
    "--verbose",
    "--dataset=none",
    "tests/clean_reorganized_data",
  ])
  assert.equal(status, 0)

  log.restore()
  error.restore()
})
