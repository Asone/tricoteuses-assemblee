let rewire = require("rewire")
let sinon = require("sinon")
import { assert } from "chai"

let dossiers_legislatifs = rewire(
  "../src/scripts/document_dossiers_legislatifs",
)

suite("document_dossier")

test("#parseArgs", function() {
  const parseArgs = dossiers_legislatifs.__get__("parseArgs")
  const warn = sinon.stub(console, "warn")
  let options = parseArgs(["--help"])
  assert.equal(options, null)
  assert(warn.args[0][0].includes("documentation du dossier"))
  warn.reset()

  const error = sinon.stub(console, "error")
  options = parseArgs([])
  assert.equal(options, null)
  assert(warn.args[0][0].includes("documentation du dossier"))
  assert(error.args[0][0].includes("data is mandatory"))
  warn.reset()
  error.reset()

  options = parseArgs(["--data", "path.json"])
  assert.deepEqual(options.data, ["path.json"])

  warn.restore()
  error.restore()
})

test("#ActesDocumentation", function() {
  const ActesDocumentation = dossiers_legislatifs.__get__("ActesDocumentation")
  const doc = new ActesDocumentation()
  doc.collectFiles(["tests/document_dossiers_legislatifs/*.json"])
  doc.processFiles(["tests/document_dossiers_legislatifs/*.json"])
  assert(
    "Responsabilité pénale du président de la république" in doc.procedures,
  )
  assert("Procédure législative" in doc.procedures)
})

test("#eqSet", function() {
  const eqSet = dossiers_legislatifs.__get__("eqSet")
  assert.isOk(eqSet(new Set([1, 2, 3]), new Set([2, 1, 3])))
  assert.isNotOk(eqSet(new Set([1]), new Set([2, 1, 3])))
  assert.isNotOk(eqSet(new Set([1, 2, 3]), new Set([4, 5, 6])))
})

test("#orderedCodes", function() {
  const ActesDocumentation = dossiers_legislatifs.__get__("ActesDocumentation")
  const doc = new ActesDocumentation(true)

  let actual = doc.orderedCodes(
    ["P-B", "P-A", "P-C"],
    { _pattern_: "/A//B//C/" },
    "P",
  )
  assert.deepEqual(new Set(["P-A", "P-B", "P-C"]), actual)

  actual = doc.orderedCodes(["B", "A", "C"], { _pattern_: "/A//B//C/" }, null)
  assert.deepEqual(new Set(["A", "B", "C"]), actual)
})

test("#remapActes", function() {
  const ActesDocumentation = dossiers_legislatifs.__get__("ActesDocumentation")
  const doc = new ActesDocumentation()
  let a = [
    {
      codeActe: "AN1",
      libelleActe: {
        nomCanonique: "AN1-libelle",
      },
    },
    {
      codeActe: "SN3-DEBAT",
      libelleActe: {
        nomCanonique: "SN3-DEBAT-libelle",
      },
    },
    {
      codeActe: "OTHER",
    },
  ]
  const expected = [
    {
      codeActe: "lecture",
      libelleActe: {
        nomCanonique: "AN1-libelle (Assemblée Nationale)",
      },
    },
    {
      codeActe: "lecture-DEBAT",
      libelleActe: {
        nomCanonique: "SN3-DEBAT-libelle (Sénat)",
      },
    },
    {
      codeActe: "OTHER",
    },
  ]
  doc.collectActes(a)
  const actual = doc.remapActes(a)
  assert.deepEqual(expected, actual)
})

test("#LectureMapping_getType", function() {
  const LectureMapping = dossiers_legislatifs.__get__("LectureMapping")
  assert.throws(
    () => LectureMapping.getType({ codeActe: "unexpected" }),
    "does not start with AN or SN",
  )
})

test("#main", function() {
  const main = dossiers_legislatifs.__get__("main")
  const log = sinon.stub(console, "log")
  const warn = sinon.stub(console, "warn")
  const error = sinon.stub(console, "error")

  // good run
  let status = main([
    "--data",
    "--no-sanity-checks",
    "tests/document_dossiers_legislatifs/*.json",
  ])
  assert.equal(status, 0)
  assert.deepEqual(warn.args, [])
  warn.reset()

  // missing mandatory --data option
  assert.equal(main([]), 1)

  // sanity check failure
  assert.throws(
    () => main(["--data", "tests/document_dossiers_legislatifs/*.json"]),
    "is never used",
  )

  error.restore()
  warn.restore()
  log.restore()
})
