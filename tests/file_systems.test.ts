import { assert } from "chai"
import { load, getFiles } from "../src/file_systems"

test("#load", function() {
  const payload = load("tests/document_dossiers_legislatifs/DLR5L14N28884.json")
  assert.equal(payload.legislature, 14)
})

test("#getFiles", function() {
  let files = getFiles([
    "tests/document_dossiers_legislatifs/DLR5L14N28884.json",
  ])
  assert(files[0].endsWith("DLR5L14N28884.json"))
  assert.equal(files.length, 1)

  files = getFiles(["tests/**/DLR5L14N28884.json"])
  assert(files[0].endsWith("DLR5L14N28884.json"))
  assert.equal(files.length, 1)

  files = getFiles(["tests/document_dossiers_legislatifs/*.json"])
  assert(files.length > 1)
})
