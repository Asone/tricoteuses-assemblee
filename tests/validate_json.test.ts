let rewire = require("rewire")
import { execSync } from "child_process"
let sinon = require("sinon")
import { assert } from "chai"
import fs from "fs-extra"
import path from "path"
import { load } from "../src/file_systems"

let validate_json = rewire("../src/scripts/validate_json")

suite("validate_json")

test("#getSchemaDir", function() {
  const log = sinon.stub(console, "log")

  const Validate = validate_json.__get__("Validate")
  const v = new Validate({
    verbose: true,
    repository: execSync("git rev-parse --show-toplevel")
      .toString()
      .trim(),
  })
  const tag = "schema-acteur-1.0"
  const tagDir = v.getSchemaDir(tag)
  assert(fs.existsSync(tagDir), tagDir)
  const acteurDir = path.join(tagDir, "acteur")
  assert(fs.existsSync(acteurDir), acteurDir)
  assert.equal(tagDir, v.getSchemaDir(tag))

  assert.throws(
    () => v.getSchemaDir("FAKETAG"),
    "pathspec 'tags/FAKETAG' did not match",
  )

  v.options.dev = true
  const devDir = v.getSchemaDir(tag)
  assert.equal(devDir, "src/schemas")

  log.restore()
})

test("#getValidator", function() {
  const Validate = validate_json.__get__("Validate")
  const v = new Validate({
    reference: ["document/CodeLibelle.json", "document/Indexation.json"],
    schema: "document/Document.json",
    repository: execSync("git rev-parse --show-toplevel")
      .toString()
      .trim(),
  })

  const validator = v.getValidator("document-1.0")
  assert.equal(validator, v.getValidator("document-1.0"))

  let valid = validator({
    schemaVersion: "document-1.0",
  })
  assert.equal(false, valid)
  assert.equal(validator.errors[0].params.missingProperty, "xsiType")

  const p = "tests/validate_json/RAPPANR5L15B1302.json"
  valid = validator(load(p))
  assert(valid, p)
})

test("#validate", function() {
  const Validate = validate_json.__get__("Validate")
  const v = new Validate({
    reference: ["document/CodeLibelle.json", "document/Indexation.json"],
    schema: "document/Document.json",
    repository: execSync("git rev-parse --show-toplevel")
      .toString()
      .trim(),
  })

  const error = sinon.stub(console, "error")

  const status = v.validate({
    schemaVersion: "document-1.0",
  })
  assert.equal(status, 1)
  const errors = error.args[1][0]
  assert.equal(errors[0].params.missingProperty, "xsiType")

  error.restore()
})

test("#getVersion", function() {
  const Validate = validate_json.__get__("Validate")
  const v = new Validate({
    schema: "acteur/Acteur.json",
  })
  assert.equal(v.getVersion({}), "acteur-1.0")
  const version = "document-2.0"
  assert.equal(v.getVersion({ schemaVersion: version }), version)
})

test("#getSchemaURL", function() {
  const Validate = validate_json.__get__("Validate")
  const v = new Validate({
    schema: "acteur/Acteur.json",
  })
  const version = "document-2.0"

  v.options.repository = "http://site/repo.git"
  assert.include(v.getSchemaURL(version), `repo/tree/schema-${version}`)

  v.options.repository = "http://site/repo"
  assert.include(v.getSchemaURL(version), `repo/tree/schema-${version}`)

  v.options.repository = "." // current directory always exists
  assert.include(v.getSchemaURL(version), `at tag schema-${version}`)

  v.options.dev = true
  assert.equal(v.getSchemaURL(version), `src/schemas`)
})

test("#parseArgs", function() {
  const parseArgs = validate_json.__get__("parseArgs")
  const warn = sinon.stub(console, "warn")
  let options = parseArgs(["--help"])
  assert.equal(options, null)
  assert(warn.args[0][0].includes("Validate JSON files"))
  warn.reset()

  const error = sinon.stub(console, "error")
  options = parseArgs([])
  assert.equal(options, null)
  assert(warn.args[0][0].includes("Validate JSON files"))
  assert(error.args[0][0].includes("data is mandatory"))
  warn.reset()
  error.reset()

  options = parseArgs(["--data", "path.json"])
  assert.deepEqual(options.data, ["path.json"])

  warn.restore()
  error.restore()
})

test("#main", function() {
  const main = validate_json.__get__("main")
  const log = sinon.stub(console, "log")
  const warn = sinon.stub(console, "warn")
  const error = sinon.stub(console, "error")

  // good run
  let status = main([
    "--verbose",
    "--repository",
    execSync("git rev-parse --show-toplevel")
      .toString()
      .trim(),
    "--reference=document/CodeLibelle.json",
    "--reference=document/Indexation.json",
    "--schema=document/Document.json",
    "--data=tests/validate_json/RAPPANR5L15B1302.json",
  ])
  assert.equal(status, 0)
  assert.deepEqual(warn.args, [])
  warn.reset()

  // missing mandatory --data option
  assert.equal(main([]), 1)

  log.restore()
  warn.restore()
  error.restore()
})
